MAKEFLAGS += --no-builtin-rules --no-builtin-variables

.SUFFIXES:

.SECONDEXPANSION:

.DELETE_ON_ERROR:

.PRECIOUS: %.hdf5

, := ,
p := (
P := )

PYTHONENV := TEXINPUTS="$(shell pwd)/build/:$(shell pwd)/:$(shell pwd)/header-components/:$(shell pwd)/texmf//:"
PYTHON := $(PYTHONENV) python3

TEX := tex-utils/tex.sh --build-dir build/thesis --tex-inputs "$(shell pwd)/build/:$(shell pwd)/header-components/:$(shell pwd)/texmf//" --biber

HEADERS := $(addprefix header/, $(addsuffix .tex, header packages settings commands))
MATPLOTLIB_HEADERS := $(addprefix header/, $(addsuffix .tex, matplotlib matplotlib-packages matplotlib-settings commands))
BIBS := libraries.bib lit.bib
DEPENDS := thesis.tex thesis/*.tex $(HEADERS) $(BIBS)

define thesis_deps
build/thesis/thesis.pdf: $(1)

build/thesis/thesis-print.pdf: $(1)
endef

$(eval $(call thesis_deps, $(DEPENDS) | build/thesis))

build/thesis/thesis.pdf:
	@$(TEX) thesis.tex

thesis: build/thesis/thesis.pdf

fast:
	@$(TEX) --fast thesis.tex

build/thesis/thesis-print.pdf:
	@$(TEX) --jobname thesis-print thesis.tex

thesis-print: build/thesis/thesis-print.pdf

fast-print:
	@$(TEX) --jobname thesis-print --fast thesis-print.tex

build/run/%/mcmc.hdf5: run/%.sh
	$< mcmc

build/run/%/mcmc.pmc.hdf5: run/%.sh build/run/%/mcmc.hdf5
	$< mcmc-to-pmc

build/run/%/propagate.hdf5: run/%.sh build/run/%/mcmc.pmc.hdf5
	$< propagate

build/run/%/propagate.pmc.hdf5: run/%.sh build/run/%/mcmc.pmc.hdf5 build/run/%/propagate.hdf5
	$< merge-pmc-propagate

define run_template
build/run/$(1)/$(2)-hist-%.pkl: script/hist.py build/run/$(1)/$(2)
	$$< build/run/$(1)/$(2) $$*

precious-dummy: build/run/$(1)/$(2)
endef

$(foreach TYPE, mcmc.hdf5 mcmc.pmc.hdf5 propagate.hdf5 propagate.pmc.hdf5, $(foreach RUN, $(notdir $(basename $(wildcard run/*.sh))), $(eval $(call run_template,$(RUN),$(TYPE)))))

build/plot/regions/%.png: build/plot/regions/%.pdf | build/plot/regions
	pdftoppm -png -singlefile -r 1000 $< $(basename $@)

build/plot/regions/B-mixing.pdf: script/plot-2d-regions.py \
    build/run/Δm/mcmc.hdf5-hist-300.pkl \
    build/run/ϕ_12/mcmc.hdf5-hist-300.pkl \
    build/run/Δm-ϕ_12/mcmc.hdf5-hist-200.pkl \
    | build/plot/regions
	$< $@ equal \
	    'Re{ll_LQ}' -0.15 0.15 \
	    'Im{ll_LQ}' -0.15 0.15 \
	    build/run/Δm/mcmc.hdf5-hist-300.pkl      1 blue \
	    build/run/ϕ_12/mcmc.hdf5-hist-300.pkl    1 red \
	    build/run/Δm-ϕ_12/mcmc.hdf5-hist-200.pkl 1 0.5,0.75,0.5,1

build/plot/B-mixing.pdf: script/plot-2d.py matplotlibrc \
    build/plot/regions/B-mixing.png \
    | build/plot
	$< $@ equal \
	    '$$\Re \lambda_{\mathrm{s} \ell \vphantom{\mathrm{b} \ell}}^{} \lambda_{\vphantom{\mathrm{s} \ell} \mathrm{b} \ell}^*$$' -0.15 0.15 \
	    '$$\Im \lambda_{\mathrm{s} \ell \vphantom{\mathrm{b} \ell}}^{} \lambda_{\vphantom{\mathrm{s} \ell} \mathrm{b} \ell}^*$$' -0.15 0.15 \
	    build/plot/regions/B-mixing.png

$(eval $(call thesis_deps, build/plot/B-mixing.pdf))

define plot_combined_template
build/plot/regions/$(strip $(1))-$(strip $(2))-M_$(strip $(3))-combined.pdf: script/plot-2d-regions.py \
    $(if $(subst e,,$(strip $(1))), \
        build/run/$(strip $(1))-B_s_ll/mcmc.hdf5-hist-500.pkl \
    ) \
    build/run/$(strip $(1))-R_K/mcmc.hdf5-hist-500.pkl \
    build/run/Δm-ϕ_12/mcmc.hdf5-hist-200.pkl \
    build/run/$(strip $(1))-$(strip $(2))-M_$(strip $(3))-combined/propagate.pmc.hdf5-hist-200.pkl \
    | build/plot/regions
	$$< $$@ equal \
	    'Re{ll_LQ}' $$$$(echo '-0.05 * $$(strip $(3)) * $$(strip $(4))' | bc -l) $$$$(echo '0.05 * $$(strip $(3)) * $$(strip $(4))' | bc -l) \
	    'Im{ll_LQ}' $$$$(echo '-0.05 * $$(strip $(3)) * $$(strip $(4))' | bc -l) $$$$(echo '0.05 * $$(strip $(3)) * $$(strip $(4))' | bc -l) \
	    $(if $(subst e,,$(strip $(1))), \
	        build/run/$$(strip $(1))-B_s_ll/mcmc.hdf5-hist-500.pkl $$$$(echo '$$(strip $(3))^2 * $$(strip $(5))' | bc -l) blue \
	    ) \
	    build/run/$$(strip $(1))-R_K/mcmc.hdf5-hist-500.pkl    $$$$(echo '$$(strip $(3))^2 * $$(strip $(6))' | bc -l) red \
	    build/run/Δm-ϕ_12/mcmc.hdf5-hist-200.pkl                   $$$$(echo '$$(strip $(3))   * $$(strip $(7))' | bc -l) green \
	    build/run/$$(strip $(1))-$$(strip $(2))-M_$$(strip $(3))-combined/propagate.pmc.hdf5-hist-200.pkl 1 yellow

build/plot/$(strip $(1))-$(strip $(2))-M_$(strip $(3))-combined.pdf: script/plot-2d.py matplotlibrc \
    build/plot/regions/$(strip $(1))-$(strip $(2))-M_$(strip $(3))-combined.png \
    | build/plot
	$$< $$@ equal \
	    '$$$$\Re \lambda_{\mathrm{s} \ell \vphantom{\mathrm{b} \ell}}^{} \lambda_{\vphantom{\mathrm{s} \ell} \mathrm{b} \ell}^*$$$$' \
	    $$$$(echo '-0.05 * $$(strip $(3)) * $$(strip $(4))' | bc -l) $$$$(echo '0.05 * $$(strip $(3)) * $$(strip $(4))' | bc -l) \
	    '$$$$\Im \lambda_{\mathrm{s} \ell \vphantom{\mathrm{b} \ell}}^{} \lambda_{\vphantom{\mathrm{s} \ell} \mathrm{b} \ell}^*$$$$' \
	    $$$$(echo '-0.05 * $$(strip $(3)) * $$(strip $(4))' | bc -l) $$$$(echo '0.05 * $$(strip $(3)) * $$(strip $(4))' | bc -l) \
	    build/plot/regions/$(strip $(1))-$(strip $(2))-M_$(strip $(3))-combined.png

$(eval $(call thesis_deps, build/plot/$(strip $(1))-$(strip $(2))-M_$(strip $(3))-combined.pdf))
endef

$(eval $(call plot_combined_template, e, 2,  1, 1,          1,   1,   1        ))
$(eval $(call plot_combined_template, e, 2, 10, 1,          1,   1,   1        ))
$(eval $(call plot_combined_template, e, 2, 50, 1,          1,   1,   1        ))

$(eval $(call plot_combined_template, e, 3,  1, sqrt(2/5), -0.5, 0.5, sqrt(2/5)))
$(eval $(call plot_combined_template, e, 3, 10, sqrt(2/5), -0.5, 0.5, sqrt(2/5)))
$(eval $(call plot_combined_template, e, 3, 50, sqrt(2/5), -0.5, 0.5, sqrt(2/5)))

$(eval $(call plot_combined_template, μ, 2,  1, 1,          1,   1,   1        ))
$(eval $(call plot_combined_template, μ, 2, 10, 1,          1,   1,   1        ))
$(eval $(call plot_combined_template, μ, 2, 50, 1,          1,   1,   1        ))

$(eval $(call plot_combined_template, μ, 3,  1, sqrt(2/5), -0.5, 0.5, sqrt(2/5)))
$(eval $(call plot_combined_template, μ, 3, 10, sqrt(2/5), -0.5, 0.5, sqrt(2/5)))
$(eval $(call plot_combined_template, μ, 3, 50, sqrt(2/5), -0.5, 0.5, sqrt(2/5)))

define plot_1d_template_template
define plot_$(strip $(1))_template
build/plot/regions/$$(strip $$(1))-$$(strip $$(2))-M_$$(strip $$(3))-$(strip $(2)).pdf build/plot/regions/$$(strip $$(1))-$$(strip $$(2))-M_$$(strip $$(3))-$(strip $(2)).details.pkl: build/plot/regions/$$(strip $$(1))-$$(strip $$(2))-M_$$(strip $$(3))-$(strip $(2)).dummy
	@if [ ! -e "$$$$@" ] ; then echo "rerun make" ; rm $$$$< ; exit 1 ; fi

build/plot/regions/$$(strip $$(1))-$$(strip $$(2))-M_$$(strip $$(3))-$(strip $(2)).dummy: script/plot-1d-regions.py \
    build/run/$$(strip $$(1))-$$(strip $$(2))-M_$$(strip $$(3))-combined/propagate.pmc.hdf5-hist-$(strip $(3)).pkl \
    | build/plot/regions
	@touch $$$$@
	$$$$< build/plot/regions/$$(strip $$(1))-$$(strip $$(2))-M_$$(strip $$(3))-$(strip $(2)).pdf \
	    build/plot/regions/$$(strip $$(1))-$$(strip $$(2))-M_$$(strip $$(3))-$(strip $(2)).details.pkl \
	    $(4) \
	    $$(strip $$(4)) $$(strip $$(5)) \
	    build/run/$$(strip $$(1))-$$(strip $$(2))-M_$$(strip $$(3))-combined/propagate.pmc.hdf5-hist-$(strip $(3)).pkl

build/plot/$$(strip $$(1))-$$(strip $$(2))-M_$$(strip $$(3))-$(strip $(2)).pdf: script/plot-1d.py matplotlibrc \
    build/plot/regions/$$(strip $$(1))-$$(strip $$(2))-M_$$(strip $$(3))-$(strip $(2)).png \
    build/plot/regions/$$(strip $$(1))-$$(strip $$(2))-M_$$(strip $$(3))-$(strip $(2)).details.pkl \
    | build/plot
	$$$$< $$$$@ \
	    $(5) $$(strip $$(4)) $$(strip $$(5)) \
	    build/plot/regions/$$(strip $$(1))-$$(strip $$(2))-M_$$(strip $$(3))-$(strip $(2)).png \
	    build/plot/regions/$$(strip $$(1))-$$(strip $$(2))-M_$$(strip $$(3))-$(strip $(2)).details.pkl \
	    $(6)

$$(eval $$(call thesis_deps, build/plot/$$(strip $$(1))-$$(strip $$(2))-M_$$(strip $$(3))-$(strip $(2)).pdf))
endef
endef

$(eval $(call plot_1d_template_template, R_K, R_K, 200, \
    'B->Kll::R_Kavg@LargeRecoil$(,)lq-lepton=$$$(p)subst μ$(,)mu$(,)$$$(p)strip $$$(p)1$(P)$(P)$(P)$(,)lq-su2=$$$(p)strip $$$(p)2$(P)$(P)$(,)model=Leptoquark$(,)q=u$(,)scan-mode=cartesian$(p)s_max=6.0$(,)s_min=1.0$(P)', \
    '$$$$$$$$R_{\mathrm{K}}$$$$$$$$', \
    'Amoroso limit: a = 0$(,) theta = 0.50054$(,) alpha = 5.0001$(,) beta = 3.7997' \
))

$(eval $(call plot_R_K_template, e, 2,  1, 0.38, 1.08))
$(eval $(call plot_R_K_template, e, 2, 10, 0.58, 1.08))
$(eval $(call plot_R_K_template, e, 2, 50, 0.87, 1.07))

$(eval $(call plot_R_K_template, e, 3,  1, 0.38, 1.08))
$(eval $(call plot_R_K_template, e, 3, 10, 0.53, 1.08))
$(eval $(call plot_R_K_template, e, 3, 50, 0.84, 1.08))

$(eval $(call plot_R_K_template, μ, 2,  1, 0.53, 1.12))
$(eval $(call plot_R_K_template, μ, 2, 10, 0.58, 1.12))
$(eval $(call plot_R_K_template, μ, 2, 50, 0.86, 1.08))

$(eval $(call plot_R_K_template, μ, 3,  1, 0.45, 1.12))
$(eval $(call plot_R_K_template, μ, 3, 10, 0.52, 1.08))
$(eval $(call plot_R_K_template, μ, 3, 50, 0.83, 1.07))

$(eval $(call plot_1d_template_template, R_Kstar, R_Kstar, 200, \
    'B->K^*ll::R_K^*avg@LargeRecoil$(,)lq-lepton=$$$(p)subst μ$(,)mu$(,)$$$(p)strip $$$(p)1$(P)$(P)$(P)$(,)lq-su2=$$$(p)strip $$$(p)2$(P)$(P)$(,)model=Leptoquark$(,)scan-mode=cartesian$(p)s_max=6.0$(,)s_min=1.0$(P)', \
    '$$$$$$$$R_{\mathrm{K}^*}$$$$$$$$', \
))

$(eval $(call plot_R_Kstar_template, e, 2,  1, -0.05, 1.75))
$(eval $(call plot_R_Kstar_template, e, 2, 10,  0.85, 1.52))
$(eval $(call plot_R_Kstar_template, e, 2, 50,  0.95, 1.11))

$(eval $(call plot_R_Kstar_template, e, 3,  1,  0.26, 1.02))
$(eval $(call plot_R_Kstar_template, e, 3, 10,  0.55, 1.07))
$(eval $(call plot_R_Kstar_template, e, 3, 50,  0.84, 1.07))

$(eval $(call plot_R_Kstar_template, μ, 2,  1,  0.95, 1.67))
$(eval $(call plot_R_Kstar_template, μ, 2, 10,  0.95, 1.43))
$(eval $(call plot_R_Kstar_template, μ, 2, 50,  0.94, 1.12))

$(eval $(call plot_R_Kstar_template, μ, 3,  1,  0.50, 2.05))
$(eval $(call plot_R_Kstar_template, μ, 3, 10,  0.58, 1.07))
$(eval $(call plot_R_Kstar_template, μ, 3, 50,  0.84, 1.06))

$(eval $(call plot_1d_template_template, Bs_ll, Bs_ll, 200, \
    'B_q->ll::BR@Untagged$(,)l=$$$(p)subst μ$(,)mu$(,)$$$(p)strip $$$(p)1$(P)$(P)$(P)$(,)lq-lepton=$$$(p)subst μ$(,)mu$(,)$$$(p)strip $$$(p)1$(P)$(P)$(P)$(,)lq-su2=$$$(p)strip $$$(p)2$(P)$(P)$(,)model=Leptoquark$(,)q=s$(,)scan-mode=cartesian$(p)$(P)', \
    $$$(p)if $$$(p)subst e$(,)$(,)$$$(p)strip $$$(p)1$(P)$(P)$(P)$(,) \
        '$$$$$$$$\mathcal{B}$$(p)\mathrm{B}_{\mathrm{s}} \to \mathrm{\mu} \mathrm{\mu}$$(P)$$$$$$$$' \
    $(,) \
        '$$$$$$$$\mathcal{B}$$(p)\mathrm{B}_{\mathrm{s}} \to \mathrm{e  } \mathrm{e  }$$(P)$$$$$$$$' \
    $(P), \
    $$$(p)if $$$(p)subst e$(,)$(,)$$$(p)strip $$$(p)1$(P)$(P)$(P)$(,) \
        'Amoroso limit: a = 0$$(,) theta = 2.4557e-09$$(,) alpha = 1.9807$$(,) beta = 3.0081' \
    $(P) \
))

#$(eval $(call plot_Bs_ll_template, e, 2,  1, 0.00, 9.00e-13))
#$(eval $(call plot_Bs_ll_template, e, 2, 10, 0.00, 9.00e-13))
#$(eval $(call plot_Bs_ll_template, e, 2, 50, 0.00, 9.00e-13))

#$(eval $(call plot_Bs_ll_template, e, 3,  1, 0.00, 9.00e-13))
#$(eval $(call plot_Bs_ll_template, e, 3, 10, 0.00, 9.00e-13))
#$(eval $(call plot_Bs_ll_template, e, 3, 50, 0.00, 9.00e-13))

#$(eval $(call plot_Bs_ll_template, μ, 2,  1, 0.00, 9.00e-09))
#$(eval $(call plot_Bs_ll_template, μ, 2, 10, 0.00, 9.00e-09))
#$(eval $(call plot_Bs_ll_template, μ, 2, 50, 0.00, 9.00e-09))

#$(eval $(call plot_Bs_ll_template, μ, 3,  1, 0.00, 9.00e-09))
#$(eval $(call plot_Bs_ll_template, μ, 3, 10, 0.00, 9.00e-09))
#$(eval $(call plot_Bs_ll_template, μ, 3, 50, 0.00, 9.00e-09))

build/plot/legend-B-mixing.pdf: script/legend.py matplotlibrc $(MATPLOTLIB_HEADERS)
	$(PYTHON) $< $@ vertical blue '$$\Del{m}_{\Ps}$$' red '$$ϕ_{\Ps}$$' green '\PBs mixing'
	pdfcrop $@ $@

$(eval $(call thesis_deps, build/plot/legend-B-mixing.pdf))

build/plot/legend-combined.pdf: script/legend.py matplotlibrc $(MATPLOTLIB_HEADERS)
	$(PYTHON) $< $@ horizontal blue '$$\PBs → \Pmu \Pmu$$' red '$$\RK$$' green '\PBs mixing' yellow 'combined'
	pdfcrop $@ $@

$(eval $(call thesis_deps, build/plot/legend-combined.pdf))

build $(addprefix build/, plot plot/regions run thesis):
	mkdir -p $@

clean:
	rm -rf build/thesis

.PHONY: all thesis thesis-print fast fast-print precious-dummy clean
