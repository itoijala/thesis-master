#!/bin/bash

MODE="${1}"
shift

COMMAND="make build/run/${1}/${2}.hdf5 ${3}"
NAME="$(echo "$(echo "${2}" | cut -c -2)-${1}" | sed -e 's/μ/mu/' -e 's/Δ/Delta/' -e 's/ϕ/phi/' | cut -c -15)"

if [ $# -gt 2 ] ; then
	shift
fi
shift 2

if [ "${MODE}" != "-q" ] && [ "${MODE}" != "-n" ] ; then
	RUNNER="bash"
else
	RUNNER="qsub -m abe -v LANG=en_US.utf-8 -d /fhgfs/users/itoijala/thesis-master -l walltime=300:00:00 -l nodes=1:ppn=4 -l vmem=8gb -N ${NAME}"
fi

if [ "${MODE}" == "-n" ] ; then
	RUN=false
else
	RUN=true
fi

echo "'${COMMAND}'" \| ${RUNNER} "$@"

if ${RUN} ; then
	echo "${COMMAND}" | ${RUNNER} "$@"
fi
