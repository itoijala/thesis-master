/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2015 Simon Braß
 *
 * This file is part of the EOS project. EOS is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * EOS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <eos/rare-b-decays/charm-loops.hh>
#include <eos/rare-b-decays/hadronic-ratio.hh>
#include <eos/rare-b-decays/long-distance.hh>
#include <eos/utils/destringify.hh>
#include <eos/utils/log.hh>
#include <eos/utils/options.hh>
#include <eos/utils/power_of.hh>
#include <eos/utils/private_implementation_pattern-impl.hh>

#include <cmath>
#include <functional>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_integration.h>

namespace eos
{
    using std::norm;

    /*
     * Ratio: ee -> hadrons / ee -> mu mu
     */
    template <>
    struct Implementation<HadronicRatio<LowRecoil>>
    {
        UsedParameter mu;
        UsedParameter alpha_e;
        UsedParameter alpha_s;

        UsedParameter ww_radius;

        UsedParameter mass_1;
        UsedParameter mass_2;
        UsedParameter mass_3;
        UsedParameter mass_4;

        UsedParameter fit_width_1;
        UsedParameter fit_width_2;
        UsedParameter fit_width_3;
        UsedParameter fit_width_4;

        UsedParameter ee_width_1;
        UsedParameter ee_width_2;
        UsedParameter ee_width_3;
        UsedParameter ee_width_4;

        UsedParameter phase_1;
        UsedParameter phase_2;
        UsedParameter phase_3;
        UsedParameter phase_4;

        UsedParameter a_cont;

        UsedParameter mass_D;
        UsedParameter mass_D_star;
        UsedParameter mass_D_s;
        UsedParameter mass_D_s_star;
        UsedParameter mass_D_one;
        UsedParameter mass_D_two_star;

        UsedParameter mass_j_psi;
        UsedParameter mass_psi_2s;
        UsedParameter ee_width_j_psi;
        UsedParameter ee_width_psi_2s;
        UsedParameter total_width_j_psi;
        UsedParameter total_width_psi_2s;

        UsedParameter mass_c;
        UsedParameter mass_b_MSbar;

        bool use_ks;
        bool use_one_loop;
        bool change_sign;

        int total_width_res;

        Implementation(const Parameters & p, const Options & o, ParameterUser & u) :
            mu(p["mu"], u),
            alpha_e(p["QED::alpha_e(m_b)"], u),
            alpha_s(p["QCD::alpha_s(MZ)"], u),
            ww_radius(p["ww_radius"], u),
            mass_1(p["mass::PSI(3770)"], u),
            mass_2(p["mass::PSI(4040)"], u),
            mass_3(p["mass::PSI(4160)"], u),
            mass_4(p["mass::PSI(4415)"], u),
            fit_width_1(p["ee->hadrons::fit_width(3770)"], u),
            fit_width_2(p["ee->hadrons::fit_width(4040)"], u),
            fit_width_3(p["ee->hadrons::fit_width(4160)"], u),
            fit_width_4(p["ee->hadrons::fit_width(4415)"], u),
            ee_width_1(p["ee->PSI(3770)::width"], u),
            ee_width_2(p["ee->PSI(4040)::width"], u),
            ee_width_3(p["ee->PSI(4160)::width"], u),
            ee_width_4(p["ee->PSI(4415)::width"], u),
            phase_1(p["ee->hadrons::phase(3770)"], u),
            phase_2(p["ee->hadrons::phase(4040)"], u),
            phase_3(p["ee->hadrons::phase(4160)"], u),
            phase_4(p["ee->hadrons::phase(4415)"], u),
            a_cont(p["ee->hadrons::a_cont"], u),
            mass_D(p["mass::D_0"], u),
            mass_D_star(p["mass::D^*_0"], u),
            mass_D_s(p["mass::D_s_charged"], u),
            mass_D_s_star(p["mass::D^*_s_charged"], u),
            mass_D_one(p["mass::D_1_0"], u),
            mass_D_two_star(p["mass::D^*_2_0"], u),
            mass_j_psi(p["mass::J/PSI"], u),
            mass_psi_2s(p["mass::PSI(2S)"], u),
            ee_width_j_psi(p["ee->J/PSI::width"], u),
            ee_width_psi_2s(p["ee->PSI(2S)::width"], u),
            total_width_j_psi(p["total_width::J/PSI"], u),
            total_width_psi_2s(p["total_width::PSI(2S)"], u),
            mass_c(p["mass::c"], u),
            mass_b_MSbar(p["mass::b(MSbar)"], u),
            use_ks(destringify<bool>(o.get("ks", "false"))),
            use_one_loop(destringify<bool>(o.get("one-loop", "false"))),
            change_sign(destringify<bool>(o.get("change-sign", "false"))),
            total_width_res(std::stoi(o.get("total-width-res", "-1")))
        {
            //
        }

        static double dispersion_int_cauchy(double t, void * p_);
        static double dispersion_int_semi_infty(double t, void * p_);

        struct dispersion_int_params
        {
            double s;
            double s_0;
            const Implementation<HadronicRatio<LowRecoil>> * hadronic_ratio;
        };

        double decay_momentum(const double & mass, const double & mass_p, const double & mass_k) const
        {
            double p = power_of<2>(mass)-power_of<2>(mass_p+mass_k);
            if (p >= 0.0)
            {
                p = std::sqrt((power_of<2>(mass) - power_of<2>(mass_p + mass_k))*(power_of<2>(mass) - power_of<2>(mass_p - mass_k))) / (2.0 * mass);
            }
            else
            {
                p = 0.0;
            }

            return p;
        }

        // cf. [], Eq. (6)
        double orbital_sum(const double & mass, const double & mass_p, const double & mass_k) const
        {
            double z = ww_radius * decay_momentum(mass, mass_p, mass_k);

            return z + power_of<3>(z) / (1.0 + power_of<2>(z)) + power_of<5>(z) / (9.0 + 3.0 * power_of<2>(z) + power_of<4>(z)) + power_of<7>(z) / (225.0 + 45.0 * power_of<2>(z) + 6.0 * power_of<4>(z) + power_of<6>(z) );
        }

        double resonance_width(const double & s, const double fit_width, const double & mass, const double & mass_p, const double & mass_k) const
        {
            return 2.0 * mass / (mass + std::sqrt(s)) * fit_width * orbital_sum(mass, mass_p, mass_k); // [Gev]
        }

        // cf. [], Eq. (9)
        double total_width(const double & s, const int resonance) const
        {
            double result = 0.0;

            switch (resonance)
            {
                case 1:
                    result  = resonance_width(s, fit_width_1(), mass_1(), mass_D(), mass_D());

                    result += (2.0 + 0.48) * ee_width_1();
                    break;
                case 2:
                    // DD
                    result  = resonance_width(s, fit_width_2(), mass_2(), mass_D(), mass_D());
                    // D*D*
                    result += resonance_width(s, fit_width_2(), mass_2(), mass_D_star(), mass_D_star());
                    // DD*
                    result += resonance_width(s, fit_width_2(), mass_2(), mass_D(), mass_D_star());
                    // DsDs
                    result += resonance_width(s, fit_width_2(), mass_2(), mass_D_s(), mass_D_s());

                    result += (2.0 + 0.66) * ee_width_2();
                    break;
                case 3:
                    // DD
                    result  = resonance_width(s, fit_width_3(), mass_3(), mass_D(), mass_D());
                    // D*D*
                    result += resonance_width(s, fit_width_3(), mass_3(), mass_D_star(), mass_D_star());
                    // DD*
                    result += resonance_width(s, fit_width_3(), mass_3(), mass_D(), mass_D_star());
                    // DsDs
                    result += resonance_width(s, fit_width_3(), mass_3(), mass_D_s(), mass_D_s());
                    // DsD*s
                    result += resonance_width(s, fit_width_3(), mass_3(), mass_D_s(), mass_D_s_star());

                    result += (2.0 + 0.72) * ee_width_3();
                    break;
                case 4:
                    // DD
                    result  = resonance_width(s, fit_width_4(), mass_4(), mass_D(), mass_D());
                    // D*D*
                    result += resonance_width(s, fit_width_4(), mass_4(), mass_D_star(), mass_D_star());
                    // DD*
                    result += resonance_width(s, fit_width_4(), mass_4(), mass_D(), mass_D_star());
                    // DsDs
                    result += resonance_width(s, fit_width_4(), mass_4(), mass_D_s(), mass_D_s());
                    // DsD*s
                    result += resonance_width(s, fit_width_4(), mass_4(), mass_D_s(), mass_D_s_star());
                    // D*sD*s
                    result += resonance_width(s, fit_width_4(), mass_4(), mass_D_s_star(), mass_D_s_star());
                    // DD(1)
                    result += resonance_width(s, fit_width_4(), mass_4(), mass_D(), mass_D_one());
                    // DD*(2)
                    result += resonance_width(s, fit_width_4(), mass_4(), mass_D(), mass_D_two_star());

                    result += (2.0 + 0.78) * ee_width_4();
                    break;
                default:
                    throw InternalError("Unknown Resonance.");
            }

            return result;
        }

        std::complex<double> amplitude_finale_state(const double & s, const int finale_state) const
        {
            static complex<double> i(0.0, 1.0);
            complex<double> result;
            switch (finale_state)
            {
                case 1:                 // DD Finale States
                    result  = mass_1() * std::sqrt(ee_width_1() * resonance_width(s, fit_width_1(), mass_1(), mass_D(), mass_D())) / (s - power_of<2>(mass_1()) + i * mass_1() * total_width(s, 1)) * std::exp(i * phase_1());
                    result += mass_2() * std::sqrt(ee_width_2() * resonance_width(s, fit_width_2(), mass_2(), mass_D(), mass_D())) / (s - power_of<2>(mass_2()) + i * mass_2() * total_width(s, 2)) * std::exp(i * phase_2());
                    result += mass_3() * std::sqrt(ee_width_3() * resonance_width(s, fit_width_3(), mass_3(), mass_D(), mass_D())) / (s - power_of<2>(mass_3()) + i * mass_3() * total_width(s, 3)) * std::exp(i * phase_3());
                    result += mass_4() * std::sqrt(ee_width_4() * resonance_width(s, fit_width_4(), mass_4(), mass_D(), mass_D())) / (s - power_of<2>(mass_4()) + i * mass_4() * total_width(s, 4)) * std::exp(i * phase_4());
                    break;
                case 2:                 // D*D*
                    result  = mass_2() * std::sqrt(ee_width_2() * resonance_width(s, fit_width_2(), mass_2(), mass_D_star(), mass_D_star())) / (s - power_of<2>(mass_2()) + i * mass_2() * total_width(s, 2)) * std::exp(i * phase_2());
                    result += mass_3() * std::sqrt(ee_width_3() * resonance_width(s, fit_width_3(), mass_3(), mass_D_star(), mass_D_star())) / (s - power_of<2>(mass_3()) + i * mass_3() * total_width(s, 3)) * std::exp(i * phase_3());
                    result += mass_4() * std::sqrt(ee_width_4() * resonance_width(s, fit_width_4(), mass_4(), mass_D_star(), mass_D_star())) / (s - power_of<2>(mass_4()) + i * mass_4() * total_width(s, 4)) * std::exp(i * phase_4());
                    break;
                case 3:                 // DD*
                    result  = mass_2() * std::sqrt(ee_width_2() * resonance_width(s, fit_width_2(), mass_2(), mass_D(), mass_D_star())) / (s - power_of<2>(mass_2()) + i * mass_2() * total_width(s, 2)) * std::exp(i * phase_2());
                    result += mass_3() * std::sqrt(ee_width_3() * resonance_width(s, fit_width_3(), mass_3(), mass_D(), mass_D_star())) / (s - power_of<2>(mass_3()) + i * mass_3() * total_width(s, 3)) * std::exp(i * phase_3());
                    result += mass_4() * std::sqrt(ee_width_4() * resonance_width(s, fit_width_4(), mass_4(), mass_D(), mass_D_star())) / (s - power_of<2>(mass_4()) + i * mass_4() * total_width(s, 4)) * std::exp(i * phase_4());
                    break;
                case 4:                // DsDs
                    result  = mass_2() * std::sqrt(ee_width_2() * resonance_width(s, fit_width_2(), mass_2(), mass_D_s(), mass_D_s())) / (s - power_of<2>(mass_2()) + i * mass_2() * total_width(s, 2)) * std::exp(i * phase_2());
                    result += mass_3() * std::sqrt(ee_width_3() * resonance_width(s, fit_width_3(), mass_3(), mass_D_s(), mass_D_s())) / (s - power_of<2>(mass_3()) + i * mass_3() * total_width(s, 3)) * std::exp(i * phase_3());
                    result += mass_4() * std::sqrt(ee_width_4() * resonance_width(s, fit_width_4(), mass_4(), mass_D_s(), mass_D_s())) / (s - power_of<2>(mass_4()) + i * mass_4() * total_width(s, 4)) * std::exp(i * phase_4());
                    break;
                case 5:                // DsD*s
                    result  = mass_3() * std::sqrt(ee_width_3() * resonance_width(s, fit_width_3(), mass_3(), mass_D_s(), mass_D_s_star())) / (s - power_of<2>(mass_3()) + i * mass_3() * total_width(s, 3)) * std::exp(i * phase_3());
                    result += mass_4() * std::sqrt(ee_width_4() * resonance_width(s, fit_width_4(), mass_4(), mass_D_s(), mass_D_s_star())) / (s - power_of<2>(mass_4()) + i * mass_4() * total_width(s, 4)) * std::exp(i * phase_4());
                    break;
                case 6:                // D*sD*s
                    result  = mass_4() * std::sqrt(ee_width_4() * resonance_width(s, fit_width_4(), mass_4(), mass_D_s_star(), mass_D_s_star())) / (s - power_of<2>(mass_4()) + i * mass_4() * total_width(s, 4)) * std::exp(i * phase_4());
                    break;
                case 7:                // DD(1)
                    result  = mass_4() * std::sqrt(ee_width_4() * resonance_width(s, fit_width_4(), mass_4(), mass_D(), mass_D_one())) / (s - power_of<2>(mass_4()) + i * mass_4() * total_width(s, 4)) * std::exp(i * phase_4());
                    break;
                case 8:                 // DD*(2)
                    result  = mass_4() * std::sqrt(ee_width_4() * resonance_width(s, fit_width_4(), mass_4(), mass_D(), mass_D_two_star())) / (s - power_of<2>(mass_4()) + i * mass_4() * total_width(s, 4)) * std::exp(i * phase_4());
                    break;
                default:
                    throw InternalError("Unknown finale state.");
            }

            return result;
        }

        double ratio(const double & q) const
        {
            complex<double> amp;
            double result = 0.0;
            double s = q * q;

            for (size_t i=1; i < 8; i++)
            {
                result += std::norm(amplitude_finale_state(s, i));
            }

            result *= 9.0 / power_of<2>(alpha_e());
            result += 2.16 + (1 - 4.0 * mass_D() * mass_D() / s)*((3.6 - 2.16) + a_cont() * 4.0 * mass_D() * mass_D() / s);

            return result;
        }

        double ratio_charm(const double & s) const
        {
            double result;

            if (4 * power_of<2>(mass_c()) <= s && s < 13.69)
            {
                result = ratio_narrow(s);
            }
            else if (13.69 <= s)
            {
                result = ratio(std::sqrt(s)) - 2.16;
            }
            else
            {
                throw InternalError("Invalid kinematic range for R_c. s = " + std::to_string(s));
            }

            return result;
        }

        double ratio_narrow(const double & s) const
        {
            const complex<double> i(0.0, 1.0);

            complex<double> breit_wigner;

            breit_wigner  = mass_j_psi() * ee_width_j_psi() / (s - power_of<2>(mass_j_psi()) + i * mass_j_psi() * total_width_j_psi());
            breit_wigner += mass_psi_2s() * ee_width_psi_2s() / (s - power_of<2>(mass_psi_2s()) + i * mass_psi_2s() * total_width_psi_2s());

            return -9.0 / power_of<2>(alpha_e()) * imag(breit_wigner);
        }

        double charm_loop_img(const double & s) const
        {
            double v = std::sqrt(1.0 - 4.0 * mass_c() * mass_c() / s);
            if (use_ks)
            {
                return imag(LongDistance::g_had_ccbar(s, mass_c()));
            }
            else if (use_one_loop)
            {
                return imag(CharmLoops::h(mu(), s, mass_c()));
            }

            double result = ( s < 23.04 ) ? M_PI / 3.0 * ratio_charm(s) : 2.0 * M_PI / 9.0 * (3.0 - power_of<2>(v)) * std::abs(v) * (1.0 + 4.0 / 3.0 * alpha_s() * (M_PI / (2.0 * v) - (3.0 / 4.0 + v / 4.0) * (M_PI / 2.0 - 3.0 / (4.0 * M_PI))));  // [LZ2014], p. 6

            // double result = ( s < 23.04 ) ? M_PI / 3.0 * ratio_charm(s) : M_PI / 3.0 * 1.02;
            return result;
        }

        double charm_loop_real(const double & s) const
        {
            size_t num_workspace = 1200;
            int gsl_status;

            if (s < 4 * power_of<2>(mass_c()))
            {
                throw InternalError("Invalid s for Re[h_c]. (Low Recoil!) s = " + std::to_string(s));
            }

            if (use_ks)
            {
                return real(LongDistance::g_had_ccbar(s, mass_c()));
            }
            else if (use_one_loop)
            {
                return real(CharmLoops::h(mu(), s, mass_c()));
            }

            // Turn off error handler
            gsl_set_error_handler_off();

            gsl_integration_workspace * w = gsl_integration_workspace_alloc(num_workspace);

            double result_cauchy, error_cauchy;
            struct dispersion_int_params params = { s, mass_c() * mass_c(), this }; // Substration Point s_0 @m_c (easiest value)
            gsl_function CauchyPrincipal;
            CauchyPrincipal.function = &dispersion_int_cauchy;
            CauchyPrincipal.params = &params;

            // First: Cauchy Principal Value
            gsl_status = gsl_integration_qawc(&CauchyPrincipal, power_of<2>(mass_j_psi() - 0.02), s + 10.0, s, 0.0, 1e-4, num_workspace, w, &result_cauchy, &error_cauchy);

            if (gsl_status)
            {
                Log::instance()->message("HadronicRatio::charm_loop_real", ll_warning)
                    << "GSL Integration Error: QAWC, errno: " << std::to_string(gsl_status) << ", result: " << std::to_string(result_cauchy) + "+/-" << std::to_string(error_cauchy) << "@s=" << std::to_string(s) <<"\n";
            }

            double result_semi, error_semi;
            gsl_function SemiFinite;
            SemiFinite.function = &dispersion_int_semi_infty;
            SemiFinite.params = &params;

            // Second: Semi-finite Integration
            gsl_status = 0;
            gsl_status = gsl_integration_qagiu(&SemiFinite, s + 10.0, 0.0, 1e-2, num_workspace, w, &result_semi, &error_semi);

            if (gsl_status)
            {
                // Some fancy error handling, I dunno...
                Log::instance()->message("HadronicRatio::charm_loop_real", ll_warning)
                    << "GSL Integration Error: QAGIU, errno: " << std::to_string(gsl_status) << ", result: " << std::to_string(result_semi) + "+/-" << std::to_string(error_semi) << "@s=" << std::to_string(s) <<"\n";
            }

            // Free Workspace
            gsl_integration_workspace_free(w);

            // Additional Sign for Integration
            double sign = (change_sign) ? -1.0 : 1.0;

            return (s - params.s_0) / M_PI * sign * (result_cauchy + result_semi) + real(CharmLoops::h(mu(), params.s_0, mass_c()));
        }
    };

    HadronicRatio<LowRecoil>::HadronicRatio(const Parameters & parameters, const Options & options) :
        PrivateImplementationPattern<HadronicRatio<LowRecoil>>(new Implementation<HadronicRatio<LowRecoil>>(parameters, options, *this))
    {
    }

    HadronicRatio<LowRecoil>::~HadronicRatio()
    {
    }

    double
    HadronicRatio<LowRecoil>::ratio(const double & q) const
    {
        return _imp->ratio(q);
    }

    double
    HadronicRatio<LowRecoil>::total_width(const double & mass) const
    {
        return (_imp->total_width_res > 0) ? _imp->total_width(mass, _imp->total_width_res) : 0.0;
    }

    double
    HadronicRatio<LowRecoil>::charm_loop_img(const double & s) const
    {
        return _imp->charm_loop_img(s);
    }

    double
    HadronicRatio<LowRecoil>::charm_loop_real(const double & s) const
    {
        return _imp->charm_loop_real(s);
    }

    complex<double>
    HadronicRatio<LowRecoil>::charm_loop(const double & s) const
    {
        return complex<double>(_imp->charm_loop_real(s), _imp->charm_loop_img(s));
    }

    double Implementation<HadronicRatio<LowRecoil>>::dispersion_int_cauchy(double t, void * p_)
    {
        struct dispersion_int_params * params = (struct dispersion_int_params *) p_;
        double s_0 = (params->s_0);

        return (params->hadronic_ratio)->charm_loop_img(t) / (t - s_0);
    }

    double Implementation<HadronicRatio<LowRecoil>>::dispersion_int_semi_infty(double t, void * p_)
    {
        struct dispersion_int_params * params = (struct dispersion_int_params *) p_;
        double s = (params->s);
        double s_0 = (params->s_0);

        return (params->hadronic_ratio)->charm_loop_img(t) / (t - s_0) / (t - s);
    }
}
