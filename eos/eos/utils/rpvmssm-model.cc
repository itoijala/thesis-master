/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2015 Dennis Loose
 *
 * This file is part of the EOS project. EOS is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * EOS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <eos/utils/rpvmssm-model.hh>
#include <eos/utils/model.hh>
#include <eos/utils/power_of.hh>
#include <eos/utils/standard-model.hh>
#include <eos/utils/stringify.hh>

#include <complex>

namespace eos
{
    namespace rpvmssmimplementation
    {
        complex<double> polar(const Parameter & abs, const Parameter & arg) { return std::polar(abs(), arg()); }
        complex<double> cartesian(const Parameter & re, const Parameter & im) { return complex<double>(re(), im()); }
    }

    RPVMSSMComponent<components::DeltaB1>::RPVMSSMComponent(const Parameters & p, const Options & o, ParameterUser & u) :
        _sm__deltab1(Model::make("SM", p, o)),
        _wilsonscan__deltab1(Model::make("WilsonScan", p, o)),
        _G_F(p["G_Fermi"], u),
        _alpha_e(p["QED::alpha_e(m_b)"], u),
        _m_snu(p["m_snu"], u),
        _abs_LAMBDA_e(p["Abs{LAMBDA_e}"]),
        _arg_LAMBDA_e(p["Arg{LAMBDA_e}"]),
        _abs_LAMBDA_prime_e(p["Abs{LAMBDA_prime_e}"]),
        _arg_LAMBDA_prime_e(p["Arg{LAMBDA_prime_e}"]),
        _re_LAMBDA_e(p["Re{LAMBDA_e}"]),
        _im_LAMBDA_e(p["Im{LAMBDA_e}"]),
        _re_LAMBDA_prime_e(p["Re{LAMBDA_prime_e}"]),
        _im_LAMBDA_prime_e(p["Im{LAMBDA_prime_e}"]),
        _abs_LAMBDA_mu(p["Abs{LAMBDA_mu}"]),
        _arg_LAMBDA_mu(p["Arg{LAMBDA_mu}"]),
        _abs_LAMBDA_prime_mu(p["Abs{LAMBDA_prime_mu}"]),
        _arg_LAMBDA_prime_mu(p["Arg{LAMBDA_prime_mu}"]),
        _re_LAMBDA_mu(p["Re{LAMBDA_mu}"]),
        _im_LAMBDA_mu(p["Im{LAMBDA_mu}"]),
        _re_LAMBDA_prime_mu(p["Re{LAMBDA_prime_mu}"]),
        _im_LAMBDA_prime_mu(p["Im{LAMBDA_prime_mu}"])
    {
        for (auto & p : *_wilsonscan__deltab1)
        {
            u.uses(p);
        }

        if ("polar" == o.get("scan-mode", "polar"))
        {
            _LAMBDA_e = std::bind(&rpvmssmimplementation::polar, _abs_LAMBDA_e, _arg_LAMBDA_e);
            u.uses(_abs_LAMBDA_e.id()); u.uses(_arg_LAMBDA_e.id());

            _LAMBDA_prime_e = std::bind(&rpvmssmimplementation::polar, _abs_LAMBDA_prime_e, _arg_LAMBDA_prime_e);
            u.uses(_abs_LAMBDA_prime_e.id()); u.uses(_arg_LAMBDA_prime_e.id());

            _LAMBDA_mu = std::bind(&rpvmssmimplementation::polar, _abs_LAMBDA_mu, _arg_LAMBDA_mu);
            u.uses(_abs_LAMBDA_mu.id()); u.uses(_arg_LAMBDA_mu.id());

            _LAMBDA_prime_mu = std::bind(&rpvmssmimplementation::polar, _abs_LAMBDA_prime_mu, _arg_LAMBDA_prime_mu);
            u.uses(_abs_LAMBDA_prime_mu.id()); u.uses(_arg_LAMBDA_prime_mu.id());
        }
        else if ("cartesian" == o.get("scan-mode", "polar"))
        {
            _LAMBDA_e = std::bind(&rpvmssmimplementation::cartesian, _re_LAMBDA_e, _im_LAMBDA_e);
            u.uses(_re_LAMBDA_e.id()); u.uses(_im_LAMBDA_e.id());

            _LAMBDA_prime_e = std::bind(&rpvmssmimplementation::cartesian, _re_LAMBDA_prime_e, _im_LAMBDA_prime_e);
            u.uses(_re_LAMBDA_prime_e.id()); u.uses(_im_LAMBDA_prime_e.id());

            _LAMBDA_mu = std::bind(&rpvmssmimplementation::cartesian, _re_LAMBDA_mu, _im_LAMBDA_mu);
            u.uses(_re_LAMBDA_mu.id()); u.uses(_im_LAMBDA_mu.id());

            _LAMBDA_prime_mu = std::bind(&rpvmssmimplementation::cartesian, _re_LAMBDA_prime_mu, _im_LAMBDA_prime_mu);
            u.uses(_re_LAMBDA_prime_mu.id()); u.uses(_im_LAMBDA_prime_mu.id());
        }
        else
        {
            throw InternalError("scan-mode = '" + stringify(o.get("scan-mode", "polar")) + "' is not a valid scan mode for LeptoquarkModel");
        }
    }

    WilsonCoefficients<BToS>
    RPVMSSMComponent<components::DeltaB1>::wilson_coefficients_b_to_s(const std::string & lepton, const bool & cp_conjugate) const
    {
        if (lepton != "e" && lepton != "mu" && lepton != "tau")
        {
            throw InternalError("RPVMSSMModel: '" + lepton + "' os not a valid lepton flavour");
        }

        WilsonCoefficients<BToS> wc = _wilsonscan__deltab1->wilson_coefficients_b_to_s(lepton, false);

        if (lepton == "e")
        {
            //const complex<double> C_S = 4 * M_PI / (_alpha_e() * _sm__deltab1->ckm_tb() * conj(_sm__deltab1->ckm_ts()) * _G_F() * power_of<2>(_m_snu())) * _LAMBDA_e();
            //const complex<double> C_S_prime = 4 * M_PI / (_alpha_e() * _sm__deltab1->ckm_tb() * conj(_sm__deltab1->ckm_ts()) * _G_F() * power_of<2>(_m_snu())) * _LAMBDA_prime_e();
            const complex<double> C_S = _LAMBDA_e();
            const complex<double> C_S_prime = _LAMBDA_prime_e();

            wc._scalar_tensor_coefficients[0] = C_S;
            wc._scalar_tensor_coefficients[1] = C_S_prime;
            wc._scalar_tensor_coefficients[2] = -C_S;
            wc._scalar_tensor_coefficients[3] = C_S_prime;
        }
        else if (lepton == "mu")
        {
            //const complex<double> C_S = 4 * M_PI / (_alpha_e() * _sm__deltab1->ckm_tb() * conj(_sm__deltab1->ckm_ts()) * _G_F() * power_of<2>(_m_snu())) * _LAMBDA_mu();
            //const complex<double> C_S_prime = 4 * M_PI / (_alpha_e() * _sm__deltab1->ckm_tb() * conj(_sm__deltab1->ckm_ts()) * _G_F() * power_of<2>(_m_snu())) * _LAMBDA_prime_mu();
            const complex<double> C_S =  _LAMBDA_mu();
            const complex<double> C_S_prime = _LAMBDA_prime_mu();

            wc._scalar_tensor_coefficients[0] = C_S;
            wc._scalar_tensor_coefficients[1] = C_S_prime;
            wc._scalar_tensor_coefficients[2] = -C_S;
            wc._scalar_tensor_coefficients[3] = C_S_prime;
        }


        if (cp_conjugate)
        {
            for (auto & c : wc._sm_like_coefficients)
            {
                c = conj(c);
            }

            for (auto & c : wc._primed_coefficients)
            {
                c = conj(c);
            }

            for (auto & c : wc._scalar_tensor_coefficients)
            {
                c = conj(c);
            }
        }

        return wc;
    }

    RPVMSSMModel::RPVMSSMModel(const Parameters & p, const Options & o) :
        SMComponent<components::CKM>(p, *this),
        SMComponent<components::QCD>(p, *this),
        RPVMSSMComponent<components::DeltaB1>(p, o, *this),
        SMComponent<components::DeltaB2>(p, *this)
    {
    }

    RPVMSSMModel::~RPVMSSMModel()
    {
    }

    std::shared_ptr<Model>
    RPVMSSMModel::make(const Parameters & parameters, const Options & options)
    {
        return std::shared_ptr<Model>(new RPVMSSMModel(parameters, options));
    }
}
