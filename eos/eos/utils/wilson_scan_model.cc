/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2011, 2013 Danny van Dyk
 * Copyright (c) 2014 Frederik Beaujean
 * Copyright (c) 2014 Christoph Bobeth
 *
 * This file is part of the EOS project. EOS is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * EOS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <eos/utils/complex.hh>
#include <eos/utils/log.hh>
#include <eos/utils/matrix.hh>
#include <eos/utils/power_of.hh>
#include <eos/utils/private_implementation_pattern-impl.hh>
#include <eos/utils/qcd.hh>
#include <eos/utils/wilson_scan_model.hh>

#include <cmath>

namespace eos
{
    using std::sqrt;

    namespace wcimplementation
    {
        complex<double> polar(const Parameter & abs, const Parameter & arg) { return std::polar(abs(), arg()); }
        complex<double> cartesian(const Parameter & re, const Parameter & im) { return complex<double>(re(), im()); }
    }

    WilsonScanComponent<components::DeltaB1>::WilsonScanComponent(const Parameters & p, const Options & o, ParameterUser & u) :
        _lepton_dep("yes" == o.get("lepton-dep", "no")),
        _alpha_s_Z__deltab1(p["QCD::alpha_s(MZ)"], u),
        _mu_b__deltab1(p["QCD::mu_b"], u),
        _m_Z__deltab1(p["mass::Z"], u),
        _mu__deltab1(p["mu"], u),
        _c1(p["c1"], u),
        _c2(p["c2"], u),
        _c3(p["c3"], u),
        _c4(p["c4"], u),
        _c5(p["c5"], u),
        _c6(p["c6"], u),
        _abs_c7(p["Abs{c7}"]),
        _arg_c7(p["Arg{c7}"]),
        _re_c7(p["Re{c7}"]),
        _im_c7(p["Im{c7}"]),
        _c8(p["c8"], u),
        _abs_c9(p["Abs{c9}"]),
        _arg_c9(p["Arg{c9}"]),
        _re_c9(p["Re{c9}"]),
        _im_c9(p["Im{c9}"]),
        _abs_c10(p["Abs{c10}"]),
        _arg_c10(p["Arg{c10}"]),
        _re_c10(p["Re{c10}"]),
        _im_c10(p["Im{c10}"]),
        _abs_c7prime(p["Abs{c7'}"]),
        _arg_c7prime(p["Arg{c7'}"]),
        _re_c7prime(p["Re{c7'}"]),
        _im_c7prime(p["Im{c7'}"]),
        _c8prime(p["c8'"], u),
        _abs_c9prime(p["Abs{c9'}"]),
        _arg_c9prime(p["Arg{c9'}"]),
        _re_c9prime(p["Re{c9'}"]),
        _im_c9prime(p["Im{c9'}"]),
        _abs_c10prime(p["Abs{c10'}"]),
        _arg_c10prime(p["Arg{c10'}"]),
        _re_c10prime(p["Re{c10'}"]),
        _im_c10prime(p["Im{c10'}"]),
        _abs_cS(p["Abs{cS}"]),
        _arg_cS(p["Arg{cS}"]),
        _re_cS(p["Re{cS}"]),
        _im_cS(p["Im{cS}"]),
        _abs_cSprime(p["Abs{cS'}"]),
        _arg_cSprime(p["Arg{cS'}"]),
        _re_cSprime(p["Re{cS'}"]),
        _im_cSprime(p["Im{cS'}"]),
        _abs_cP(p["Abs{cP}"]),
        _arg_cP(p["Arg{cP}"]),
        _re_cP(p["Re{cP}"]),
        _im_cP(p["Im{cP}"]),
        _abs_cPprime(p["Abs{cP'}"]),
        _arg_cPprime(p["Arg{cP'}"]),
        _re_cPprime(p["Re{cP'}"]),
        _im_cPprime(p["Im{cP'}"]),
        _abs_cT(p["Abs{cT}"]),
        _arg_cT(p["Arg{cT}"]),
        _re_cT(p["Re{cT}"]),
        _im_cT(p["Im{cT}"]),
        _abs_cT5(p["Abs{cT5}"]),
        _arg_cT5(p["Arg{cT5}"]),
        _re_cT5(p["Re{cT5}"]),
        _im_cT5(p["Im{cT5}"]),

        // lepton dependent wilson coefficients
        _abs_c9_e(p["Abs{c9_e}"]),
        _arg_c9_e(p["Arg{c9_e}"]),
        _re_c9_e(p["Re{c9_e}"]),
        _im_c9_e(p["Im{c9_e}"]),
        _abs_c10_e(p["Abs{c10_e}"]),
        _arg_c10_e(p["Arg{c10_e}"]),
        _re_c10_e(p["Re{c10_e}"]),
        _im_c10_e(p["Im{c10_e}"]),
        _abs_c9prime_e(p["Abs{c9_e'}"]),
        _arg_c9prime_e(p["Arg{c9_e'}"]),
        _re_c9prime_e(p["Re{c9_e'}"]),
        _im_c9prime_e(p["Im{c9_e'}"]),
        _abs_c10prime_e(p["Abs{c10_e'}"]),
        _arg_c10prime_e(p["Arg{c10_e'}"]),
        _re_c10prime_e(p["Re{c10_e'}"]),
        _im_c10prime_e(p["Im{c10_e'}"]),
        _abs_cS_e(p["Abs{cS_e}"]),
        _arg_cS_e(p["Arg{cS_e}"]),
        _re_cS_e(p["Re{cS_e}"]),
        _im_cS_e(p["Im{cS_e}"]),
        _abs_cSprime_e(p["Abs{cS_e'}"]),
        _arg_cSprime_e(p["Arg{cS_e'}"]),
        _re_cSprime_e(p["Re{cS_e'}"]),
        _im_cSprime_e(p["Im{cS_e'}"]),
        _abs_cP_e(p["Abs{cP_e}"]),
        _arg_cP_e(p["Arg{cP_e}"]),
        _re_cP_e(p["Re{cP_e}"]),
        _im_cP_e(p["Im{cP_e}"]),
        _abs_cPprime_e(p["Abs{cP_e'}"]),
        _arg_cPprime_e(p["Arg{cP_e'}"]),
        _re_cPprime_e(p["Re{cP_e'}"]),
        _im_cPprime_e(p["Im{cP_e'}"]),
        _abs_cT_e(p["Abs{cT_e}"]),
        _arg_cT_e(p["Arg{cT_e}"]),
        _re_cT_e(p["Re{cT_e}"]),
        _im_cT_e(p["Im{cT_e}"]),
        _abs_cT5_e(p["Abs{cT5_e}"]),
        _arg_cT5_e(p["Arg{cT5_e}"]),
        _re_cT5_e(p["Re{cT5_e}"]),
        _im_cT5_e(p["Im{cT5_e}"]),

        _abs_c9_mu(p["Abs{c9_mu}"]),
        _arg_c9_mu(p["Arg{c9_mu}"]),
        _re_c9_mu(p["Re{c9_mu}"]),
        _im_c9_mu(p["Im{c9_mu}"]),
        _abs_c10_mu(p["Abs{c10_mu}"]),
        _arg_c10_mu(p["Arg{c10_mu}"]),
        _re_c10_mu(p["Re{c10_mu}"]),
        _im_c10_mu(p["Im{c10_mu}"]),
        _abs_c9prime_mu(p["Abs{c9_mu'}"]),
        _arg_c9prime_mu(p["Arg{c9_mu'}"]),
        _re_c9prime_mu(p["Re{c9_mu'}"]),
        _im_c9prime_mu(p["Im{c9_mu'}"]),
        _abs_c10prime_mu(p["Abs{c10_mu'}"]),
        _arg_c10prime_mu(p["Arg{c10_mu'}"]),
        _re_c10prime_mu(p["Re{c10_mu'}"]),
        _im_c10prime_mu(p["Im{c10_mu'}"]),
        _abs_cS_mu(p["Abs{cS_mu}"]),
        _arg_cS_mu(p["Arg{cS_mu}"]),
        _re_cS_mu(p["Re{cS_mu}"]),
        _im_cS_mu(p["Im{cS_mu}"]),
        _abs_cSprime_mu(p["Abs{cS_mu'}"]),
        _arg_cSprime_mu(p["Arg{cS_mu'}"]),
        _re_cSprime_mu(p["Re{cS_mu'}"]),
        _im_cSprime_mu(p["Im{cS_mu'}"]),
        _abs_cP_mu(p["Abs{cP_mu}"]),
        _arg_cP_mu(p["Arg{cP_mu}"]),
        _re_cP_mu(p["Re{cP_mu}"]),
        _im_cP_mu(p["Im{cP_mu}"]),
        _abs_cPprime_mu(p["Abs{cP_mu'}"]),
        _arg_cPprime_mu(p["Arg{cP_mu'}"]),
        _re_cPprime_mu(p["Re{cP_mu'}"]),
        _im_cPprime_mu(p["Im{cP_mu'}"]),
        _abs_cT_mu(p["Abs{cT_mu}"]),
        _arg_cT_mu(p["Arg{cT_mu}"]),
        _re_cT_mu(p["Re{cT_mu}"]),
        _im_cT_mu(p["Im{cT_mu}"]),
        _abs_cT5_mu(p["Abs{cT5_mu}"]),
        _arg_cT5_mu(p["Arg{cT5_mu}"]),
        _re_cT5_mu(p["Re{cT5_mu}"]),
        _im_cT5_mu(p["Im{cT5_mu}"]),

        _abs_c9_tau(p["Abs{c9_tau}"]),
        _arg_c9_tau(p["Arg{c9_tau}"]),
        _re_c9_tau(p["Re{c9_tau}"]),
        _im_c9_tau(p["Im{c9_tau}"]),
        _abs_c10_tau(p["Abs{c10_tau}"]),
        _arg_c10_tau(p["Arg{c10_tau}"]),
        _re_c10_tau(p["Re{c10_tau}"]),
        _im_c10_tau(p["Im{c10_tau}"]),
        _abs_c9prime_tau(p["Abs{c9_tau'}"]),
        _arg_c9prime_tau(p["Arg{c9_tau'}"]),
        _re_c9prime_tau(p["Re{c9_tau'}"]),
        _im_c9prime_tau(p["Im{c9_tau'}"]),
        _abs_c10prime_tau(p["Abs{c10_tau'}"]),
        _arg_c10prime_tau(p["Arg{c10_tau'}"]),
        _re_c10prime_tau(p["Re{c10_tau'}"]),
        _im_c10prime_tau(p["Im{c10_tau'}"]),
        _abs_cS_tau(p["Abs{cS_tau}"]),
        _arg_cS_tau(p["Arg{cS_tau}"]),
        _re_cS_tau(p["Re{cS_tau}"]),
        _im_cS_tau(p["Im{cS_tau}"]),
        _abs_cSprime_tau(p["Abs{cS_tau'}"]),
        _arg_cSprime_tau(p["Arg{cS_tau'}"]),
        _re_cSprime_tau(p["Re{cS_tau'}"]),
        _im_cSprime_tau(p["Im{cS_tau'}"]),
        _abs_cP_tau(p["Abs{cP_tau}"]),
        _arg_cP_tau(p["Arg{cP_tau}"]),
        _re_cP_tau(p["Re{cP_tau}"]),
        _im_cP_tau(p["Im{cP_tau}"]),
        _abs_cPprime_tau(p["Abs{cP_tau'}"]),
        _arg_cPprime_tau(p["Arg{cP_tau'}"]),
        _re_cPprime_tau(p["Re{cP_tau'}"]),
        _im_cPprime_tau(p["Im{cP_tau'}"]),
        _abs_cT_tau(p["Abs{cT_tau}"]),
        _arg_cT_tau(p["Arg{cT_tau}"]),
        _re_cT_tau(p["Re{cT_tau}"]),
        _im_cT_tau(p["Im{cT_tau}"]),
        _abs_cT5_tau(p["Abs{cT5_tau}"]),
        _arg_cT5_tau(p["Arg{cT5_tau}"]),
        _re_cT5_tau(p["Re{cT5_tau}"]),
        _im_cT5_tau(p["Im{cT5_tau}"])

    {
        if ("polar" == o.get("scan-mode", "polar"))
        {
            _c7 = std::bind(&wcimplementation::polar, _abs_c7, _arg_c7);
            u.uses(_abs_c7.id()); u.uses(_arg_c7.id());
            _c7prime = std::bind(&wcimplementation::polar, _abs_c7prime, _arg_c7prime);
            u.uses(_abs_c7prime.id()); u.uses(_arg_c7prime.id());

            if (!_lepton_dep)
            {
                _c9 = std::bind(&wcimplementation::polar, _abs_c9, _arg_c9);
                u.uses(_abs_c9.id()); u.uses(_arg_c9.id());
                _c10 = std::bind(&wcimplementation::polar, _abs_c10, _arg_c10);
                u.uses(_abs_c10.id()); u.uses(_arg_c10.id());
                _c9prime = std::bind(&wcimplementation::polar, _abs_c9prime, _arg_c9prime);
                u.uses(_abs_c9prime.id()); u.uses(_arg_c9prime.id());
                _c10prime = std::bind(&wcimplementation::polar, _abs_c10prime, _arg_c10prime);
                u.uses(_abs_c10prime.id()); u.uses(_arg_c10prime.id());
                _cS = std::bind(&wcimplementation::polar, _abs_cS, _arg_cS);
                u.uses(_abs_cS.id()); u.uses(_arg_cS.id());
                _cSprime = std::bind(&wcimplementation::polar, _abs_cSprime, _arg_cSprime);
                u.uses(_abs_cSprime.id()); u.uses(_arg_cSprime.id());
                _cP = std::bind(&wcimplementation::polar, _abs_cP, _arg_cP);
                u.uses(_abs_cP.id()); u.uses(_arg_cP.id());
                _cPprime = std::bind(&wcimplementation::polar, _abs_cPprime, _arg_cPprime);
                u.uses(_abs_cPprime.id()); u.uses(_arg_cPprime.id());
                _cT = std::bind(&wcimplementation::polar, _abs_cT, _arg_cT);
                u.uses(_abs_cT.id()); u.uses(_arg_cT.id());
                _cT5 = std::bind(&wcimplementation::polar, _abs_cT5, _arg_cT5);
                u.uses(_abs_cT5.id()); u.uses(_arg_cT5.id());
            }
            else
            {
                _c9_e = std::bind(&wcimplementation::polar, _abs_c9_e, _arg_c9_e);
                u.uses(_abs_c9_e.id()); u.uses(_arg_c9_e.id());
                _c10_e = std::bind(&wcimplementation::polar, _abs_c10_e, _arg_c10_e);
                u.uses(_abs_c10_e.id()); u.uses(_arg_c10_e.id());
                _c9prime_e = std::bind(&wcimplementation::polar, _abs_c9prime_e, _arg_c9prime_e);
                u.uses(_abs_c9prime_e.id()); u.uses(_arg_c9prime_e.id());
                _c10prime_e = std::bind(&wcimplementation::polar, _abs_c10prime_e, _arg_c10prime_e);
                u.uses(_abs_c10prime_e.id()); u.uses(_arg_c10prime_e.id());
                _cS_e = std::bind(&wcimplementation::polar, _abs_cS_e, _arg_cS_e);
                u.uses(_abs_cS_e.id()); u.uses(_arg_cS_e.id());
                _cSprime_e = std::bind(&wcimplementation::polar, _abs_cSprime_e, _arg_cSprime_e);
                u.uses(_abs_cSprime_e.id()); u.uses(_arg_cSprime_e.id());
                _cP_e = std::bind(&wcimplementation::polar, _abs_cP_e, _arg_cP_e);
                u.uses(_abs_cP_e.id()); u.uses(_arg_cP_e.id());
                _cPprime_e = std::bind(&wcimplementation::polar, _abs_cPprime_e, _arg_cPprime_e);
                u.uses(_abs_cPprime_e.id()); u.uses(_arg_cPprime_e.id());
                _cT_e = std::bind(&wcimplementation::polar, _abs_cT_e, _arg_cT_e);
                u.uses(_abs_cT_e.id()); u.uses(_arg_cT_e.id());
                _cT5_e = std::bind(&wcimplementation::polar, _abs_cT5_e, _arg_cT5_e);
                u.uses(_abs_cT5_e.id()); u.uses(_arg_cT5_e.id());

                _c9_mu = std::bind(&wcimplementation::polar, _abs_c9_mu, _arg_c9_mu);
                u.uses(_abs_c9_mu.id()); u.uses(_arg_c9_mu.id());
                _c10_mu = std::bind(&wcimplementation::polar, _abs_c10_mu, _arg_c10_mu);
                u.uses(_abs_c10_mu.id()); u.uses(_arg_c10_mu.id());
                _c9prime_mu = std::bind(&wcimplementation::polar, _abs_c9prime_mu, _arg_c9prime_mu);
                u.uses(_abs_c9prime_mu.id()); u.uses(_arg_c9prime_mu.id());
                _c10prime_mu = std::bind(&wcimplementation::polar, _abs_c10prime_mu, _arg_c10prime_mu);
                u.uses(_abs_c10prime_mu.id()); u.uses(_arg_c10prime_mu.id());
                _cS_mu = std::bind(&wcimplementation::polar, _abs_cS_mu, _arg_cS_mu);
                u.uses(_abs_cS_mu.id()); u.uses(_arg_cS_mu.id());
                _cSprime_mu = std::bind(&wcimplementation::polar, _abs_cSprime_mu, _arg_cSprime_mu);
                u.uses(_abs_cSprime_mu.id()); u.uses(_arg_cSprime_mu.id());
                _cP_mu = std::bind(&wcimplementation::polar, _abs_cP_mu, _arg_cP_mu);
                u.uses(_abs_cP_mu.id()); u.uses(_arg_cP_mu.id());
                _cPprime_mu = std::bind(&wcimplementation::polar, _abs_cPprime_mu, _arg_cPprime_mu);
                u.uses(_abs_cPprime_mu.id()); u.uses(_arg_cPprime_mu.id());
                _cT_mu = std::bind(&wcimplementation::polar, _abs_cT_mu, _arg_cT_mu);
                u.uses(_abs_cT_mu.id()); u.uses(_arg_cT_mu.id());
                _cT5_mu = std::bind(&wcimplementation::polar, _abs_cT5_mu, _arg_cT5_mu);
                u.uses(_abs_cT5_mu.id()); u.uses(_arg_cT5_mu.id());

                _c9_tau = std::bind(&wcimplementation::polar, _abs_c9_tau, _arg_c9_tau);
                u.uses(_abs_c9_tau.id()); u.uses(_arg_c9_tau.id());
                _c10_tau = std::bind(&wcimplementation::polar, _abs_c10_tau, _arg_c10_tau);
                u.uses(_abs_c10_tau.id()); u.uses(_arg_c10_tau.id());
                _c9prime_tau = std::bind(&wcimplementation::polar, _abs_c9prime_tau, _arg_c9prime_tau);
                u.uses(_abs_c9prime_tau.id()); u.uses(_arg_c9prime_tau.id());
                _c10prime_tau = std::bind(&wcimplementation::polar, _abs_c10prime_tau, _arg_c10prime_tau);
                u.uses(_abs_c10prime_tau.id()); u.uses(_arg_c10prime_tau.id());
                _cS_tau = std::bind(&wcimplementation::polar, _abs_cS_tau, _arg_cS_tau);
                u.uses(_abs_cS_tau.id()); u.uses(_arg_cS_tau.id());
                _cSprime_tau = std::bind(&wcimplementation::polar, _abs_cSprime_tau, _arg_cSprime_tau);
                u.uses(_abs_cSprime_tau.id()); u.uses(_arg_cSprime_tau.id());
                _cP_tau = std::bind(&wcimplementation::polar, _abs_cP_tau, _arg_cP_tau);
                u.uses(_abs_cP_tau.id()); u.uses(_arg_cP_tau.id());
                _cPprime_tau = std::bind(&wcimplementation::polar, _abs_cPprime_tau, _arg_cPprime_tau);
                u.uses(_abs_cPprime_tau.id()); u.uses(_arg_cPprime_tau.id());
                _cT_tau = std::bind(&wcimplementation::polar, _abs_cT_tau, _arg_cT_tau);
                u.uses(_abs_cT_tau.id()); u.uses(_arg_cT_tau.id());
                _cT5_tau = std::bind(&wcimplementation::polar, _abs_cT5_tau, _arg_cT5_tau);
                u.uses(_abs_cT5_tau.id()); u.uses(_arg_cT5_tau.id());
            }
        }
        else if ("cartesian" == o.get("scan-mode", "polar"))
        {
            _c7 = std::bind(&wcimplementation::cartesian, _re_c7, _im_c7);
            u.uses(_re_c7.id()); u.uses(_im_c7.id());
            _c7prime = std::bind(&wcimplementation::cartesian, _re_c7prime, _im_c7prime);
            u.uses(_re_c7prime.id()); u.uses(_im_c7prime.id());

            if (!_lepton_dep)
            {
                _c9 = std::bind(&wcimplementation::cartesian, _re_c9, _im_c9);
                u.uses(_re_c9.id()); u.uses(_im_c9.id());
                _c10 = std::bind(&wcimplementation::cartesian, _re_c10, _im_c10);
                u.uses(_re_c10.id()); u.uses(_im_c10.id());
                _c9prime = std::bind(&wcimplementation::cartesian, _re_c9prime, _im_c9prime);
                u.uses(_re_c9prime.id()); u.uses(_im_c9prime.id());
                _c10prime = std::bind(&wcimplementation::cartesian, _re_c10prime, _im_c10prime);
                u.uses(_re_c10prime.id()); u.uses(_im_c10prime.id());
                _cS = std::bind(&wcimplementation::cartesian, _re_cS, _im_cS);
                u.uses(_re_cS.id()); u.uses(_im_cS.id());
                _cSprime = std::bind(&wcimplementation::cartesian, _re_cSprime, _im_cSprime);
                u.uses(_re_cSprime.id()); u.uses(_im_cSprime.id());
                _cP = std::bind(&wcimplementation::cartesian, _re_cP, _im_cP);
                u.uses(_re_cP.id()); u.uses(_im_cP.id());
                _cPprime = std::bind(&wcimplementation::cartesian, _re_cPprime, _im_cPprime);
                u.uses(_re_cPprime.id()); u.uses(_im_cPprime.id());
                _cT = std::bind(&wcimplementation::cartesian, _re_cT, _im_cT);
                u.uses(_re_cT.id()); u.uses(_im_cT.id());
                _cT5 = std::bind(&wcimplementation::cartesian, _re_cT5, _im_cT5);
                u.uses(_re_cT5.id()); u.uses(_im_cT5.id());
            }
            else
            {
                _c9_e = std::bind(&wcimplementation::cartesian, _re_c9_e, _im_c9_e);
                u.uses(_re_c9_e.id()); u.uses(_im_c9_e.id());
                _c10_e = std::bind(&wcimplementation::cartesian, _re_c10_e, _im_c10_e);
                u.uses(_re_c10_e.id()); u.uses(_im_c10_e.id());
                _c9prime_e = std::bind(&wcimplementation::cartesian, _re_c9prime_e, _im_c9prime_e);
                u.uses(_re_c9prime_e.id()); u.uses(_im_c9prime_e.id());
                _c10prime_e = std::bind(&wcimplementation::cartesian, _re_c10prime_e, _im_c10prime_e);
                u.uses(_re_c10prime_e.id()); u.uses(_im_c10prime_e.id());
                _cS_e = std::bind(&wcimplementation::cartesian, _re_cS_e, _im_cS_e);
                u.uses(_re_cS_e.id()); u.uses(_im_cS_e.id());
                _cSprime_e = std::bind(&wcimplementation::cartesian, _re_cSprime_e, _im_cSprime_e);
                u.uses(_re_cSprime_e.id()); u.uses(_im_cSprime_e.id());
                _cP_e = std::bind(&wcimplementation::cartesian, _re_cP_e, _im_cP_e);
                u.uses(_re_cP_e.id()); u.uses(_im_cP_e.id());
                _cPprime_e = std::bind(&wcimplementation::cartesian, _re_cPprime_e, _im_cPprime_e);
                u.uses(_re_cPprime_e.id()); u.uses(_im_cPprime_e.id());
                _cT_e = std::bind(&wcimplementation::cartesian, _re_cT_e, _im_cT_e);
                u.uses(_re_cT_e.id()); u.uses(_im_cT_e.id());
                _cT5_e = std::bind(&wcimplementation::cartesian, _re_cT5_e, _im_cT5_e);
                u.uses(_re_cT5_e.id()); u.uses(_im_cT5_e.id());

                _c9_mu = std::bind(&wcimplementation::cartesian, _re_c9_mu, _im_c9_mu);
                u.uses(_re_c9_mu.id()); u.uses(_im_c9_mu.id());
                _c10_mu = std::bind(&wcimplementation::cartesian, _re_c10_mu, _im_c10_mu);
                u.uses(_re_c10_mu.id()); u.uses(_im_c10_mu.id());
                _c9prime_mu = std::bind(&wcimplementation::cartesian, _re_c9prime_mu, _im_c9prime_mu);
                u.uses(_re_c9prime_mu.id()); u.uses(_im_c9prime_mu.id());
                _c10prime_mu = std::bind(&wcimplementation::cartesian, _re_c10prime_mu, _im_c10prime_mu);
                u.uses(_re_c10prime_mu.id()); u.uses(_im_c10prime_mu.id());
                _cS_mu = std::bind(&wcimplementation::cartesian, _re_cS_mu, _im_cS_mu);
                u.uses(_re_cS_mu.id()); u.uses(_im_cS_mu.id());
                _cSprime_mu = std::bind(&wcimplementation::cartesian, _re_cSprime_mu, _im_cSprime_mu);
                u.uses(_re_cSprime_mu.id()); u.uses(_im_cSprime_mu.id());
                _cP_mu = std::bind(&wcimplementation::cartesian, _re_cP_mu, _im_cP_mu);
                u.uses(_re_cP_mu.id()); u.uses(_im_cP_mu.id());
                _cPprime_mu = std::bind(&wcimplementation::cartesian, _re_cPprime_mu, _im_cPprime_mu);
                u.uses(_re_cPprime_mu.id()); u.uses(_im_cPprime_mu.id());
                _cT_mu = std::bind(&wcimplementation::cartesian, _re_cT_mu, _im_cT_mu);
                u.uses(_re_cT_mu.id()); u.uses(_im_cT_mu.id());
                _cT5_mu = std::bind(&wcimplementation::cartesian, _re_cT5_mu, _im_cT5_mu);
                u.uses(_re_cT5_mu.id()); u.uses(_im_cT5_mu.id());

                _c9_tau = std::bind(&wcimplementation::cartesian, _re_c9_tau, _im_c9_tau);
                u.uses(_re_c9_tau.id()); u.uses(_im_c9_tau.id());
                _c10_tau = std::bind(&wcimplementation::cartesian, _re_c10_tau, _im_c10_tau);
                u.uses(_re_c10_tau.id()); u.uses(_im_c10_tau.id());
                _c9prime_tau = std::bind(&wcimplementation::cartesian, _re_c9prime_tau, _im_c9prime_tau);
                u.uses(_re_c9prime_tau.id()); u.uses(_im_c9prime_tau.id());
                _c10prime_tau = std::bind(&wcimplementation::cartesian, _re_c10prime_tau, _im_c10prime_tau);
                u.uses(_re_c10prime_tau.id()); u.uses(_im_c10prime_tau.id());
                _cS_tau = std::bind(&wcimplementation::cartesian, _re_cS_tau, _im_cS_tau);
                u.uses(_re_cS_tau.id()); u.uses(_im_cS_tau.id());
                _cSprime_tau = std::bind(&wcimplementation::cartesian, _re_cSprime_tau, _im_cSprime_tau);
                u.uses(_re_cSprime_tau.id()); u.uses(_im_cSprime_tau.id());
                _cP_tau = std::bind(&wcimplementation::cartesian, _re_cP_tau, _im_cP_tau);
                u.uses(_re_cP_tau.id()); u.uses(_im_cP_tau.id());
                _cPprime_tau = std::bind(&wcimplementation::cartesian, _re_cPprime_tau, _im_cPprime_tau);
                u.uses(_re_cPprime_tau.id()); u.uses(_im_cPprime_tau.id());
                _cT_tau = std::bind(&wcimplementation::cartesian, _re_cT_tau, _im_cT_tau);
                u.uses(_re_cT_tau.id()); u.uses(_im_cT_tau.id());
                _cT5_tau = std::bind(&wcimplementation::cartesian, _re_cT5_tau, _im_cT5_tau);
                u.uses(_re_cT5_tau.id()); u.uses(_im_cT5_tau.id());
            }
        }
        else
        {
            throw InternalError("scan-mode = '" + stringify(o.get("scan-mode", "polar")) + "' is not a valid scan mode for WilsonScanModel");
        }
    }

    /* b->s Wilson coefficients */
    WilsonCoefficients<BToS>
    WilsonScanComponent<components::DeltaB1>::wilson_coefficients_b_to_s(const std::string & lepton, const bool & cp_conjugate) const
    {
        double alpha_s = 0.0;
        if (_mu__deltab1 < _mu_b__deltab1)
        {
            alpha_s = QCD::alpha_s(_mu_b__deltab1, _alpha_s_Z__deltab1, _m_Z__deltab1, QCD::beta_function_nf_5);
            alpha_s = QCD::alpha_s(_mu__deltab1, alpha_s, _mu_b__deltab1, QCD::beta_function_nf_4);
        }
        else
        {
            alpha_s = QCD::alpha_s(_mu__deltab1, _alpha_s_Z__deltab1, _m_Z__deltab1, QCD::beta_function_nf_5);
        }

        complex<double> a_s = alpha_s / 4.0 / M_PI;
        WilsonCoefficients<BToS> result;

        /* implementation of lepton dependent wilson coefficients */
        if (_lepton_dep)
        {
            if (lepton == "e")
            {
                result._sm_like_coefficients = std::array<std::complex<double>, 15>
                {
                    _c1(), _c2(), _c3(), _c4(), _c5(), _c6(),
                    0.0, 0.0, 0.0, 0.0, 0.0,
                    a_s * _c7(), a_s * _c8(), a_s * _c9_e(), a_s * _c10_e()
                };
                result._primed_coefficients = std::array<std::complex<double>, 15>
                {
                    /* we only consider c7', c8', c9' and c10' */
                    0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                    0.0, 0.0, 0.0, 0.0, 0.0,
                    a_s * _c7prime(), a_s * _c8prime(), a_s * _c9prime_e(), a_s * _c10prime_e()
                };
                result._scalar_tensor_coefficients = std::array<std::complex<double>, 6>
                {
                    _cS_e(), _cSprime_e(), _cP_e(), _cPprime_e(), _cT_e(), _cT5_e()
                };
            }
            else if (lepton == "mu")
            {
                result._sm_like_coefficients = std::array<std::complex<double>, 15>
                {
                    _c1(), _c2(), _c3(), _c4(), _c5(), _c6(),
                    0.0, 0.0, 0.0, 0.0, 0.0,
                    a_s * _c7(), a_s * _c8(), a_s * _c9_mu(), a_s * _c10_mu()
                };
                result._primed_coefficients = std::array<std::complex<double>, 15>
                {
                    /* we only consider c7', c8', c9' and c10' */
                    0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                    0.0, 0.0, 0.0, 0.0, 0.0,
                    a_s * _c7prime(), a_s * _c8prime(), a_s * _c9prime_mu(), a_s * _c10prime_mu()
                };
                result._scalar_tensor_coefficients = std::array<std::complex<double>, 6>
                {
                    _cS_mu(), _cSprime_mu(), _cP_mu(), _cPprime_mu(), _cT_mu(), _cT5_mu()
                };
            }
            else if (lepton == "tau")
            {
                result._sm_like_coefficients = std::array<std::complex<double>, 15>
                {
                    _c1(), _c2(), _c3(), _c4(), _c5(), _c6(),
                    0.0, 0.0, 0.0, 0.0, 0.0,
                    a_s * _c7(), a_s * _c8(), a_s * _c9_tau(), a_s * _c10_tau()
                };
                result._primed_coefficients = std::array<std::complex<double>, 15>
                {
                    /* we only consider c7', c8', c9' and c10' */
                    0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                    0.0, 0.0, 0.0, 0.0, 0.0,
                    a_s * _c7prime(), a_s * _c8prime(), a_s * _c9prime_tau(), a_s * _c10prime_tau()
                };
                result._scalar_tensor_coefficients = std::array<std::complex<double>, 6>
                {
                    _cS_tau(), _cSprime_tau(), _cP_tau(), _cPprime_tau(), _cT_tau(), _cT5_tau()
                };
            }
            else
                throw InternalError("lepton = '" + lepton + "' is not a valid lepton name");
        }
        else
        {
            result._sm_like_coefficients = std::array<std::complex<double>, 15>
            {
                _c1(), _c2(), _c3(), _c4(), _c5(), _c6(),
                0.0, 0.0, 0.0, 0.0, 0.0,
                a_s * _c7(), a_s * _c8(), a_s * _c9(), a_s * _c10()
            };
            result._primed_coefficients = std::array<std::complex<double>, 15>
            {
                /* we only consider c7', c8', c9' and c10' */
                0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0,
                a_s * _c7prime(), a_s * _c8prime(), a_s * _c9prime(), a_s * _c10prime()
            };
            result._scalar_tensor_coefficients = std::array<std::complex<double>, 6>
            {
                _cS(), _cSprime(), _cP(), _cPprime(), _cT(), _cT5()
            };
        }
        result._alpha_s = alpha_s;

        if (cp_conjugate)
        {
            for (auto & c : result._sm_like_coefficients)
            {
                c = conj(c);
            }

            for (auto & c : result._primed_coefficients)
            {
                c = conj(c);
            }

            for (auto & c : result._scalar_tensor_coefficients)
            {
                c = conj(c);
            }
        }

        return result;
    }

    WilsonScanComponent<components::DeltaB2>::WilsonScanComponent(const Parameters & p, const Options & o, ParameterUser & u) :
        _abs_c_deltab2(p["Abs{c_deltab2}"]),
        _arg_c_deltab2(p["Arg{c_deltab2}"]),
        _re_c_deltab2(p["Re{c_deltab2}"]),
        _im_c_deltab2(p["Im{c_deltab2}"])
    {
        if ("polar" == o.get("scan-mode", "polar"))
        {
            _c_deltab2 = std::bind(&wcimplementation::polar, _abs_c_deltab2, _arg_c_deltab2);
            u.uses(_abs_c_deltab2.id()); u.uses(_arg_c_deltab2.id());
        }
        else if ("cartesian" == o.get("scan-mode", "polar"))
        {
            _c_deltab2 = std::bind(&wcimplementation::cartesian, _re_c_deltab2, _im_c_deltab2);
            u.uses(_re_c_deltab2.id()); u.uses(_im_c_deltab2.id());
        }
        else
        {
            throw InternalError("scan-mode = '" + stringify(o.get("scan-mode", "polar")) + "' is not a valid scan mode for WilsonScanModel");
        }
    }

    WilsonCoefficients<DeltaB2>
    WilsonScanComponent<components::DeltaB2>::wilson_coefficients_deltab2(const std::string & /*quark*/, const bool & cp_conjugate) const
    {
        /*
         * No support for quark flavor dependent Wilson coefficient.
         */

        WilsonCoefficients<DeltaB2> result;
        result._c = _c_deltab2();

        if (cp_conjugate)
        {
            result._c = conj(result._c);
        }

        return result;
    }

    WilsonScanModel::WilsonScanModel(const Parameters & parameters, const Options & options) :
        SMComponent<components::CKM>(parameters, *this),
        SMComponent<components::QCD>(parameters, *this),
        WilsonScanComponent<components::DeltaB1>(parameters, options, *this),
        WilsonScanComponent<components::DeltaB2>(parameters, options, *this)
    {
    }

    WilsonScanModel::~WilsonScanModel()
    {
    }

    std::shared_ptr<Model>
    WilsonScanModel::make(const Parameters & parameters, const Options & options)
    {
        return std::shared_ptr<Model>(new WilsonScanModel(parameters, options));
    }
}
