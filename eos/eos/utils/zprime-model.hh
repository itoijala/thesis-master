/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2015 Niklas Bonacker
 *
 * This file is part of the EOS project. EOS is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * EOS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * deltails.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef EOS_GUARD_SRC_UTILS_Z_PRIME_MODEL_HH
#define EOS_GUARD_SRC_UTILS_Z_PRIME_MODEL_HH 1

#include <eos/utils/model.hh>
#include <eos/utils/standard-model.hh>

namespace eos
{
    template <typename Tag> class ZPrimeComponent;

    template <> class ZPrimeComponent<components::DeltaB1> :
        public virtual ModelComponent<components::DeltaB1>
    {
        private:
            std::shared_ptr<Model> _sm__deltab1;

            std::shared_ptr<Model> _wilson__deltab1;

            UsedParameter _G_F__deltab1;

            UsedParameter _alpha_e__deltab1;

            UsedParameter _mu__deltab1;

            UsedParameter _m_Q__deltab1;

            UsedParameter _m_D__deltab1;

            Parameter _abs_YY_Q__deltab1, _arg_YY_Q__deltab1;

            Parameter _re_YY_Q__deltab1, _im_YY_Q__deltab1;

            Parameter _abs_YY_D__deltab1, _arg_YY_D__deltab1;

            Parameter _re_YY_D__deltab1, _im_YY_D__deltab1;

            std::function<complex<double> ()> _YY_Q__deltab1;

            std::function<complex<double> ()> _YY_D__deltab1;

        public:
            ZPrimeComponent(const Parameters &, const Options &, ParameterUser &);

            virtual WilsonCoefficients<BToS> wilson_coefficients_b_to_s(const std::string & lepton, const bool & cp_conjugate) const;
    };

   template <> class ZPrimeComponent<components::DeltaB2> :
        public virtual ModelComponent<components::DeltaB2>
    {
        private:
            std::shared_ptr<Model> _sm__deltab2;

            std::shared_ptr<Model> _wilson__deltab2;

            UsedParameter _G_F__deltab2;

            UsedParameter _m_W__deltab2;

            UsedParameter _v_Phi__deltab2;

            UsedParameter _m_Q__deltab2;

            UsedParameter _m_D__deltab2;

            Parameter _abs_YY_Q__deltab2, _arg_YY_Q__deltab2;

            Parameter _re_YY_Q__deltab2, _im_YY_Q__deltab2;

            Parameter _abs_YY_D__deltab2, _arg_YY_D__deltab2;

            Parameter _re_YY_D__deltab2, _im_YY_D__deltab2;

            std::function<complex<double> ()> _YY_Q__deltab2;

            std::function<complex<double> ()> _YY_D__deltab2;

        public:
            ZPrimeComponent(const Parameters &, const Options &, ParameterUser &);

            virtual WilsonCoefficients<DeltaB2> wilson_coefficients_deltab2(const std::string & quark, const bool & cp_conjugate) const;
    };

 class ZPrimeModel :
        public Model,
        public SMComponent<components::CKM>,
        public SMComponent<components::QCD>,
        public ZPrimeComponent<components::DeltaB1>,
        public ZPrimeComponent<components::DeltaB2>
    {
        public:
            ZPrimeModel(const Parameters &, const Options &);
            virtual ~ZPrimeModel();

            static std::shared_ptr<Model> make(const Parameters &, const Options &);
    };
}

#endif
