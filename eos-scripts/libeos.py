import bisect
import itertools
import multiprocessing

import h5py
import numpy as np
import scipy.special
import scipy.stats

def get_mcmc_run(file):
    if 'main run' in file:
        return 'main run'
    elif 'prerun' in file:
        return 'prerun'
    return None

class Parameter:
    def __init__(self, name, index, min, max, nuisance, cyclic, prior):
        self.name = name
        self.index = index
        self.min = min
        self.max = max
        self.nuisance = nuisance
        self.cyclic = cyclic
        self.prior = prior

        self.id = name

    def __getitem__(self, key):
        return self.__getattribute__(key)

    def __repr__(self):
        return '''Parameter(
    name: {},
    index: {},
    min: {},
    max: {},
    nuisance: {},
    cyclic: {},
    prior: {},
   )'''.format(self.name, self.index, self.min, self.max, self.nuisance, self.cyclic, self.prior)

    def extract(self, parameters, observables):
        return parameters[self.index]

class Observable:
    def __init__(self, name, index, kinematics, options, SM):
        self.name = name
        self.index = index
        self.kinematics = kinematics
        self.options = options
        self.SM = SM

        self.observable = ','.join([name] + ['{}={}'.format(k, v) for k, v in sorted(options.items())])

        self.observable_full = '{},{}({})'.format(
            name,
            ','.join(['{}={}'.format(k, v) for k, v in sorted(options.items())]),
            ','.join(['{}={}'.format(k, v) for k, v in sorted(kinematics.items())]),
        )

        self.id = self.observable_full

    def __getitem__(self, key):
        if key == 'observable-full':
            return self.observable_full
        return self.__getattribute__(key)

    def __repr__(self):
        return '''Observable(
    name: {},
    index: {},
    kinematics: {},
    options: {},
    SM: {},
   )'''.format(self.name, self.index, self.kinematics, self.options, self.SM)

    def extract(self, parameters, observables):
        return observables[self.index]

def parse_parameters(file):
    run = get_mcmc_run(file)
    if run is not None:
        chain = list(file[run].keys())[0]
        return parse_parameters_mc(file['descriptions/{}/{}/parameters'.format(run, chain)])
    elif isinstance(file['descriptions/parameters'], h5py._hl.group.Group):
        return parse_parameters_propagate(file['descriptions/parameters'])
    else:
        return parse_parameters_mc(file['descriptions/parameters'])

def parse_parameters_mc(descriptions):
    parameters = {}

    for i, d in enumerate(descriptions[:]):
        param = Parameter(
            name=d['name'].decode('utf8'),
            index=i,
            min=d['min'],
            max=d['max'],
            nuisance=bool(d['nuisance']),
            cyclic=bool(d['cyclic']) if 'cyclic' in d else False,
            prior=d['prior'].decode('utf8'),
        )

        parameters[param.name] = param

    return parameters

def parse_parameters_propagate(descriptions):
    parameters = {}

    for p in descriptions:
        data = descriptions[p][:]

        param = Parameter(
            name=descriptions[p].attrs['name'].decode('utf8'),
            index=int(p),
            min=data[0]['min'],
            max=data[0]['max'],
            nuisance=bool(descriptions[p].attrs['nuisance']) if 'nuisance' in descriptions[p].attrs else False,
            cyclic=bool(descriptions[p].attrs['cyclic']) if 'cyclic' in descriptions[p].attrs else False,
            prior=descriptions[p].attrs['prior'].decode('utf8'),
        )

        parameters[param.name] = param

    return parameters

def parse_observables(file):
    if ('descriptions/observables' in file
            and isinstance(file['descriptions/observables'], h5py._hl.group.Group)):
        return parse_observables_propagate(file['descriptions/observables'])
    else:
        return {}

def parse_observables_propagate(descriptions):
    observables = {}

    for o in descriptions:
        kinematics = descriptions[o].attrs['kinematics'].decode('utf8')
        kinematics = {s.split('=')[0]: float(s.split('=')[1]) for s in kinematics.split(', ')} if kinematics != '' else {}

        options = descriptions[o].attrs['options'].decode('utf8')
        options = {s.split('=')[0]: s.split('=')[1] for s in options.split(',')} if options != '' else {}

        obs = Observable(
            name=descriptions[o].attrs['name'].decode('utf8'),
            index=int(o),
            kinematics=kinematics,
            options=options,
            SM=descriptions[o].attrs['SM prediction'],
        )

        observables[obs.observable_full] = obs

    return observables

def pmc_step_sort_key(step):
    try:
        f = float(step)
        return f
    except:
        if step == 'initial':
            return -1.
        elif step == 'final':
            return float('inf')
        else:
            return float('-inf')

def calc_chunks(begin, end, chunk_size):
    chunks = begin + chunk_size * np.arange((end - begin) // chunk_size + 1 + (1 if (end - begin) % chunk_size != 0 else 0))
    chunks[-1] = end

    return list(zip(chunks, chunks[1:]))

def calc_data_chunks(data, chunk_size):
    return calc_chunks(0, len(data), chunk_size)

def work(arguments):
    filename, pars, func, args, (path, (b, e)) = arguments

    f = h5py.File(filename, 'r')
    parameters = f[path[0]][b:e].T if path[0] is not None else None
    observables = f[path[1]][b:e].T if path[1] is not None else None
    return (
            '{} {} {}'.format(path, b, e),
            [func(np.vstack([v.extract(parameters, observables) for v in p]), *a) for p, a in zip(pars, args)],
            )

def map_chunked(filename, paths, pars, chunk_size, func, args):
    f = h5py.File(filename, 'r')

    all_chunks = []
    for path in paths:
        if path[0] is not None and 'parameters_begin' in f[path[0]].parent.attrs:
            begin = f[path[0]].parent.attrs['parameters_begin']
            end = f[path[0]].parent.attrs['parameters_end']
        else:
            begin = 0
            end = len(f[path[0] if path[0] is not None else path[1]])

        chunks = calc_chunks(begin, end, chunk_size)
        all_chunks.append(zip([path] * len(chunks), chunks))
    all_chunks = list(itertools.chain(*all_chunks))

    f.close() # important: close all handles to file before multiprocessing

    with multiprocessing.Pool() as p:
        l = len(all_chunks)
        print(l, 'chunks')

        results = []
        for i, (s, r) in enumerate(p.imap_unordered(work, list(zip([filename] * l, [pars] * l, [func] * l, [args] * l, all_chunks)))):
            print('  {:{}} / {}: {}'.format(i + 1, len(str(l)), l, s))
            results.append(r)
        return results

def work_extrema(d):
    return np.array([np.min(d, axis=-1), np.max(d, axis=-1)]).T

def work_histogram(d, ranges, bins=100):
    return np.histogramdd(d.T, bins=bins, range=ranges)

def ranges_chunked(filename, paths, pars, chunk_size=int(1e6)):
    print('ranges', end=' ')
    ranges = map_chunked(filename, paths, [pars], chunk_size, work_extrema, [[]] * len(pars))

    ranges = list(map(list, zip(*ranges)))
    ranges = [np.hstack(r) for r in ranges]
    ranges = [np.array([np.min(r, axis=-1), np.max(r, axis=-1)]).T for r in ranges]

    return ranges

def histogram_chunked(filename, paths, pars, chunk_size=int(1e6), ranges=None, bins=100):
    if ranges is None:
        single_pars = list(set(sum(pars, [])))

        ranges = ranges_chunked(filename, paths, single_pars, chunk_size)

        ranges = {k:v for k, v in zip(single_pars, ranges[0])}
        ranges = [[ranges[p] for p in par] for par in pars]

    print('histogram', end=' ')
    hists = map_chunked(filename, paths, pars, chunk_size, work_histogram, [[r, bins] for r in ranges] * len(pars))

    hists = list(map(list, zip(*hists)))

    bins = [h[0][1] for h in hists]
    hists = [[i[0] for i in h] for h in hists]

    hist = [np.sum(h, axis=0) for h in hists]
    hist = [h.T for h in hist]
    return list(zip(hist, bins, ranges))

def region_heights(data, levels):
    levels = scipy.special.erf(np.array(levels) / np.sqrt(2))
    sorted_data = np.sort(data, axis=None)[::-1]
    total = np.sum(sorted_data)
    return [sorted_data[bisect.bisect_left(np.cumsum(sorted_data), x * total) - 1] for x in levels]

def region_norm(data, levels):
    import matplotlib as mpl

    return mpl.colors.BoundaryNorm([0] + list(region_heights(data, levels))[::-1] + [np.inf], len(levels) + 1)

def distribution_flat_pdf(x, min_, max_):
    return np.ones(np.shape(x)) / np.abs(max_ - min_)

def distribution_gaussian_pdf(x, min_, max_, mu, sigma):
    return scipy.stats.norm.pdf(x, loc=mu, scale=sigma)

def distribution_asymmetric_gaussian_pdf(x, min_, max_, mu, sigma_low, sigma_high):
    return np.piecewise(x, [x <= mu, x > mu], [scipy.stats.norm(loc=mu, scale=sigma_low).pdf, scipy.stats.norm(loc=mu, scale=sigma_high).pdf])

def distribution_loggamma_pdf(x, min_, max_, mu, lambda_, alpha):
    return 1 / (scipy.special.gamma(alpha) * np.abs(lambda_)) * np.exp(alpha * (x - mu) / lambda_ - np.exp((x - mu) / lambda_))

def distribution_amoroso_pdf(x, min, max, mu, lambda_, alpha, beta):
    return np.abs(beta) / (scipy.special.gamma(alpha) * np.abs(lambda_)) * ((x - mu) / lambda_)**(alpha * beta - 1) * np.exp(- ((x - mu) / lambda_)**beta)
