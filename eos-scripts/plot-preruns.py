#!/usr/bin/env python3

import itertools
import sys

import h5py
import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt
import numpy as np

from libeos import parse_parameters

mpl.rcdefaults()

name = sys.argv[1].rsplit(':', 1)
filename = name[0]

f = h5py.File(filename, 'r')

parameters = parse_parameters(f)

if len(name) > 1:
    chains = name[1].split(',')
else:
    chains = [c.split('#')[1] for c in f['prerun']]

plot_params = list(parameters.keys())

if len(sys.argv) > 2:
    start = int(sys.argv[2])
else:
    start = 0

if len(sys.argv) > 3:
    step = int(sys.argv[3])
else:
    step = 200

params = list(itertools.combinations(sorted(plot_params), 2))

for i, (px, py) in enumerate(params):
    print('{:{}} / {}: {} {} {}'.format(i + 1, len(str(len(params))), len(params), filename, px, py))
    fig, ax = plt.subplots(1, 1)

    for chain in chains:
        c = 'chain #{}'.format(chain)

        data = f['prerun'][c]['samples'][start :: (len(f['prerun'][c]['samples']) - start) // step].T

        ix = parameters[px]['index']
        iy = parameters[py]['index']

        ax.plot(data[ix], data[iy], '.')

    ax.set_xlabel(px)
    ax.set_ylabel(py)

    ax.ticklabel_format(scilimits=(-3, 4))

    ax.grid()

    fig.tight_layout()
    fig.savefig('{}-preruns-{}--{}.pdf'.format(':'.join(name), px.replace('/', ''), py.replace('/', '')))
    plt.close(fig)
