#!/usr/bin/env python3

import pprint
import sys

import h5py

from libeos import (
    parse_parameters,
    parse_observables,
)

f = h5py.File(sys.argv[1], 'r')

parameters = parse_parameters(f)
observables = parse_observables(f)

pprint.pprint({k: v for k, v in parameters.items() if not v.nuisance}, indent=2)
print()
pprint.pprint({k: v for k, v in parameters.items() if v.nuisance}, indent=2)
print()
pprint.pprint(observables, indent=2)
