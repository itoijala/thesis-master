#!/usr/bin/env python3

import os.path
import sys

import h5py

from libeos import calc_chunks

chunk_size = int(1e6)

def merge_pmc_propagate(pmc_filename, pmc_step, begin, end, propagate_filename, output_filename):
    pmc_file = h5py.File(pmc_filename, 'r')
    propagate_file = h5py.File(propagate_filename, 'r')
    output_file = h5py.File(output_filename, 'w')

    descriptions = output_file.create_group('descriptions')
    pmc_file.copy('descriptions/parameters', descriptions)
    propagate_file.copy('descriptions/observables', descriptions)

    data = output_file.create_group('data')
    propagate_file.copy('data/observables', data)

    samples = pmc_file['data'][pmc_step]['samples']
    output_samples = data.create_dataset(
            'parameters',
            shape=(end - begin,),
            dtype=samples.dtype,
            )

    chunks = calc_chunks(begin, end, chunk_size)

    for b, e in chunks:
        print(b, e)
        output_samples[b:e] = samples[b:e]

    pmc_file.close()
    propagate_file.close()
    output_file.close()

def merge_pmc_propagate_sparse(pmc_filename, pmc_step, begin, end, propagate_filename, output_filename):
    output_dir = os.path.dirname(output_filename)
    pmc_path = os.path.relpath(pmc_filename, output_dir)
    propagate_path = os.path.relpath(propagate_filename, output_dir)

    output_file = h5py.File(output_filename, 'w')

    descriptions = output_file.create_group('descriptions')
    descriptions['parameters'] = h5py.ExternalLink(pmc_path, 'descriptions/parameters')
    descriptions['observables'] = h5py.ExternalLink(propagate_path, 'descriptions/observables')

    data = output_file.create_group('data')
    data['observables'] = h5py.ExternalLink(propagate_path, 'data/observables')
    data['parameters'] = h5py.ExternalLink(pmc_path, '{}/samples'.format(pmc_step))

    data.attrs['parameters_begin'] = begin
    data.attrs['parameters_end'] = end

    output_file.close()

def main():
    pmc_file, pmc_step, begin, end = sys.argv[1].rsplit(':', 3)
    begin, end = int(begin), int(end)

    propagate_file = sys.argv[2]
    output_file = sys.argv[3]

    sparse = len(sys.argv) > 4 and sys.argv[4] == '--sparse'

    if sparse:
        merge_pmc_propagate_sparse(pmc_file, pmc_step, begin, end, propagate_file, output_file)
    else:
        merge_pmc_propagate(pmc_file, pmc_step, begin, end, propagate_file, output_file)

if __name__ == '__main__':
    main()
