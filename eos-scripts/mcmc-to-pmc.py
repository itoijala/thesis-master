#!/usr/bin/env python3

import sys

import h5py
import numpy as np

from libeos import calc_data_chunks

chunk_size = int(1e6)

def convert_mcmc(output_file_name, input_file_name):
    output_file = h5py.File(output_file_name, 'w')
    input_file = h5py.File(input_file_name, 'r')
    samples = input_file['main run']['chain #0']['samples']
    input_file.copy('descriptions/main run/chain #0', output_file, name='/descriptions')
    nchains = len(input_file['main run'])

    output_samples = output_file.create_dataset(
            '/data/final/samples',
            shape=(len(samples) * nchains,),
            dtype=np.dtype((samples.dtype.base, (samples.dtype.shape[0] + 2,)))
            )

    for i, chain in enumerate(input_file['main run']):
        chain = input_file['main run'][chain]['samples']

        chunks = calc_data_chunks(samples, chunk_size)

        for b, e in chunks:
            print(i, b, e)
            d = chain[b:e]
            output_samples[b + len(samples) * i : e + len(samples) * i] = np.hstack((d[:,:-1], np.zeros((e - b, 2)), d[:,-1:]))

    input_file.close()
    output_file.close()

def main():
    input_file = sys.argv[1]
    output_file = sys.argv[2]

    convert_mcmc(output_file, input_file)

if __name__ == '__main__':
    main()
