#!/usr/bin/env python3

import itertools
import multiprocessing
import re
import sys
import textwrap

import h5py
import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt
import numpy as np
import scipy.special
import scipy.stats

import libeos

mpl.rcdefaults()

name = sys.argv[1].rsplit(':', 1)
filename = name[0]

if len(name) > 1 and name[1] =='':
    name = [name[0]]

f = h5py.File(filename, 'r')

parameters = libeos.parse_parameters(f)
observables = libeos.parse_observables(f)

variables = {}
if 'data/observables' in f:
    if 'data/parameters' in f:
        variables.update(parameters)
    variables.update(observables)
else:
    variables.update(parameters)

if len(name) > 1:
    ps = name[1].split(',')
    paths = []
    for p in ps:
        if p in f:
            paths.append(('/' + p if not p[0] == '/' else p, None))
        else:
            if 'main run' in f:
                paths.append(('/main run/chain #{}/samples'.format(p), None))
            elif 'prerun' in f:
                paths.append(('/prerun/chain #{}/samples'.format(p), None))
            else:
                paths.append(('/data/{}/samples'.format(p), None))
else:
    if 'main run' in f:
        paths = sorted([('/main run/{}/samples'.format(c), None) for c in f['main run']])
    elif 'prerun' in f:
        paths = sorted([('/prerun/{}/samples'.format(c), None) for c in f['prerun']])
    elif 'data/parameters' in f or 'data/observables' in f:
        paths = [(
            'data/parameters' if 'data/parameters' in f else None,
            'data/observables' if 'data/observables' in f else None,
        )]
    else:
        step = sorted([s for s in f['data'] if 'samples' in f['data'][s]], key=libeos.pmc_step_sort_key)[-1]
        paths = [('/data/{}/samples'.format(step), None)]

if 'main run' in f:
    a = [(f['main run'][c]['samples'].name, None) in paths for c in f['main run']]
elif 'prerun' in f:
    a = [(f['prerun'][c]['samples'].name in paths, None) for c in f['prerun']]
else:
    a = [False]

if all(a) or paths[0][1] is not None:
    name = [name[0]]
else:
    name = [name[0], ','.join([p[0].rsplit('#', 1)[1].rsplit('/', 1)[0] if len(p[0].rsplit('#', 1)) > 1 else p[0].rsplit('/', 2)[1] for p in paths])]

f.close() # important!

plot_params = list(variables.keys())

if len(sys.argv) < 3:
    plot_params = [(p,) for p in plot_params] + list(itertools.combinations(sorted(plot_params), 2))
elif len(sys.argv) == 3 and sys.argv[2].startswith('--'):
    if sys.argv[2] == '--1d':
        plot_params = [(p,) for p in plot_params]
    elif sys.argv[2] == '--2d':
        plot_params = list(itertools.combinations(sorted(plot_params), 2))
    else:
        raise Exception("Unknown argument '{}'".format(sys.argv[2]))
else:
    plot_params = [p.split('--', 1) for p in sys.argv[2:]]

params = [[variables[p] for p in params] for params in plot_params]

hist_output = libeos.histogram_chunked(filename, paths, params)

def work(arguments):
    name, script, params, (hist, bins, ranges) = arguments

    if script == 'regions' or len(params) == 1:
        levels = [1, 2, 3]
        blues = mpl.cm.get_cmap('Blues')
        colors = ['w'] + [blues(int(256 * i)) for i in [1/3, 2/3, 3/3]]

        cmap = mpl.colors.ListedColormap(colors)
        norm = libeos.region_norm(hist, levels)
    else:
        cmap = mpl.cm.get_cmap('Blues')
        cmap.set_under('w')
        norm = None

    fig, ax = plt.subplots(1, 1)

    plot_func = plot_1d if len(params) == 1 else plot_2d

    plot_func(ax, name, script, params, hist, bins, ranges, cmap, norm)

    ax.ticklabel_format(scilimits=(-3, 4))

    ax.grid()

    fig.tight_layout()
    fig.savefig('{}-{}-{}.pdf'.format(':'.join(name), script, '--'.join([p.id for p in params]).replace('/', '')))
    plt.close(fig)

    return '{} {}'.format(':'.join(name), ' '.join([p.id for p in params]))

def plot_2d(ax, name, script, params, hist, bins, ranges, cmap, norm):
    px, py = params
    bins_x, bins_y = bins
    range_x, range_y = ranges

    ax.pcolor(bins_x, bins_y, hist, cmap=cmap, norm=norm, vmin=1)

    ax.set_xlim(*range_x)
    ax.set_ylim(*range_y)

    ax.set_xlabel('\n'.join(textwrap.wrap(px.id, width=30)))
    ax.set_ylabel('\n'.join(textwrap.wrap(py.id, width=30)))

def plot_1d(ax, name, script, params, hist, bins, ranges, cmap, norm):
    px, = params
    bins_x, = bins
    ranges_x, = ranges

    ax.plot(bins_x, list(hist) + [hist[-1]], drawstyle='steps-post', color='k', solid_capstyle='butt')
    ax.bar(bins_x[:-1], hist, width=np.ediff1d(bins_x), color=mpl.cm.ScalarMappable(norm, cmap).to_rgba(hist), linewidth=0)

    plot_prior(ax, params[0], hist, bins_x)

    ax.set_xlim(*ranges_x)

    ax.set_xlabel('\n'.join(textwrap.wrap(px.id, width=30)))

    ax.set_yticks([])

def plot_prior(ax, param, hist, bins):
    if not hasattr(param, 'prior'):
        return

    r = re.compile(r'^Parameter: [^ ]+, prior type: ([^ ]+), range: \[(.*?),(.*?)\](?:, (.*))?$')
    m = r.match(param.prior)
    if not m:
        return

    prior_type = m.group(1)
    prior_min = float(m.group(2))
    prior_max = float(m.group(3))
    prior_params = m.group(4)

    x = np.linspace(prior_min, prior_max, 1000)
    prior = None

    if prior_type == 'flat':
        prior = libeos.distribution_flat_pdf(x, prior_min, prior_max)

    elif prior_type == 'Gaussian':
        r = re.compile(r'^x = ([^ ]+) \+ ([^ ]+) - ([^ ]+)$')
        m = r.match(prior_params)
        if m:
            mu = float(m.group(1))
            sigma_high = float(m.group(2))
            sigma_low = float(m.group(3))

            prior = libeos.distribution_asymmetric_gaussian_pdf(x, prior_min, prior_max, mu, sigma_low, sigma_high)

        else:
            r = re.compile(r'^x = ([^ ]+) \+- ([^ ]+)$')
            m = r.match(prior_params)
            if m:
                mu = float(m.group(1))
                sigma = float(m.group(2))

                prior = libeos.distribution_gaussian_pdf(x, prior_min, prior_max, mu, sigma)

    elif prior_type == 'LogGamma':
        r = re.compile(r'^x = ([^ ]+) \+ ([^ ]+) - ([^ ]+), nu: ([^ ]+), lambda: ([^ ]+), alpha: ([^ ]+)$')
        m = r.match(prior_params)
        if m:
            mu = float(m.group(1))
            sigma_high = float(m.group(2))
            sigma_low = float(m.group(3))
            nu = float(m.group(4))
            lambda_ = float(m.group(5))
            alpha = float(m.group(6))

            prior = libeos.distribution_loggamma_pdf(x, prior_min, prior_max, nu, lambda_, alpha)

    if prior is not None:
        ax.plot(x, np.sum(hist * np.diff(bins)) * prior, 'r--')

with multiprocessing.Pool() as p:
    l = len(params)
    print('plot {} plots'.format(l))

    for i, s in enumerate(p.imap_unordered(work, list(zip([name] * l, [sys.argv[0].rsplit('-', 1)[1].rsplit('.', 1)[0]] * l, params, hist_output)))):
        print('  {:{}} / {}: {}'.format(i + 1, len(str(l)), l, s))
