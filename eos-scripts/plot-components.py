#!/usr/bin/env python3

import itertools
import sys

import h5py
import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt
import numpy as np

from libeos import parse_parameters, pmc_step_sort_key

mpl.rcdefaults()

name = sys.argv[1].rsplit(':', 1)
filename = name[0]

f = h5py.File(filename, 'r')

parameters = parse_parameters(f)
lenp = len(parameters)

if len(name) > 1 and name[1] =='':
    name = [name[0]]

if len(name) > 1:
    step = name[1]
else:
    step = sorted(f['data'], key=pmc_step_sort_key)[-1]
    name.append(step)

if len(sys.argv) < 3:
    plot_params = list(parameters.keys())
else:
    plot_params = [sys.argv[2], sys.argv[3]]

for px, py in itertools.combinations(sorted(plot_params), 2):
    print('{} {} {}'.format(':'.join(name), px, py))

    fig, ax = plt.subplots(1, 1)

    data = f['data'][step]['components']

    weight = data['weight']
    max_weight = np.max(weight)

    ix = parameters[px]['index']
    iy = parameters[py]['index']

    mean = data['mean'][weight > 0].T
    mean_x = mean[ix]
    mean_y = mean[iy]

    covariance = data['covariance'][weight > 0]
    std_x = np.sqrt(covariance[:,ix * (lenp + 1)])
    std_y = np.sqrt(covariance[:,iy * (lenp + 1)])
    angle = covariance[:,ix + iy * lenp] / std_x / std_y

    weight = weight[weight > 0]

    t_x = np.arctan(-std_y / std_x * np.tan(angle)) + np.array([[0, 1]]).T * np.pi
    t_y = np.arctan( std_y / std_x / np.tan(angle)) + np.array([[0, 1]]).T * np.pi

    xlim = mean_x + std_x * np.cos(t_x) * np.cos(angle) - std_y * np.sin(t_x) * np.sin(angle)
    ylim = mean_y + std_y * np.sin(t_y) * np.cos(angle) + std_x * np.cos(t_y) * np.sin(angle)

    for i in range(len(mean_x)):
        ax.add_patch(mpl.patches.Ellipse(xy=(mean_x[i], mean_y[i]), width=2 * std_x[i], height=2 * std_y[i],
                angle=angle[i] * 45, # degrees
                fc='none',
                alpha=weight[i] / max_weight,
                ))

    ax.set_xlim(np.min(xlim), np.max(xlim))
    ax.set_ylim(np.min(ylim), np.max(ylim))

    ax.set_xlabel(px)
    ax.set_ylabel(py)

    ax.ticklabel_format(scilimits=(-3, 4))

    ax.grid()

    fig.tight_layout()
    fig.savefig('{}-components-{}--{}.pdf'.format(':'.join(name), px.replace('/', ''), py.replace('/', '')))
    plt.close(fig)
