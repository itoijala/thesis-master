#!/usr/bin/env python3

import sys

import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize
import scipy.special

import libeos

def amoroso_pdf(x, a, λ, α, β):
    return libeos.distribution_amoroso_pdf(x, None, None, a, λ, α, β)

def amoroso_cdf(x, a, λ, α, β):
    if β / λ < 0:
        return 1 - scipy.special.gammainc(α, ((x - a) / λ)**β)
    else:
        return scipy.special.gammainc(α, ((x - a) / λ)**β)

def amoroso_mode(a, λ, α, β):
    if α * β > 1:
        return a + λ * (α - 1/β)**(1/β)
    else:
        return a

def constraints(x):
    λ, α, β = x
    return np.array([
        amoroso_mode(a, λ, α, β) / μ - 1,
        amoroso_cdf(μ + σ_high, a, λ, α, β) - amoroso_cdf(μ - σ_low, a, λ, α, β) - scipy.special.erf(1 / np.sqrt(2)),
        amoroso_pdf(μ + σ_high, a, λ, α, β) - amoroso_pdf(μ - σ_low, a, λ, α, β),
    ])

def constraints2(x):
    return np.sum(np.abs(np.array([1, 1, 1]) * constraints(x)))

def plot(a, λ, α, β, color):
    plt.plot(x, amoroso_pdf(x, a, λ, α, β), color=color)

    plt.axvline(x=(μ - σ_low), color=color)
    plt.axvline(x=(μ + σ_high), color=color)

    plt.axhline(y=amoroso_pdf(μ - σ_low, a, λ, α, β), color=color)
    plt.axhline(y=amoroso_pdf(μ + σ_high, a, λ, α, β), color=color)

    plt.axvline(x=amoroso_mode(a, λ, α, β), color=color)

μ = float(sys.argv[1])

σ_low_stat = np.abs(float(sys.argv[2]))
σ_low_syst = np.abs(float(sys.argv[3]))
σ_low = np.sqrt(σ_low_stat**2 + σ_low_syst**2)

σ_high_stat = np.abs(float(sys.argv[4]))
σ_high_syst = np.abs(float(sys.argv[5]))
σ_high = np.sqrt(σ_high_stat**2 + σ_high_syst**2)

print('x = {} -{} -{} +{} +{}\n  = {} -{} +{}'.format(μ, σ_low_stat, σ_low_syst, σ_high_stat, σ_high_syst, μ, σ_low, σ_high))
print('1σ: x in [{}, {}]'.format(μ - σ_low, μ + σ_high))
print()

x = np.linspace(μ - 4 * σ_low, μ + 4 * σ_high, 1000)

plt.plot(x, libeos.distribution_asymmetric_gaussian_pdf(x, None, None, μ, σ_low, σ_high), color='b')
plt.axvline(x=μ, color='b')
plt.axvline(x=μ - σ_low, color='b')
plt.axvline(x=μ + σ_high, color='b')

a = float(sys.argv[6])
λ = float(sys.argv[7])
α = float(sys.argv[8])
β = float(sys.argv[9])

print('a = {}, λ = {}, α = {}, β = {}'.format(a, λ, α, β))
print(constraints((λ, α, β)), constraints2((λ, α, β)))
print()
plot(a, λ, α, β, 'g')

r = scipy.optimize.minimize(constraints2, (λ, α, β), bounds=[(None, None), (0, None), (None, None)])
print(r)
print()

λ, α, β = r.x

print('a = {}, λ = {}, α = {}, β = {}'.format(a, λ, α, β))
print(constraints((λ, α, β)), constraints2((λ, α, β)))
print()
plot(a, λ, α, β, 'r')

plt.savefig('amoroso-fit.pdf')

def sigma_constraints2(n):
    def f(x):
        x1, x2 = x
        return np.sum(np.abs(np.array([
            amoroso_cdf(x2, a, λ, α, β) - amoroso_cdf(x1, a, λ, α, β) - scipy.special.erf(n / np.sqrt(2)),
            amoroso_pdf(x2, a, λ, α, β) - amoroso_pdf(x1, a, λ, α, β),
        ])))
    return f

for n in range(2, 4):
    r = scipy.optimize.minimize(sigma_constraints2(n), (μ - n * σ_low, μ + n * σ_high), bounds=[(0, None), (0, None)])
    print(r)
    x1, x2 = r.x
    print('{}σ in [{}, {}]'.format(n, x1, x2))
    print()
