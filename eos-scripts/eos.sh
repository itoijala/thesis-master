#!/bin/bash

EOS="eos"
EOS_SCRIPTS="eos-scripts"
OUTPUT="build/run"

NAME="$(basename "$0")"
NAME="${NAME%.*}"

# $1 = type (mcmc|mcmc.pmc|prerun|prerun.merge|pmc|propagate)
# $2 = is log (true|false) (null = false)
# $3 = prerun id (prerun only)
function output-path-default() {
	local path="${OUTPUT}/${NAME}${3:+/prerun}"
	mkdir -p "${path}"
	echo "${path}/${3:-$1}.hdf5$("${2:-false}" && echo ".log")"
}

function output-path() {
	output-path-default "$@"
}

MCMC_PRERUN_UPDATE="400"
MCMC_PRERUN_MIN="400"
MCMC_PRERUN_MAX="100000"

MCMC_CHAINS="4"
MCMC_CHUNK_SIZE="1000"
MCMC_CHUNKS="100"

function mcmc() {
	local args=(
		--output "$(output-path mcmc)"
		--store-prerun
		--debug

		--prerun-update "${MCMC_PRERUN_UPDATE}"
		--prerun-min "${MCMC_PRERUN_MIN}"
		--prerun-max "${MCMC_PRERUN_MAX}"

		--chains "${MCMC_CHAINS}"
		--chunk-size "${MCMC_CHUNK_SIZE}"
		--chunks "${MCMC_CHUNKS}"

		${MCMC_OPTIONS[@]}

		${GLOBAL_OPTIONS[@]/#/--global-option }

		${CONSTRAINTS[@]/#/--constraint }

		${PARAMETERS_FIX[@]/#/--fix }
		${PARAMETERS_SCAN[@]/#/--scan }
		${PARAMETERS_NUISANCE[@]/#/--nuisance }
	)

	( set -x
		"${EOS}/src/clients/eos-scan-mc" "${args[@]}"
	) 2>&1 | tee "$(output-path mcmc true)"
}

function mcmc-to-pmc() {
	local input="$(output-path mcmc)"
	local output="$(output-path mcmc.pmc)"
	( set -x
		"${EOS_SCRIPTS}/mcmc-to-pmc.py" "${input}" "${output}"
	) 2>&1 | tee "$(output-path mcmc.pmc true)"
}

PRERUN_UPDATE="400"
PRERUN_MIN="400"
PRERUN_MAX="100000"

function prerun-single() {
	local args=(
		--output "$(output-path prerun false "$1")"
		--debug

		--seed "$1"

		--prerun-only
		--prerun-chains-per-partition 1

		--prerun-update "${PRERUN_UPDATE}"
		--prerun-min "${PRERUN_MIN}"
		--prerun-max "${PRERUN_MAX}"

		${PRERUN_OPTIONS[@]}

		${GLOBAL_OPTIONS[@]/#/--global-option }

		${CONSTRAINTS[@]/#/--constraint }

		${PARAMETERS_FIX[@]/#/--fix }
		${PARAMETERS_SCAN[@]/#/--scan }
		${PARAMETERS_NUISANCE[@]/#/--nuisance }
	)

	( set -x
		"${EOS}/src/clients/eos-scan-mc" "${args[@]}"
	) 2>&1 | tee "$(output-path prerun true "$1")"
}

PRERUN_NUMBER="1"

function prerun-multi() {
	( set -x
		seq 0 $(( ${PRERUN_NUMBER} - 1 )) | parallel "$0" prerun-single '{}'
	) 2>&1
}

function prerun-merge() {
	local p
	declare -a preruns
	for p in $(seq 0 $(( ${PRERUN_NUMBER} - 1 )) ) ; do
		preruns[$p]="$(output-path prerun false $p)"
	done
	local output="$(output-path prerun.merge)"
	( set -x
	        "${EOS_SCRIPTS}/merge-preruns.py" "${output}" "${preruns[@]}"
	) 2>&1 | tee "$(output-path prerun.merge true)"
}

function prerun() {
	"$0" prerun-multi && "$0" prerun-merge
}

PMC_HC_PATCH_LENGTH="1000"
PMC_HC_SKIP_INITIAL="0.2"

PMC_MAX_UPDATES="10"
PMC_MIN_UPDATES="2"
PMC_RELATIVE_STD="0.1"
PMC_CONVERGENCE_PERPLEXITY="0.9"
PMC_SAMPLES_PER_COMPONENT="10000"
PMC_FINAL_SAMPLES="20000"

function pmc() {
	PMC_PRERUN_INPUT="${PMC_PRERUN_INPUT:-$(output-path prerun.merge)}"

	local args=(
		--output "$(output-path pmc)"
		--debug

		--use-pmc
		--pmc-initialize-from-file "${PMC_PRERUN_INPUT}"

		--hc-patch-length "${PMC_HC_PATCH_LENGTH}"
		--hc-skip-initial "${PMC_HC_SKIP_INITIAL}"
		--hc-target-ncomponents "${PMC_HC_TARGET_NCOMPONENTS}"

		--pmc-max-updates "${PMC_MAX_UPDATES}"
		--pmc-relative-std-deviation-over-last-steps "${PMC_RELATIVE_STD}" "${PMC_MIN_UPDATES}"
		--pmc-convergence-perplexity "${PMC_CONVERGENCE_PERPLEXITY}"
		--pmc-samples-per-component "${PMC_SAMPLES_PER_COMPONENT}"
		--pmc-final-samples "${PMC_FINAL_SAMPLES}"

		${PMC_OPTIONS[@]}

		${GLOBAL_OPTIONS[@]/#/--global-option }

		${OBSERVABLES[@]}
		${CONSTRAINTS[@]/#/--constraint }

		${PARAMETERS_FIX[@]/#/--fix }
		${PARAMETERS_SCAN[@]/#/--scan }
		${PARAMETERS_NUISANCE[@]/#/--nuisance }
	)

	( set -x
		"${EOS}/src/clients/eos-scan-mc" "${args[@]}"
	) 2>&1 | tee "$(output-path pmc true)"
}

PROPAGATE_USE_MCMC="false"
PROPAGATE_PMC_SAMPLE_DIRECTORY="/data/final"
PROPAGATE_WORKERS="4"

function propagate() {
	if ! ${PROPAGATE_USE_MCMC} ; then
		PROPAGATE_PMC_INPUT="${PROPAGATE_PMC_INPUT:-$(output-path pmc)}"
		PROPAGATE_PMC_INPUT_SAMPLES="${PROPAGATE_PMC_INPUT_SAMPLES:-0 ${PMC_FINAL_SAMPLES}}"
	else
		PROPAGATE_PMC_INPUT="${PROPAGATE_PMC_INPUT:-$(output-path mcmc.pmc)}"
		PROPAGATE_PMC_INPUT_SAMPLES="${PROPAGATE_PMC_INPUT_SAMPLES:-0 $(( $MCMC_CHAINS * $MCMC_CHUNKS * $MCMC_CHUNK_SIZE ))}"
	fi

	local args=(
		--output "$(output-path propagate)"
		--debug

		--pmc-input "${PROPAGATE_PMC_INPUT}" ${PROPAGATE_PMC_INPUT_SAMPLES}
		--pmc-sample-directory "${PROPAGATE_PMC_SAMPLE_DIRECTORY}"

		--workers "${PROPAGATE_WORKERS}"

		${PROPAGATE_OPTIONS[@]}

		${GLOBAL_OPTIONS[@]/#/--global-option }

		${PREDICTIONS[@]}

		${PARAMETERS_FIX[@]/#/--fix }
	)

	( set -x
		"${EOS}/src/clients/eos-propagate-uncertainty" "${args[@]}"
	) 2>&1 | tee "$(output-path propagate true)"
}

function merge-pmc-propagate() {
	if ! ${PROPAGATE_USE_MCMC} ; then
		PROPAGATE_PMC_INPUT="${PROPAGATE_PMC_INPUT:-$(output-path pmc)}"
		PROPAGATE_PMC_INPUT_SAMPLES="${PROPAGATE_PMC_INPUT_SAMPLES:-0 ${PMC_FINAL_SAMPLES}}"
		local pmc="$(output-path pmc)"
	else
		PROPAGATE_PMC_INPUT="${PROPAGATE_PMC_INPUT:-$(output-path mcmc.pmc)}"
		PROPAGATE_PMC_INPUT_SAMPLES="${PROPAGATE_PMC_INPUT_SAMPLES:-0 $(( $MCMC_CHAINS * $MCMC_CHUNKS * $MCMC_CHUNK_SIZE ))}"
		local pmc="$(output-path mcmc.pmc)"
	fi

	local samples="$(echo "${PROPAGATE_PMC_INPUT_SAMPLES}" | sed 's/ /:/')"
	local propagate="$(output-path propagate)"
	local output="$(output-path propagate.pmc)"

	( set -x
		"${EOS_SCRIPTS}/merge-pmc-propagate.py" "${pmc}:${PROPAGATE_PMC_SAMPLE_DIRECTORY}:${samples}" "${propagate}" "${output}" "${MERGE_PROPAGATE_PMC_SPARSE}"
	) 2>&1 | tee "$(output-path propagate.pmc true)"
}

function main() {
	local run="$1"
	shift
	"${run}" "$@"
}
