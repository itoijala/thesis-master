#!/usr/bin/env python3

import os
import sys

import h5py

def invalid_chain(file, chain, cut_off=None):
    """Check if mode larger than cut off"""

    # last value of the last mode is the best found in all updates sequences
    mode = file['/prerun/chain #%d/stats/mode' % chain][-1][-1]

    if cut_off is None:
        print('chain %d has mode %g' % (chain, mode))
        return False

    if mode > cut_off:
        return False
    else:
        print('skipping chain %d with mode %g < %g' % (chain, mode, cut_off))
        return True

def merge_preruns(output_file_name, input_files, cut_off=None):
    """
    Merge a prerun, stored in different files into one common file.
    Before:
        file0:/prerun/chain #0/ file1:/prerun/chain #0 ...
    After:
        file0:/prerun/chain #0/ file0:/prerun/chain #1 ...
    """

    # hdf5 groups
    groups = ['/prerun', '/descriptions/prerun']

    # count chains copied
    nchains_copied = 0

    output_file = h5py.File(output_file_name, "w")
    for g in groups:
        output_file.create_group(g)

    for f_i, file_name in enumerate(input_files):
        print("merging %s" % file_name)
        input_file = h5py.File(file_name, 'r')
        nchains_in_file = len(input_file[groups[0]].keys())

        # copy data
        for i in range(nchains_in_file):
            if invalid_chain(input_file, i, cut_off):
                continue
            for g in groups:
                input_file.copy(g + "/chain #%d" % i, output_file, name=g + "/chain #%d" % nchains_copied)
            nchains_copied += 1
        input_file.close()

    print("Merged %d chains of %d files" % (nchains_copied, len(input_files)))

    output_file.close()

def main():
    print("Merging into output file %s" % sys.argv[1])

    merge_preruns(sys.argv[1], sys.argv[2:])

if __name__ == '__main__':
    main()
