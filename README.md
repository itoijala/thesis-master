# Master Thesis "Explaining R_K with leptoquarks" by Ismo Toijala

Final PDFs:

- [Screen](https://bitbucket.org/itoijala/thesis-master/raw/master/final/thesis.pdf)
- [Print](https://bitbucket.org/itoijala/thesis-master/raw/master/final/thesis-print.pdf)

Dependencies (tested versions):

- Bash (4.3.042)
- GNU make (4.1)
- for EOS:
    - libtool (2.4.6)
    - automake (1.15)
    - autoconf (2.69)
    - GCC (5.2.0)
    - GSL (1.16)
    - HDF5 (1.8.14)
    - minuit2 (5.34.14) ([Arch pkgbuild](https://bitbucket.org/itoijala/thesis-master/src/master/pkgbuilds/minuit2))
    - pmclib (1.2) ([Arch pkgbuild](https://bitbucket.org/itoijala/thesis-master/src/master/pkgbuilds/pmclib))
- Python (3.4.3)
    - numpy (1.9.2)
    - scipy (0.16.0)
    - h5py (2.5.0)
    - matplotlib (1.4.3)
- TeXLive (2015)

Building:

- EOS:
```
cd eos
./autogen.bash
./configure --with-minuit2=… --with-pmc=…
make -j…
cd ..
```
- thesis:
```
make -j… thesis thesis-print
```
