\chapter{Theory}
\label{sec:theory}

Relevant parts of the SM are outlined in section~\ref{sec:theory-SM}.
The theoretical description of~\PB decays takes place within the framework of an effective field theory, as outlined in section~\ref{sec:theory-OPE}.
The central observables $\RK$, $\RKs$ and $\smash{\BRa(\PBs → ℓℓ)}$ are defined in sections~\ref{sec:theory-RK}, \ref{sec:theory-RKs} and~\ref{sec:theory-Bs→ℓℓ}, respectively.
Mixing observables in the $\PBq$ system which provide complementary constraints are discussed in section~\ref{sec:theory-B-mixing}.

\section{Flavour in the standard model}
\label{sec:theory-SM}

The SM is built upon its gauge group
\begin{eqn}
  \SU(3)↓c × \SU(2)↓L × \U(1)_Y \.,
\end{eqn}
where $\t{c}$ stands for colour, $\t{L}$ for left and $Y$ for hypercharge, and its matter content, the fermions.
Flavour as a concept exists because the SM includes three instead of only one generation of fermions, each of which contains two flavours.
Fermions are sorted into four groups: up- and down-type quarks, charged leptons and neutral leptons called neutrinos; they are defined as
\begin{eqn}
  U = \g(u, c, t) \.,
  \qquad
  D = \g(d, s, b) \.,
  \qquad
  l = \g(e, μ, τ) \.,
  \qquad
  ν = \g(ν_{\Pe}, ν_{\Pmu}, ν_{\Ptau}) \.,
\end{eqn}
respectively, for both left- and right-handed particles.
While right-handed fermions are singlets under the $\SU(2)↓L$ symmetry of the SM, the left-handed $\SU(2)$ doublets of quarks and leptons are defined as
\begin{eqn}
  Q_q = \begin{pmatrix*}[r] U'_q \\[3pt] D'_q \end{pmatrix*} \! \.,
  \qquad
  L_ℓ = \begin{pmatrix*}[r] ν_ℓ \\[3pt] l_ℓ \end{pmatrix*} \! \.,
\end{eqn}
respectively, where $q = \Pd, \Ps, \Pb$ and $ℓ = \Pe, \Pmu, \Ptau$ are generation indices and the primes denote flavour eigenstates.
The representations of the fermions under the SM gauge group are shown in table~\ref{tab:theory-SM-repr} using the definition $Q = T_3 + Y$ for the electric charge, where $T_3$ is the third component of the weak isospin.

\begin{table}
  \caption{
    SM gauge group representations of the fermions.
    The hypothetical right-handed neutrino $ν↓R$ is a singlet under the SM gauge group and is included for completeness.
  }
  \label{tab:theory-SM-repr}
  $
  \begin{array}{r c c c @{\qquad\qquad} r c c c}
      \toprule
          & \SU(3)↓c    & \SU(2)↓L    & \U(1)_Y       &     & \SU(3)↓c    & \SU(2)↓L    & \U(1)_Y                                         \\
      \midrule
      Q↓L & \symbfup{3} & \symbfup{2} & +\sfrac{1}{6} & L↓L & \symbfup{1} & \symbfup{2} & -\sfrac{1}{2}                                   \\
      U↓R & \symbfup{3} & \symbfup{1} & +\sfrac{2}{3} & ν↓R & \symbfup{1} & \symbfup{1} & \hphantom{-}\mathrlap{0}\hphantom{\sfrac{1}{2}} \\
      D↓R & \symbfup{3} & \symbfup{1} & -\sfrac{1}{3} & l↓R & \symbfup{1} & \symbfup{1} & -\mathrlap{1}\hphantom{\sfrac{1}{2}}            \\
      \bottomrule
    \end{array}
  $
\end{table}

Transitions between generations in the SM are caused solely by the mismatch
\begin{eqn}
  D' = \m{V} D
\end{eqn}
between the mass (unprimed) and flavour (primed) eigenstates of the quarks, where the CKM matrix is given by
\begin{eqn}
  \m{V} =
    \begin{pmatrix}
      1 - \sfrac{1}{2} λ²               & λ                   & A λ³ \g(\bar{ρ} - \I \bar{η}) \\
      -λ                                & 1 - \sfrac{1}{2} λ² & A λ²                          \\
      A λ³ \g(1 - \bar{ρ} - \I \bar{η}) & -A λ²               & 1
    \end{pmatrix}
    + \LandauO(λ⁴)
\end{eqn}
in the Wolfenstein parameterisation, an expansion in the small parameter $λ = \sin.θ↓C. ≈ \num{0.2}$, where $θ↓C$ is the Cabibbo angle.
This reflects the hierarchical structure of the CKM matrix, the origin of which is unknown.
A different parameterisation using three rotation angles and one complex phase is also used in the literature~\cite{pdg2014}.
The CKM matrix is the only source of $\CP$~violation in the SM, encoded as the complex phase.

Charged currents involving \PW~bosons are the only elementary flavour changing interactions in the SM.
This is due to the fact that only the weak interaction couples to flavour instead of mass eigenstates, and therefore experiences the mixing induced by the CKM matrix.
All other interactions are flavour diagonal.

Flavour changing neutral currents (FCNCs), as required for the decay $\Pb → \Ps ℓℓ$ studied here, are induced at loop level by the Feynman diagrams in figure~\ref{fig:theory-SM-feynman}.
These diagrams are suppressed not only through loop suppression and the CKM elements, but also by the Glashow–Iliopoulos–Maiani (GIM) mechanism.
Due to the unitarity of the CKM~matrix, the amplitudes of the diagrams with top, charm and up quarks in the loop would cancel for degenerate quark masses.
Only the differences of the quark masses, relative to the \PW~mass, enter the amplitude.
For this reason, the difference of the charm and up quark masses can be neglected, leaving only the difference of the top and charm quark masses to contribute to the amplitude.

\begin{figure}
  \begin{tikzpicture}[
    scale=2,
    style={font=\vphantom{\strut}},
  ]
    \coordinate (c) at (0, 0);
    \coordinate (v1) at ($(c) + (-0.5, 0)$);
    \coordinate (v2) at ($(c) + (+0.5, 0)$);
    \coordinate (v3) at ($(c) + (0, -0.5)$);
    \coordinate (v4) at ($(v3) + (0.5, -0.5)$);

    \draw [fermion] (v1) +(-0.5, 0.3) -- node [left,  pos=0] {\Pb} (v1);
    \draw [photon, decoration={end down}] (v1) -- node [above, pos=0.5] {\PW} (v2);
    \draw [fermion] (v2) -- node [right, pos=1] {\Ps} +(+0.5, 0.3);

    \draw [fermion=0.25, fermion=0.75] (v1) .. controls ($4/3*(v3) - 1/3*(c) + (-1, 0)$) and ($4/3*(v3) - 1/3*(c) + (+1, 0)$) .. node [above, pos=0.5] {\Pt, \Pc, \Pu} (v2);

    \draw [photon, decoration={start down}] (v3) -- node [left=0.1, pos=0.72] {\Pphoton, \PZ, \PHiggs} (v4);
    \draw [fermion] (v4) -- node [right, pos=1] {$ℓ$} +(0.5, +0.3);
    \draw [antifermion] (v4) -- node [right, pos=1] {$ℓ$} +(0.5, -0.3);


    \coordinate (c) at (3, 0);
    \coordinate (v1) at ($(c) + (-0.5, 0)$);
    \coordinate (v2) at ($(c) + (+0.5, 0)$);
    \coordinate (v3) at ($(c) + (+0.5, -1 -0.3)$);
    \coordinate (v4) at ($(c) + (+0.5, -1 +0.3)$);

    \draw [fermion] (v1) +(-0.5, +0.3) -- node [left, pos=0] {\Pb} (v1);
    \draw [fermion] (v1) -- node [above, pos=0.5] {\Pt, \Pc, \Pu} (v2);
    \draw [fermion] (v2) -- node [right, pos=1] {\Ps} +(+0.5, +0.3);

    \draw [photon, decoration={start down}] (v1) -- node [left=0.1, pos=0.53] {\PW} (v3);
    \draw [photon] (v2) -- node [right, pos=0.5] {\PW} (v4);

    \draw [fermion] (v3) -- node [right, pos=0.5] {$ν$} (v4);
    \draw [fermion] (v4) -- node [right, pos=1] {$ℓ$} ($(c) + (+1, -1 +0.3)$);
    \draw [antifermion] (v3) -- node [right, pos=1] {$ℓ$} ($(c) + (+1, -1 -0.3)$);
  \end{tikzpicture}
  \caption{
    Feynman diagrams for $\Pb → \Ps ℓℓ$ in the SM.
    In the left diagram, the photon~\Pphoton, the \PZ~boson and the Higgs boson~\PHiggs can also be coupled to the down-type quarks or the \PW~boson.
  }
  \label{fig:theory-SM-feynman}
  \vspace*{2\baselineskip}
\end{figure}

\section{Effective theory for \texorpdfstring{$\Pb → \Ps$}{b → s} transitions}
\label{sec:theory-OPE}

Transitions of bottom to strange quarks are mediated by the effective Hamiltonian~\cite{9910220v1}
\begin{eqn}
  \H↓{eff}^{\Pb → \Ps} = - \frac{4 G↓F}{\sqrt{2}} \CKM{\Pt \Pb}{\Pt \Ps} \sum_i \f{C_i}(μ) \f{O_i}(μ)
\end{eqn}
of dimension six operators $O_i$.
Here, $G↓F$ is the Fermi constant and $V_{q \smash{q'}}$ are CKM~matrix elements.
The operators encode the physics at low scales while the Wilson coefficients $C_i$ encode the physics at high scales.
The scale $μ$ separates the high and low scales and is usually around the bottom quark mass $m_{\Pb}$.
Here, the only relevant operators are
\begin{eqn}
  \begin{IEEEeqnarraybox}[][c]{rCll " rCll}
    O_9^ℓ  &=& \frac{α↓e}{4 \PI} \g[\bar{s} γ_μ P↓L b] & \g[\bar{l}_ℓ γ^μ l_ℓ]
      & O_{10}^ℓ  &=& \frac{α↓e}{4 \PI} \g[\bar{s} γ_μ P↓L b] & \g[\bar{l}_ℓ γ^μ γ_5 l_ℓ] \\
    O_9'^ℓ &=& \frac{α↓e}{4 \PI} \g[\bar{s} γ_μ P↓R b] & \g[\bar{l}_ℓ γ^μ l_ℓ]
      & O_{10}'^ℓ &=& \frac{α↓e}{4 \PI} \g[\bar{s} γ_μ P↓R b] & \g[\bar{l}_ℓ γ^μ γ_5 l_ℓ] \.,
  \end{IEEEeqnarraybox}
\end{eqn}
where $α↓e$ is the fine-structure constant and $P_{\t{L}, \t{R}}$ are the chirality projectors.
In the SM the primed coefficients $C_{9, 10}'$ vanish and the unprimed coefficients $C_{10} ≈ - C_9$ are related by the $\SU(2)$ symmetry of the SM.
The SM is lepton universal, up to negligible effects from Higgs couplings, and so the coefficients $C_i^ℓ = C_i^{}$ do not depend on the lepton flavour~$ℓ$.

\section{\texorpdfstring{$\RK$}{R\_K}}
\label{sec:theory-RK}

A simple and effective test of lepton universality is the measurement of the $\CP$\-/averaged observable
\vspace{-\jot}
\begin{eqn}
  R_{\PK, \intcc{q²↓{min}}{q²↓{\vphantom{i}max}}}
    = \frac{
        \displaystyle \sum_{\CP} \int_{q²↓{min}}^{q²↓{max}} \dif{q²} \d{q²}{ƒΓ(\PBplus → \PKplus \APmuon \Pmuon)};
      }{
        \displaystyle \sum_{\CP} \int_{q²↓{min}}^{q²↓{max}} \dif{q²} \d{q²}{ƒΓ(\PBplus → \PKplus \APelectron \Pelectron)};
      } \.,
\end{eqn}
which in the SM is $1 + \smash{\LandauO(\num{e-4})}$ \cite[tab.\@ 2]{0709.4174v2}.
The kinematic variable $q²$ is the invariant mass squared of the dilepton system.
All the hadronic uncertainties that make a precise prediction of the branching ratio $\BR(\PB → \PK ℓℓ)$ difficult \cite{0709.4174v2} cancel in the ratio.
This makes it simple to find the effect of NP on the observable \cite{1411.4773v2}.
Neglecting everything but the leading Wilson coefficients, the branching ratio is
\begin{eqn}
  \BR(\PB → \PK ℓℓ)
    ∼ \abs{C_9^ℓ + C_9'^ℓ}² + \abs{C_{10}^ℓ + C_{10}'^ℓ}² \.,
  \itext{which reduces to}[\vspace{-\jot}][\vspace{-\jot}]
  \BR(\PB → \PK ℓℓ)
    ∝ \abs{C↑{SM} + Δ^{ℓ+}}²
\end{eqn}
using the SM relation $C_9 = - C_{10} ≕ C$ and the NP contribution  $Δ^{ℓ±} ≔ C^{ℓ, \t{NP}} ± C'^{ℓ, \t{NP}}$.
For the purposes of this approximation, the $\SU(2)$ relation is taken to be exact.
Furthermore, it is assumed that the $\SU(2)$ relation also holds for any NP contributions.
This leads to
\begin{eqn}
  \RK ∼ \frac{\abs{C↑{SM} + Δ^{\Pmu +}}²}{\abs{C↑{SM} + Δ^{\Pe +}}²} \., \label{eqn:theory-R_K-approx}
  \itext{which reduces to}[\vspace{-\jot}][\vspace{-\jot}]
  \abs{C↑{SM} + Δ^{ℓ +}}²
    = \begin{cases}
      \g(C↑{SM} \sqrt{\RK})^{\! 2}           \., & ℓ = \Pmu \\
      \g(\frac{C↑{SM}}{\sqrt{\RK}})^{\!\! 2} \., & ℓ = \Pe \..
    \end{cases}
\end{eqn}
These are rings in the complex plane of the NP contribution $Δ^{ℓ +}$ centred around the SM coefficient $C↑{SM}$.
The radius is given by the measured value of $\RK$.

\section{\texorpdfstring{$\RKs$}{R\_K*}}
\label{sec:theory-RKs}

The observable
\vspace{-\jot}
\begin{eqn}
  R_{\PKstar, \intcc{q²↓{min}}{q²↓{\vphantom{i}max}}}
    = \frac{
        \displaystyle \sum_{\CP} \int_{q²↓{min}}^{q²↓{max}} \dif{q²} \d{q²}{ƒΓ(\PBzero → \PKstarzero \APmuon \Pmuon)};
      }{
        \displaystyle \sum_{\CP} \int_{q²↓{min}}^{q²↓{max}} \dif{q²} \d{q²}{ƒΓ(\PBzero → \PKstarzero \APelectron \Pelectron)};
      }
\end{eqn}
is defined analogously to $\RK$.
The branching ratio \cite{0805.2525v2} is approximately given by \cite{1411.4773v2}
\begin{eqn}
  \BR(\PB → \PKstar ℓℓ)
    ∼ p \abs{C_9^ℓ - C_9'^ℓ}² + \g(1 - p) \abs{C_9^ℓ + C_9'^ℓ}² + \g(9 → 10) \.,
  \vspace{-0.5\jot}
\end{eqn}
where
\vspace{-0.5\jot}
\begin{eqn}
  p = \frac{g_0 + g_\parallel}{g_0 + g_\parallel + g_\perp}
\end{eqn}
is the polarisation fraction.
The $g_i$ with $g_{\parallel, \perp} ∝ g_{+1} ± g_{-1}$ represent the spin states of the spin-$1$ \PKstar meson.
For large hadronic recoil, meaning small $q²$, $p$ is approximately one due to the enhancement of the longitudinal amplitude by \cite[appendix D]{1312.1923v2}
\begin{eqn}
  \frac{m_{\PB}²}{2 m_{\PKstar} \sqrt{q²}} \in \intcc{6}{15} \quad \text{for} \quad q² \in \intcc{\SI{1}{\GeVs}}{\SI{6}{\GeVs}} \.,
  \vspace{-0.5\jot}
\end{eqn}
giving
\vspace{-0.5\jot}
\begin{eqn}
  \BR(\PB → \PKstar ℓℓ) ∝ \abs{C↑{SM} + Δ^{ℓ-}}² \..
  \vspace{-0.5\jot}
\end{eqn}
This leads to
\vspace{-0.5\jot}
\begin{eqn}
  \RKs ∼ \frac{\abs{C↑{SM} + Δ^{\Pmu -}}²}{\abs{C↑{SM} + Δ^{\Pe -}}²} \.,
\end{eqn}
the analogous expression to \eqref{eqn:theory-R_K-approx}.

\section{\texorpdfstring{$\APBs → ℓ ℓ$}{B̅ₛ → ℓℓ}}
\label{sec:theory-Bs→ℓℓ}

For vanishing $C_{\t{S}, \t{P}}^{(\prime)}$, as is the case in the SM, the decay width is given by \mbox{\cites[eq.\@ (6.4)]{0709.4174v2}{1204.1737v4}}
\begin{eqn}
  ƒΓ(ƒ{\APBs}(t = 0) → ℓ ℓ) = \frac{G↓F² α↓{e\vphantom{F}}² m_{\PBs}⁵ f_{\PBs}²}{64 \PI³} \abs{\CKM{\Pt \Pb}{\Pt \Ps}}² \sqrt{1 - \frac{4 m_{ℓ}²}{m_{\PBs}²}} \abs{P^ℓ}² \.,
\end{eqn}
where $f_{\PBs}$ is the hadronic decay constant and
\begin{eqn}
  P^ℓ = \frac{2 m_ℓ}{m_{\PBs}²} \g(C_{10}^ℓ - C_{10}'^ℓ) \..
\end{eqn}
To compare with experiment, it is necessary to calculate the $\CP$-averaged, time\-/integrated branching ratio
\begin{eqns}
  \BRa(\PBs → ℓ ℓ)
    &=& \frac{1}{2} \int_0^∞ \dif{t} \g[ƒΓ(ƒ{\APBs}(t) → ℓ ℓ) + ƒΓ(ƒ{\PBs}(t) → ℓ ℓ)] \\
    &=& \frac{1 + \symcal{A}_{\Del{Γ}} y_{\Ps}}{1 - y_{\Ps}²} τ_{\PBs} ƒΓ(ƒ{\APBs}(t = 0) → ℓ ℓ) \.,
\end{eqns}
where $τ_{\PBs}$ is the \PBs meson lifetime and
\begin{eqn}
  \symcal{A}_{\Del{Γ}} = \cos(2 \arg.P.) \\
  y_{\Ps} = \frac{τ_{\PBs} \Del{Γ_{\Ps}}}{2} \..
\end{eqn}
The width difference $\Del{Γ_{\Ps}}$ of the eigenstates of the \PBs system is due to the mixing effects discussed in the next section.

\section{\texorpdfstring{\PBq}{B\_q} mixing}
\label{sec:theory-B-mixing}

The mixing of \PBq and \APBq mesons produces observables which are complementary to the above $\abs{\Del{B}} = 1$ observables.
Meson mixing is described by the Schrödinger equation
\vspace{-\jot}
\begin{eqn}
  \I \partial_t \! \begin{pmatrix} ƒ{\PBq}(t) \\ ƒ{\APBq}(t) \end{pmatrix} = \m{H}^q \! \begin{pmatrix} ƒ{\PBq}(t) \\ ƒ{\APBq}(t) \end{pmatrix} \! \.,
\end{eqn}
where the Hamilton operator is
\begin{eqn}
  \m{H}^q = \m{M}^q - \frac{\I}{2} \m{Γ}^q \.. \label{eqn:theory-Bmixing-Hamilton}
\end{eqn}
The effective Hamiltonian for $\abs{\Del{B}} = 2$ processes is given by \cite[sec.\@ 8.2]{1310.8555v4}
\begin{eqn}
  \H↓{eff}^{\abs{\Del{B}} = 2} = \frac{G↓F² m_{\PW}²}{16 \PI²} \g(\CKM{\Pt \Pb}{\Pt q})^{\! 2} \sum_{\mathclap{q = \Ps, \Pd}} \sum_i \f{C_i^q}(μ) \f{O_i^q}(μ) \.. \label{eqn:theory-Bmixing-Heff}
\end{eqn}
Of the five operators $O_i$, only
\begin{eqn}
  O_1^q = \g[\bar{b} γ_μ \g(1 - γ_5) q] \g[\bar{b} γ^μ \g(1 - γ_5) q]
\end{eqn}
is relevant for the examined observables.
Its matrix element is
\begin{eqn}
  \bra{\APBq} \f{O_{1, \t{R}}^q}(μ) \ket{\PBq \vphantom{\APBq}} = \frac{8}{3} m_{\PBq}² f_{\PBq}² \hat{B}_{\PBq}^{} \g(\f{b_{\PBq}^{}}(μ))^{\! -1} \.,
\end{eqn}
where $\hat{B}_{\PBq}$ is the hadronic bag parameter and the function $ƒ{b_{\PBq}}(μ)$ contains the scale dependence.
The matrix element of the effective Hamiltonian for \PBq mixing is
\begin{eqns}
  \bra{\APBq} \H↓{eff}^{\abs{\Del{B}} = 2} \ket{\PBq \vphantom{\APBq}}
  &=& \frac{G↓F² m_{\PW}²}{16 \PI²} \g(\CKM{\Pt \Pb}{\Pt q})^{\! 2} C_1^q \f{b_{\PBq}^{}}(μ) \bra{\APBq} \f{O_{1, \t{R}}^q}(μ) \ket{\PBq \vphantom{\APBq}} + \hc \IEEEeqnarraynumspace \label{eqn:theory-Bmixing-matrix-element} \\
    &=& \frac{G↓F² m_{\PW}²}{6 \PI²} m_{\PBq}² f_{\PBq}² \hat{B}_{\PBq}^{} \g(\CKM{\Pt \Pb}{\Pt q})^{\! 2} C_1^q + \hc \.,
\end{eqns}
where the scale dependence cancels.
The Wilson coefficient in the SM is given by
\begin{eqn}
  C_1^{q, \t{SM}} = \f{S_0}(x_{\Pt}) η_{2 \PB} \.,
\end{eqn}
where $η_{2 \PB}$ \cites[7]{1008.1593v3}[eq.\@ (XIII.3)]{9512380v1} includes higher order QCD corrections and \mbox{\cite[eq.\@ (XII.4)]{9512380v1}}
\vspace{-\jot}
\begin{eqn}
  \f{S_0}(x) = \frac{4 x - 11 x² + x³}{4 \g(1 - x)²} - \frac{3 x³ \ln.x.}{2 \g(1 - x)³} \.,
  \qquad
  x_{\Pt} = \frac{\f{\overline{m_{\Pt}}}(\overline{m_{\Pt}})^{\! 2}}{m_{\PW}²}
\end{eqn}
is an Inami–Lim function of the $\overline{\text{MS}}$ mass of the top quark.
The matrix elements
\begin{eqn}
  M_{12}^q = \frac{\bra{\APBq} \H↓{eff}^{\abs{\Del{B}} = 2} \ket{\PBq \vphantom{\APBq}}}{2 m_{\PBq}}
\end{eqn}
and $Γ_{12}^q$ are the off-diagonal elements in the Hamilton operator \eqref{eqn:theory-Bmixing-Hamilton}.

The observable
\vspace*{-\jot}
\begin{eqns}
  \Del{m_q}
    &=& 2 \abs{M_{12}^q} + \LandauO(\frac{m_{\Pb}²}{m_{\PW}²}) \\
    &=& \frac{G↓F² m_{\PW}²}{6 \PI²} m_{\PBq}^{} f_{\PBq}² \hat{B}_{\PBq}^{} \abs{\CKM{\Pt \Pb}{\Pt q}}² \abs{C_1^q}
\end{eqns}
is the mass difference of the eigenstates of the Hamilton operator \eqref{eqn:theory-Bmixing-Hamilton}.
The phase
\begin{eqns}
  ϕ_q
    &=& \arg(- \frac{M_{12}^q}{Γ_{12}^q})
  \itext{reduces to}[\vspace{-\jot}][\vspace{-\jot}]
    &=& \arg(C_1^q)
\end{eqns}
given that the SM Wilson coefficient is real, the phase is negligible in the SM and assuming that no NP enters $Γ_{12}^q$.
