\chapter{Statistical methods}
\label{sec:statistics}

The framework of Bayesian statistics, described in section~\ref{sec:statistics-bayes}, will be used for the numerical analysis, the results of which are credible regions, whose theory is summarised in section~\ref{sec:statistics-credible}.
The probability distributions described in section~\ref{sec:statistics-distributions} are used to model priors in the analysis.

\section{Bayesian inference}
\label{sec:statistics-bayes}

Inference, the updating of beliefs to account for new data $\v{D}$, is accomplished by using Bayes' theorem \cite{jaynes}
\begin{eqn}
  ƒp(\v{θ} \given \v{D}, M) = \frac{ƒp(\v{D} \given M, \v{θ}) ƒp(\v{θ} \given M)}{ƒp(\v{D} \given M)}
\end{eqn}
to calculate the posterior probability.
Here $M$ represents a model of reality and $\v{θ}$ are the model parameters.
The denominator
\begin{eqn}
  ƒp(\v{D} \given M) = \int^{} \dif{\v{θ}} ƒp(\v{D} \given M, \v{θ}) ƒp(\v{θ} \given M)
\end{eqn}
is called the evidence and is responsible for normalising the posterior probability.
It is difficult to calculate and unnecessary if normalised probabilities are not required, as is the case here.
The prior $ƒp(\v{θ} \given M)$ encodes the beliefs before knowing the new data.
The likelihood $ƒp(\v{D} \given M, \v{θ})$ gives the probability of finding the data given concrete model parameters.
A measurement is interpreted as an experimental probability distribution $p_j↑{\smash{exp}}$ of observables $ƒ{\v{f}}_j(M, \v{θ})$ which are functions of the model parameters.
The likelihood is thus taken to be
\begin{eqn}
  ƒ{p_j}(\v{D}_j \given M, \v{θ}) = ƒ{p↑{\smash{exp}}_j}(ƒ{\v{f}_j^{}}(M, \v{θ})) \..
\end{eqn}
This assumes that the observables are unambiguous functions of the model parameters, which is not required by Bayes' theorem itself.
Nuisance parameters are used to correctly model uncertainties but are not themselves of interest.
For inference, there is no difference between interesting and nuisance parameters.

\section{Credible regions}
\label{sec:statistics-credible}

A $Φ$ credible region $R$ is a not necessarily connected region of parameter space that contains a fraction $Φ$ of the complete probability
\begin{eqn}
  \int_R^{} \dif{\v{θ}} ƒp(\v{θ}) = Φ \.. \label{eqn:statistics-credible-region}
\end{eqn}
The highest posterior density region
\begin{eqn}
  \argmin_R.\int_R^{} \dif{\v{θ}}. \.,
\end{eqn}
where the candidates $R$ also fulfil \eqref{eqn:statistics-credible-region}, is the smallest credible interval that contains the required probability mass.
This region by definition includes the global modes of the distribution.
In this work, the notation $nσ$ is used to refer to the
\begin{eqn}
  Φ = \erf(\frac{n}{\sqrt{2}})
\end{eqn}
highest posterior density region.
This notation is also used when comparing two values, of which one or both are probability distributions.
Values being compatible at $nσ$ means that the $nσ$ regions of the values overlap.

\section{Probability distributions}
\label{sec:statistics-distributions}

For a Bayesian analysis, the experimental results for observables need to be translated into probability densities.
Generally, an observable $O$ can have physical boundaries
\begin{eqn}
  a₋ ≤ O ≤ a₊
\end{eqn}
that arise from the observable's definition, for example $m ≥ 0$ for a mass.
If the observable is measured as
\vspace{-\jot}
\begin{eqn}
  O = μ \: {}^{+σ₊}_{-σ₋} \.,
\end{eqn}
then the boundaries are not a problem if they are far away from the central value,
\begin{eqn}
  \frac{\abs{μ - a_±}}{σ_±} \gg 1 \.,
\end{eqn}
and a simple normal distribution can be used if the uncertainties are symmetric.

If this is not the case, then a more general distribution must be used to correctly model the probability near the boundaries.
Here it is sufficient to consider the case $σ₊ = ∞$ with only one boundary.
This gives four conditions for the distribution $p$ with cumulative density $c$:
\vspace{-\jot}
\begin{eqn}
  ƒc(a₋) = 0 \., \\
  \argmax.p. = μ \., \\
  ƒc(μ + σ₊) - ƒc(μ - σ₋) = \erf(\tfrac{1}{\sqrt{2}}) \., \\
  ƒp(μ - σ₋) = ƒp(μ + σ₊) \..
\end{eqn}
The first fixes the physical boundary, the second fixes the mode and the last two fix the $1σ$ region to be equal to the experimental result.
These conditions are fulfilled by the Amoroso distribution \cite{1005.3274v2} which is also useful for experimental results with asymmetric uncertainties.

Measurement results are frequently given as frequentist confidence intervals whose interpretation is not so straightforward.
In these cases, it is necessary to assume a Bayesian credible interval equal to the confidence interval.
