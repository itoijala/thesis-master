\chapter{Analysis}
\label{sec:analysis}

A modified version of the EOS program \cite{1006.5013v2, 1111.2558v2} is used to calculate the posterior distributions for the model parameters.
EOS uses a Markov chain Monte Carlo algorithm to draw samples from the posterior.
These samples are then histogrammed and marginalised to the parameters of interest, meaning that unwanted parameters are integrated over.

The modifications to EOS\footnote{The modified version of EOS is available from\\\url{https://bitbucket.org/itoijala/thesis-master-eos}} include support for lepton flavour–dependent Wilson coefficients, addition of the \PBq mixing observables, correctly taking complex Wilson coefficients into account for the untagged branching ratio of $\PBq → ℓℓ$ and implementation of the leptoquark models.

The posterior distributions for the complex combination $\smash{\LL{\Ps ℓ}{\Pb ℓ}}$ of leptoquark couplings for $\SU(2)$ doublets and triplets and $ℓ = \Pe, \Pmu$ are calculated for leptoquark masses of $M = \SI{1}{\TeV}, \SI{10}{\TeV}, \SI{50}{\TeV}$.
A prediction for the yet to be measured observable $R_{\PKstar, \intcc{\SI{1}{\giga\electronvolt\squared}}{\SI{6}{\giga\electronvolt\squared}}}$ is calculated for each of these cases.

\section{Constraints}

For $R_{\PK}$ the experimental result from LHCb \cite{1406.6482v1}
\begin{eqn}
  R_{\PK, \intcc{\SI{1}{\giga\electronvolt\squared}}{\SI{6}{\giga\electronvolt\squared}}}
    = \num{0.745} \: {}^{+\num{0.090}}_{-\num{0.074}} ± \num{0.036}
\end{eqn}
is used to fit an Amoroso distribution due to the asymmetric uncertainties.
Statistical and systematic uncertainties are always assumed to be uncorrelated and are added in quadrature.

For the decay $\PBs → \Pmu \Pmu$ the combined CMS and LHCb \cite{LHCb-CONF-2013-012} result
\begin{eqn}
  \BRa(\PBs → \APmuon \Pmuon) = \num{2.9 ± 0.7 e-9}
\end{eqn}
is used.
Here, the correct behaviour of the Amoroso distribution near the physical boundary is especially important.

The experimental averages
\begin{eqn}
  \Del{m}_{\Ps} = \g(\num{17.757} ± \num{0.020} ± \num{0.007}) \, \si{\per\pico\second} \\
  ϕ_{\Ps} = \num{0.025 ± 0.035}
\end{eqn}
for the \PBs mixing observables from \cite[eqs. (64), (88)]{1412.7515v1} are used as normal distributions.

\section{Nuisance parameters}

For the CKM parameters the results
\begin{eqns}
  λ       &=& \num{0.2255 ± 0.0006} \\
  A       &=& \num{0.818 ± 0.015} \\
  \bar{ρ} &=& \num{0.124 ± 0.024} \\
  \bar{η} &=& \num{0.354 ± 0.015}
\end{eqns}
of the Bayesian analysis of the PDG 2014 data \cite{pdg2014} by the UTfit collaboration \cite{0606167v2} are used.

The necessary hadronic inputs are the decay constant
\cites[eq. (111)]{1310.8555v4}{1112.3051v1, 1110.4510v1, 1202.4914v2}
\begin{eqn}
  f_{\PBs} = \SI{227.7 ± 4.5}{\mega\electronvolt}
\end{eqn}
and the bag parameter
\cites[eq. (125)]{1310.8555v4}{0902.1815v3}
\begin{eqn}
  \hat{B}_{\PBs} = \num{1.33 ± 0.06} \.,
\end{eqn}
both of which are taken from lattice QCD calculations averaged by FLAG.
These two values are correlated when extracted from the lattice.
However, no correlations are given in the literature, so the values are used in an uncorrelated manner.
The values as updated by the Bayesian fit are correlated, meaning that using correlated priors would be an improvement.
