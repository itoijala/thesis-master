\chapter{Leptoquark models}
\label{sec:leptoquark}

In the notation of \cite{9309310v1}, the leptoquarks of mass $M$ examined here have the representations
\vspace{-\jot}
\begin{eqn}
  \g(\SU(3)↓c, \SU(2)↓L, \U(1)_Y) =
  \begin{cases}
    \g(\bar{\symbfup{3}}, \symbfup{2}, - \frac{1}{6}) \., & \PSthalf \\
    \g(\bar{\symbfup{3}}, \symbfup{3}, - \frac{1}{3}) \., & \PSone
  \end{cases}
\end{eqn}
under the SM gauge group.
Other representations are possible, though not studied here.
The $\SU(2)$ multiplets are defined as
\begin{eqn}
  \tilde{S}_{\sfrac{1}{2}}
    = \begin{pmatrix*}[l] \tilde{S}_{\sfrac{1}{2}}^{+ \sfrac{1}{3}} \\ \tilde{S}_{\sfrac{1}{2}}^{- \sfrac{2}{3}} \end{pmatrix*} \! \., \\
  \v{S}_1
    = \begin{pmatrix*}[l] S_{1, +1} \\ S_{1, 0} \\ S_{1, -1} \end{pmatrix*}
    = \sqrt{2} \!
      \begin{pmatrix*}[r] \mathmakebox[\widthof{1}][c]{\I} & 0 & \mathmakebox[\widthof{1}][c]{\I} \\ -1 & 0 & 1 \\ 0 & 1 & 0 \end{pmatrix*}
      \! \begin{pmatrix*}[l] S_1^{+ \sfrac{2}{3}} \\ S_1^{- \sfrac{1}{3}} \\ S_1^{- \sfrac{4}{3}} \end{pmatrix*} \! \.,
\end{eqn}
where the superscripts are electrical charges.
The interactions of the leptoquarks with quarks and leptons are given by the Yukawa interactions in the Lagrangians~\mbox{\cite{9309310v1, 1408.1627v1}}
\begin{eqns}
  \symcal{L}_{\PSthalf}
    &=& - λ_{q ℓ} \tilde{S}_{\sfrac{1}{2}}^\dagger \g(\bar{D}_q P↓L L_ℓ) + \hc \\
    &=& - λ_{q ℓ} \g(\bar{D}_q P↓L l_ℓ) \tilde{S}_{\sfrac{1}{2}}^{+\sfrac{2}{3}} - λ_{q ℓ} \g(\bar{D}_q P↓L ν_ℓ) \tilde{S}_{\sfrac{1}{2}}^{-\sfrac{1}{3}} + \hc \label{eqn:leptoquark-lagrangian-S1/2}
  \vspace{-0.5\jot}
\end{eqns}
and
\vspace{-0.5\jot}
\begin{eqns}[rll]
  \symcal{L}_{\PSone}
  &{}={}& \mathrlap{- \tilde{λ}_{q ℓ} \g(\bar{Q}_q↑c \I σ_2 \v{σ} P↓L L_ℓ) \v{S}_1^\dagger + \hc}
          \hphantom{{} - \sqrt{2} λ_{q ℓ} V_{q q'}^* \g(\bar{U}_{q'}↑c P↓L ν_ℓ) S_1^{-\sfrac{2}{3}} + λ_{q ℓ} V_{q q'}^* \g(\bar{U}_{q'}↑c P↓L l_ℓ) S_1^{+\sfrac{1}{3}} + \hc \.,} \\
    &
      \mathrlap{
        \begin{IEEEeqnarraybox}[][c]{Ll}
          =&                 - \sqrt{2} λ_{q ℓ} \g(\bar{D}_q↑c P↓L l_ℓ) S_1^{+\sfrac{4}{3}} + λ_{q ℓ} \g(\bar{D}_q↑c P↓L ν_ℓ) S_1^{+\sfrac{1}{3}} \\
           & \negmedspace {} - \sqrt{2} λ_{q ℓ} V_{q \smash{q'}}^* \g(\bar{U}_{\smash{q'}\vphantom{q}}↑c P↓L ν_ℓ) S_1^{-\sfrac{2}{3}} + λ_{q ℓ} V_{q \smash{q'}}^* \g(\bar{U}_{\smash{q'}\vphantom{q}}↑c P↓L l_ℓ) S_1^{+\sfrac{1}{3}} + \hc \.,
        \end{IEEEeqnarraybox}
      } & \label{eqn:leptoquark-lagrangian-S1}
\end{eqns}
where $f↑c$ denotes the charge conjugation of field $f$.
The coupling $\smash{\tilde{λ}_{q ℓ}^{\vphantom{*}} = λ_{\smash{q'}\vphantom{q} ℓ}^{\vphantom{*}} V_{\smash{q'} q}^*}$ is defined in order to transfer the difference between the mass and flavour eigenstates to the up-type quarks on the second line of \eqref{eqn:leptoquark-lagrangian-S1}, the effects of which are small \cite{1408.1627v1} and are neglected.
Here, the coupling matrices are limited to the required couplings $λ_{\Pb ℓ}$ and $λ_{\Ps ℓ}$ for $ℓ = \Pe, \Pmu$.
All other couplings are set to zero.
The different values of $ℓ$ are examined as different models, since doing otherwise would lead to $\PBs → \Pmu \Pe$ and $\Pmu → \Pe \Pgamma$, for example, and lepton flavour violation in general.
Although examining such models would be interesting, lepton flavour violation is difficult to implement in the EOS framework used for the analysis.

\section{Effects on \texorpdfstring{$\Pb → \Ps ℓℓ$}{b → sℓℓ} transitions}

The effects of the leptoquarks on the $\abs{\Del{B}} = 1$ Wilson coefficients after Fierz transformation are
\cite[eqs.\@ (39), (53)]{1408.1627v1}
\begin{eqn}
  \kern-\nulldelimiterspace \left.
  \begin{IEEEeqnarraybox}[][c]{r'rClCl}
    \PSthalf \colon & C_9'^{ℓ, \t{LQ}} &=& - C_{10}'^{ℓ, \t{LQ}} &=& -1 \\
    \PSone   \colon & C_9^{ℓ, \t{LQ}}  &=& - C_{10}^{ℓ, \t{LQ}}  &=& -2
  \end{IEEEeqnarraybox}
  \right\} \cdot
  \frac{\LL{\Ps ℓ}{\Pb ℓ}}{4 M²} \g(\frac{4 G↓F}{\sqrt{2}} \CKM{\Pt \Pb}{\Pt \Ps} \frac{α↓e}{4 \PI})^{\!\!\! -1} \..
\end{eqn}
These are created at tree level by the Feynman diagram in figure \ref{fig:leptoquark-feynman-b→s}.

\begin{figure}
  \begin{tikzpicture}[
    scale=2,
    style={font=\vphantom{\strut}},
  ]
    \coordinate (v1) at (0, 0);
    \coordinate (v2) at ($(v1) + (+0, -1)$);

    \draw [fermion] ($(v1) + (-0.5, +0.3)$) -- node [left,  pos=0] {\Pb} (v1);
    \draw [fermion] (v1)                    -- node [right, pos=1] {$ℓ$} ($(v1) + (+0.5, +0.3)$);

    \draw [scalar] (v1) -- node [right, pos=0.5] {\PSthalf, \PSone} (v2);

    \draw [antifermion]     ($(v2) + (-0.5, -0.3)$) -- node [left,  pos=0] {\Ps} (v2);
    \draw [antifermion]     (v2)                    -- node [right, pos=1] {$ℓ$} ($(v2) + (+0.5, -0.3)$);
  \end{tikzpicture}
  \caption{Tree diagram of $\Pb → \Ps ℓℓ$ mediated by a leptoquark.}
  \label{fig:leptoquark-feynman-b→s}
\end{figure}

\section{Effects on \texorpdfstring{\PBs}{B\_s} mixing}

The mixing of \PBs mesons is induced by the box diagrams in figure~\ref{fig:leptoquark-feynman-mixing}.
The contributions of neutrinos in the loops and the case of $\SU(2)$ triplet leptoquarks can be easily obtained from the case of doublet leptoquarks and charged leptons detailed here.
Using $P↓L = \g(1 - γ_5)/2$ and the properties of the $γ$-matrices, each diagram contributes
\vspace{-\jot}
\begin{eqn}
  \I \mathcal{A}
    = \frac{1}{4} \LLL{\Ps ℓ}{\Pb ℓ}
      \g[\bar{b} γ_μ \g(1 - γ_5) s] \g[\bar{b} γ_ν \g(1 - γ_5) s]
      \overbrace{
        \int^{} \frac{\dif{⁴ k}}{\g(2 \PI)⁴} \frac{k^μ k^ν}{\g(k²)² \g(k² - M²)²}
      }^I
      \IEEEeqnarraynumspace
\end{eqn}
to the amplitude for vanishing lepton masses and external momenta.
Using the Feynman parameterisation \cite[eq.\@ (2)]{0508013v1}
\begin{eqn}
  \frac{1}{\prod A_j^{α_j}}
    = \frac{\GammaF(\sum α_j)}{\prod \GammaF(α_j)}
      \int_0^1 \g(\prod \dif{x_j}) \Diracdelta(1 - \sum x_j)
      \frac{\prod x_j^{α_j - 1}}{\g[\sum x_j A_j]^{\sum α_j}} \.,
\end{eqn}
where all the sums and products are over $j$, the integral can be written as
\begin{eqn}
  I = \GammaF(4) \int_0^1 \dif{x} x \g(1 - x)
    \int^{} \frac{\dif{⁴ k}}{\g(2 \PI)⁴} \frac{k^μ k^ν}{\g[k² - \g(1 - x) M²]⁴} \..
\end{eqn}
The $k$-integration can be solved using the master integral \cite[eq.\@ (A.46)]{peskin-schroeder}
\begin{eqn}
  \int^{} \frac{\dif{^d k}}{\g(2 \PI)^d} \frac{k^μ k^ν}{\g(k² - R)^n}
    = \frac{\g(-1)^{n - 1}}{\g(4 \PI)^{d/2}}
      \frac{\I g^{μν}}{2} \frac{\GammaF(n - \frac{d}{2} - 1)}{\GammaF(n)}
      \g(\frac{1}{R})^{\! n - \frac{d}{2} - 1}
\end{eqn}
for $d = 4$ dimensions, leaving
\begin{eqn}
  I = - \frac{\I g^{μν}}{32 \PI² M²} \int_0^1 \dif{x} x \..
  \vspace{-\jot}
\end{eqn}
This leads to the sum
\begin{eqn}
  \I \mathcal{A}
    = - \I \frac{\LLL{\Ps ℓ}{\Pb ℓ}}{128 \PI² M²}
      \g[\bar{b} γ_μ \g(1 - γ_5) s] \g[\bar{b} γ^μ \g(1 - γ_5) s]
\end{eqn}
of both diagrams.
Adding the neutrino contributions and comparing with the effective theory \eqref{eqn:theory-Bmixing-Heff} gives the Wilson coefficients
\begin{eqn}
  \kern-\nulldelimiterspace \left.
  \begin{IEEEeqnarraybox}[][c]{r'rCl}
    \PSthalf \colon & C_1↑{LQ} &=& \underbrace{\g(1 + 1)}_{\smash{\raisebox{-0.35em}{$\displaystyle \hphantom{1} \mathllap{ℓ} + \mathrlap{ν} \hphantom{1}$}}} \\
    \PSone   \colon & C_1↑{LQ} &=&  \overbrace{\g(4 + 1)}
  \end{IEEEeqnarraybox}
  \right\} \cdot
  \frac{\LLL{\Ps ℓ}{\Pb ℓ}}{128 \PI² M²} \g(\frac{G↓F² m_{\PW}²}{16 \PI²} \g(\CKM{\Pt \Pb}{\Pt \Ps})^{\! 2})^{\!\!\! -1} \., \label{eqn:leptoquark-B-mixing}
\end{eqn}
where the first contributions are from charged leptons in the loop and the second contributions from neutrinos.
The $4$ is caused by the $\sqrt{2}$ in \eqref{eqn:leptoquark-lagrangian-S1}.
This result is in agreement with the literature \cites{9309310v1}[eq.\@ (14)]{1003.1384v2}.
Since the diagrams are not divergent, no renormalisation is necessary.
For this reason, no scale $μ$ enters the expressions for the Wilson coefficients.
The problem that the scale dependence in \eqref{eqn:theory-Bmixing-matrix-element} no longer cancels is ignored here.
Using next-to-leading order (NLO) QCD corrections would solve this problem.

\begin{figure}
  \begin{tikzpicture}[
    scale=2,
    style={font=\vphantom{\strut}},
  ]
    \coordinate (v1) at (0, 0);
    \coordinate (v2) at ($(v1) + (+0, -1)$);
    \coordinate (v3) at ($(v1) + (+1, +0)$);
    \coordinate (v4) at ($(v1) + (+1, -1)$);

    \draw [fermion] ($(v1) + (-0.5, 0)$) -- node [left, pos=0] {\Pb} (v1);
    \draw [antifermion]     ($(v2) + (-0.5, 0)$) -- node [left, pos=0] {\Ps} (v2);

    \draw [scalar] (v1) -- node [left,  pos=0.5] {\PSthalf, \PSone} (v2);
    \draw [scalar] (v3) -- node [right, pos=0.5] {\PSthalf, \PSone} (v4);

    \draw [fermion] (v1) -- node [above, pos=0.5] {$ℓ, ν$} (v3);
    \draw [antifermion]     (v2) -- node [below, pos=0.5] {$ℓ, ν$} (v4);

    \draw [fermion] (v3) -- node [right, pos=1] {\Ps} ($(v3) + (+0.5, 0)$);
    \draw [antifermion]     (v4) -- node [right, pos=1] {\Pb} ($(v4) + (+0.5, 0)$);


    \coordinate (v1) at (3.5, 0);
    \coordinate (v2) at ($(v1) + (+0, -1)$);
    \coordinate (v3) at ($(v1) + (+1, +0)$);
    \coordinate (v4) at ($(v1) + (+1, -1)$);

    \draw [fermion] ($(v1) + (-0.5, 0)$) -- node [left, pos=0] {\Pb} (v1);
    \draw [antifermion]     ($(v2) + (-0.5, 0)$) -- node [left, pos=0] {\Ps} (v2);

    \draw [scalar] (v1) -- node [above, pos=0.5] {\PSthalf, \PSone} (v3);
    \draw [scalar] (v2) -- node [below, pos=0.5] {\PSthalf, \PSone} (v4);

    \draw [fermion] (v1) -- node [left,  pos=0.5] {$ℓ, ν$} (v2);
    \draw [antifermion]     (v3) -- node [right, pos=0.5] {$ℓ, ν$} (v4);

    \draw [fermion] (v3) -- node [right, pos=1] {\Ps} ($(v3) + (+0.5, 0)$);
    \draw [antifermion]     (v4) -- node [right, pos=1] {\Pb} ($(v4) + (+0.5, 0)$);
  \end{tikzpicture}
  \caption{Box diagrams for \PBs mixing mediated by leptoquarks.}
  \label{fig:leptoquark-feynman-mixing}
\end{figure}

\section{Leptoquarks at the LHC}

Leptoquark production at a hadron collider occurs through the couplings to fermions and gluons.
Depending on the magnitude of the Yukawa couplings, single production becomes more or less important compared to pair production.
For small Yukawa couplings, only gluon fusion diagrams such as figure~\ref{fig:leptoquark-lhc-production-2g} are relevant \cite{1406.4831v2}.
For larger couplings, single production and the pair production $t$-channel diagrams with fermions must be taken into account.

For decays of a leptoquark to a quark and a lepton the decay rate is \cite[eq.\@ (7)]{9309310v1}
\vspace{-\jot}
\begin{eqn}
  ƒΓ(\PS → q ℓ) = \frac{\abs{λ_{q ℓ}}² M}{16 \PI} \..
\end{eqn}
Since the product $\LL{\Ps ℓ}{\Pb ℓ}$ is constrained, a simple calculation shows that the minimal width is given by
\vspace{-\jot}
\begin{eqn}
  Γ↓{tot} = 2 ƒΓ(λ² = \LL{\Ps ℓ}{\Pb ℓ}) \..
\end{eqn}
To see if hadronisation of the leptoquark is possible, the total decay width is compared to the QCD scale $Λ↓{QCD} ≈ \SI{200}{\mega\electronvolt}$.
For the leptoquarks with the smallest decay rate of those examined here
\begin{eqn}
  ƒ{Γ↓{tot}}(λ² ≈ \num{0.01}, M ≈ \SI{1}{\TeV}) ≈ \SI{400}{\mega\electronvolt} \.,
\end{eqn}
meaning that, even for the smallest studied masses, the leptoquark does not hadronise.
In all cases, the signal to search for is a quark, experimentally a jet, and a lepton originating from the same vertex.

Experimental bounds on leptoquarks are only available for single-generation leptoquarks.
These searches \cite{CMS-PAS-EXO-12-042, 1406.4831v2, 1508.04735v1} consistently find that masses $M ≲ \SI{1}{\TeV}$ are excluded.
The applicability of these results to leptoquarks that couple to both second and third generation quarks is not clear, though the order of magnitude of excluded masses in a hypothetical search is expected to be the same as for single-generation leptoquarks.

\begin{figure}
  \begin{tikzpicture}[
    scale=2,
    style={font=\vphantom{\strut}},
  ]
    \coordinate (v) at (0, 0);
    \coordinate (v1) at ($(v) + (-0.2, 0)$);
    \coordinate (v2) at ($(v) + (+0.2, 0)$);

    \draw [fermion]                    ($(v1) + (-0.3, +0.5)$) -- node [left, pos=0] {$q$}     (v1);
    \draw [gluon, decoration={mirror}] ($(v1) + (-0.3, -0.5)$) -- node [left, pos=0] {\Pgluon} (v1);

    \draw [fermion] (v1) -- node [below, pos=0.5] {$q$} (v2);

    \draw [fermion] (v2) -- node [right, pos=1] {$ℓ$} ($(v2) + (+0.3, +0.5)$);
    \draw [scalar]  (v2) -- node [right, pos=1] {\PS} ($(v2) + (+0.3, -0.5)$);

    \node () at ($(v) + (0, -0.7)$) {%
      \subcaptionbox{%
        $s$-channel%
        \settowidth{\dimen0}{\ }\hspace*{-\dimen0}%
        \label{fig:leptoquark-lhc-production-1s}%
      }[0.18\textwidth]{}%
    };


    \coordinate (v) at (+1.84, 0);
    \coordinate (v1) at ($(v) + (0, +0.2)$);
    \coordinate (v2) at ($(v) + (0, -0.2)$);

    \draw [fermion] ($(v1) + (-0.5, +0.3)$) -- node [left,  pos=0] {$q$} (v1);
    \draw [fermion] (v1)                    -- node [right, pos=1] {$ℓ$} ($(v1) + (+0.5, +0.3)$);

    \draw [scalar] (v1) -- node [right, pos=0.5] {\APS} (v2);

    \draw [gluon, decoration={mirror}]  ($(v2) + (-0.5, -0.3)$) -- node [left,  pos=0] {\Pgluon} (v2);
    \draw [scalar]                      (v2)                    -- node [right, pos=1] {\PS}     ($(v2) + (+0.5, -0.3)$);

    \node () at ($(v) + (0, -0.7)$) {%
      \subcaptionbox{%
        $t$-channel%
        \settowidth{\dimen0}{\ }\hspace*{-\dimen0}%
        \label{fig:leptoquark-lhc-production-1t}%
      }[0.18\textwidth]{}%
    };


    \coordinate (v) at (+3.68, 0);

    \draw [gluon]                      ($(v) + (-0.5, +0.5)$) -- node [left, pos=0] {\Pgluon} (v);
    \draw [gluon, decoration={mirror}] ($(v) + (-0.5, -0.5)$) -- node [left, pos=0] {\Pgluon} (v);

    \draw [scalar] (v) -- node [right, pos=1] {\APS} ($(v) + (+0.5, +0.5)$);
    \draw [scalar] (v) -- node [right, pos=1] {\PS}  ($(v) + (+0.5, -0.5)$);

    \node () at ($(v) + (0, -0.7)$) {%
      \subcaptionbox{%
        Gluon fusion%
        \settowidth{\dimen0}{\ }\hspace*{-\dimen0}%
        \label{fig:leptoquark-lhc-production-2g}%
      }[0.25\textwidth]{}%
    };


    \coordinate (v) at (+5.52, 0);
    \coordinate (v1) at ($(v) + (0, +0.2)$);
    \coordinate (v2) at ($(v) + (0, -0.2)$);

    \draw [antifermion] ($(v1) + (-0.5, +0.3)$) -- node [left,  pos=0] {$\mathrlap{q'}\hphantom{q}$} (v1);
    \draw [scalar]      (v1)                    -- node [right, pos=1] {\APS}                        ($(v1) + (+0.5, +0.3)$);

    \draw [antifermion] (v1) -- node [right, pos=0.5] {$ℓ$} (v2);

    \draw [fermion] ($(v2) + (-0.5, -0.3)$) -- node [left,  pos=0] {$q$} (v2);
    \draw [scalar]  (v2)                    -- node [right, pos=1] {\PS} ($(v2) + (+0.5, -0.3)$);

    \node () at ($(v) + (0, -0.7)$) {%
      \subcaptionbox{%
        $t$-channel%
        \settowidth{\dimen0}{\ }\hspace*{-\dimen0}%
        \label{fig:leptoquark-lhc-production-2t}%
      }[0.18\textwidth]{}%
    };
  \end{tikzpicture}
  \caption{
    Leptoquark production mechanisms at hadron colliders, adapted from \cite{1406.4831v2}.
    Both single (\subref{fig:leptoquark-lhc-production-1s} and \subref{fig:leptoquark-lhc-production-1t} at lowest order) and pair production (\subref{fig:leptoquark-lhc-production-2g} and \subref{fig:leptoquark-lhc-production-2t} are representative) are possible.
  }
  \label{fig:leptoquark-lhc-production}
\end{figure}
