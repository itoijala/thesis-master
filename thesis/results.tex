\chapter{Results}
\label{sec:results}

The constraints from the \PBs mixing observables $\Del{m_{\Ps}}$ and $ϕ_{\Ps}$ are first combined in section~\ref{sec:results-B-mixing} in order to simplify the analysis in the following sections.
The results for leptoquark masses of $M = \SI{1}{\TeV}, \SI{10}{\TeV}, \SI{50}{\TeV}$ are presented in sections~\ref{sec:results-M_1}, \ref{sec:results-M_10} and~\ref{sec:results-M_50}, respectively.

\section{\texorpdfstring{\PBs}{Bₛ} mixing}
\label{sec:results-B-mixing}

The constraints from \PBs mixing on the leptoquark couplings are shown in figure~\ref{fig:results-B-mixing}.
The real part of the coupling is constrained effectively by the mass difference $\Del{m}_{\Ps}$ and the imaginary part by the phase $ϕ_{\Ps}$.
For $\Del{m}_{\Ps}$, it is necessary to include all of the nuisance parameters in order to correctly model the theoretical uncertainties due to the high experimental precision.
It is sufficient to calculate the posteriors only for one of the four models, as the other cases can be found by scaling the couplings by a constant factor evident from \eqref{eqn:leptoquark-B-mixing}.
These observables are independent of the lepton flavour that the leptoquark couples to.

\begin{figure}
  \includegraphics{plot/B-mixing.pdf}%
  \rlap{\raisebox{2.75cm}{\includegraphics{plot/legend-B-mixing.pdf}}}%
  \caption{
    Constraints on $\SU(2)$ doublet leptoquark couplings from \PBs mixing at $M = \SI{1}{\TeV}$.
    For triplets, the couplings are scaled down by a factor of~$\sqrt{2/5}$.
  }%
  \label{fig:results-B-mixing}
  \vspace*{\baselineskip}
\end{figure}

\section{\texorpdfstring{$M = \SI{1}{\TeV}$}{M = 1 TeV}}
\label{sec:results-M_1}

Figure~\ref{fig:results-M_1-combined} shows the constraints on the leptoquark couplings at $M = \SI{1}{\TeV}$.
The model predictions for $\RK$ from the posterior are shown in figure~\ref{fig:results-M_1-R_K}.
As is visible from subfigures~\ref{fig:results-M_1-combined-μ-2} and~\ref{fig:results-M_1-R_K-μ-2}, the constraints from $\RK$ and $\PBs → \Pmu \Pmu$ are not fully compatible, leading to the model not fully reproducing the measured distribution.
However, the incompatibility is small and the model is still compatible with experiment within $1σ$.
More precise measurements of either observable could rule out the $\SU(2)$ doublet leptoquark model that couples to muons.

The model predictions for $\RKs$ are shown in figure~\ref{fig:results-M_1-R_Kstar}.
The $\SU(2)$ triplet coupling to electrons predicts $\RKs < 1$ while the $\SU(2)$ doublet coupling to muons predicts $\RKs > 1$.
Both predictions are at levels greater than $2σ$.
The other two models have no clear prediction for $\RKs$ that would allow a measurement to rule them out.

For electrons it is also possible to predict $\smash{\BRa(\PBs → \Pe \Pe)}$.
All values are below \smash{\num{e-12}} and most are of the order of \smash{\num{e-13}}, which can be compared to the SM prediction $\num{7.56 ± 0.32 e-14}$ \cite{0709.4174v2} and the current upper limit $\num{3.7e-7}$ (\SI{95}{\percent} C.L.) \cite{CDF-note-9413}.
These leptoquark effects on $\smash{\BRa(\PBs → \Pe \Pe)}$ are not going to be measurable in the foreseeable future and so no plots are provided.

\begin{figure}
  \begin{subfigure}{0.495\textwidth}
    \includegraphics{plot/e-2-M_1-combined.pdf}%
    \hfill%
    \caption{$ℓ = \Pe$, $\SU(2)$ doublet}%
    \label{fig:results-M_1-combined-e-2}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.495\textwidth}
    \hfill%
    \includegraphics{plot/e-3-M_1-combined.pdf}%
    \caption{$ℓ = \Pe$, $\SU(2)$ triplet}%
    \label{fig:results-M_1-combined-e-3}
  \end{subfigure}
  \\[0.7\baselineskip]
  \includegraphics{plot/legend-combined.pdf}
  \\[0.7\baselineskip]
  \begin{subfigure}{0.495\textwidth}
    \includegraphics{plot/μ-2-M_1-combined.pdf}%
    \hfill%
    \caption{$ℓ = \Pmu$, $\SU(2)$ doublet}%
    \label{fig:results-M_1-combined-μ-2}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.495\textwidth}
    \hfill%
    \includegraphics{plot/μ-3-M_1-combined.pdf}%
    \caption{$ℓ = \Pmu$, $\SU(2)$ triplet}%
    \label{fig:results-M_1-combined-μ-3}
  \end{subfigure}
  \caption{
    Constraints on leptoquark couplings at $M = \SI{1}{\TeV}$.
    Shown are the $2σ$ regions.
  }
  \label{fig:results-M_1-combined}
\end{figure}

\begin{figure}
  \begin{subfigure}{0.495\textwidth}
    \includegraphics{plot/e-2-M_1-R_K.pdf}%
    \hfill%
    \caption{$ℓ = \Pe$, $\SU(2)$ doublet}%
    \label{fig:results-M_1-R_K-e-2}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.495\textwidth}
    \hfill%
    \includegraphics{plot/e-3-M_1-R_K.pdf}%
    \caption{$ℓ = \Pe$, $\SU(2)$ triplet}%
    \label{fig:results-M_1-R_K-e-3}
  \end{subfigure}
  \\[\baselineskip]
  \begin{subfigure}{0.495\textwidth}
    \includegraphics{plot/μ-2-M_1-R_K.pdf}%
    \hfill%
    \caption{$ℓ = \Pmu$, $\SU(2)$ doublet}%
    \label{fig:results-M_1-R_K-μ-2}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.495\textwidth}
    \hfill%
    \includegraphics{plot/μ-3-M_1-R_K.pdf}%
    \caption{$ℓ = \Pmu$, $\SU(2)$ triplet}%
    \label{fig:results-M_1-R_K-μ-3}
  \end{subfigure}
  \caption{
    Value of $\RK$ as measured (dashed line) and as predicted by the models at $M = \SI{1}{\TeV}$ ($1σ$, $2σ$ and $3σ$ regions).
  }
  \label{fig:results-M_1-R_K}
\end{figure}

\begin{figure}
  \begin{subfigure}{0.495\textwidth}
    \includegraphics{plot/e-2-M_1-R_Kstar.pdf}%
    \hfill%
    \caption{$ℓ = \Pe$, $\SU(2)$ doublet}%
    \label{fig:results-M_1-R_Kstar-e-2}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.495\textwidth}
    \hfill%
    \includegraphics{plot/e-3-M_1-R_Kstar.pdf}%
    \caption{$ℓ = \Pe$, $\SU(2)$ triplet}%
    \label{fig:results-M_1-R_Kstar-e-3}
  \end{subfigure}
  \\[\baselineskip]
  \begin{subfigure}{0.495\textwidth}
    \includegraphics{plot/μ-2-M_1-R_Kstar.pdf}%
    \hfill%
    \caption{$ℓ = \Pmu$, $\SU(2)$ doublet}%
    \label{fig:results-M_1-R_Kstar-μ-2}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.495\textwidth}
    \hfill%
    \includegraphics{plot/μ-3-M_1-R_Kstar.pdf}%
    \caption{$ℓ = \Pmu$, $\SU(2)$ triplet}%
    \label{fig:results-M_1-R_Kstar-μ-3}
  \end{subfigure}
  \caption{
    Value of $\RKs$ as predicted by the models at $M = \SI{1}{\TeV}$ ($1σ$, $2σ$ and $3σ$ regions).
  }
  \label{fig:results-M_1-R_Kstar}
\end{figure}

\section{\texorpdfstring{$M = \SI{10}{\TeV}$}{M = 10 TeV}}
\label{sec:results-M_10}

The constraints on the leptoquark couplings at $M = \SI{10}{\TeV}$ are shown in figure~\ref{fig:results-M_10-combined}.
The results for $\RK$ are shown in figure~\ref{fig:results-M_10-R_K}.
The constraints from \PBs mixing start to play a role at these higher masses and so all predictions of $\RK$ are higher than the measurement, though by no more than $1σ$.
Figure~\ref{fig:results-M_10-combined-μ-2} shows nicely the disagreement between $\RK$ and $\PBs → \Pmu \Pmu$.

The predictions for $\RKs$ in figure \ref{fig:results-M_10-R_Kstar} are partly stronger than at $M = \SI{1}{\TeV}$, since every model predicts quite unambiguously the sign of $\RKs - 1$.
The doublets predict $\RKs > 1$ and the triplets that $\RKs < 1$.

\begin{figure}
  \begin{subfigure}{0.495\textwidth}
    \includegraphics{plot/e-2-M_10-combined.pdf}%
    \hfill%
    \caption{$ℓ = \Pe$, $\SU(2)$ doublet}%
    \label{fig:results-M_10-combined-e-2}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.495\textwidth}
    \hfill%
    \includegraphics{plot/e-3-M_10-combined.pdf}%
    \caption{$ℓ = \Pe$, $\SU(2)$ triplet}%
    \label{fig:results-M_10-combined-e-3}
  \end{subfigure}
  \\[0.7\baselineskip]
  \includegraphics{plot/legend-combined.pdf}
  \\[0.7\baselineskip]
  \begin{subfigure}{0.495\textwidth}
    \includegraphics{plot/μ-2-M_10-combined.pdf}%
    \hfill%
    \caption{$ℓ = \Pmu$, $\SU(2)$ doublet}%
    \label{fig:results-M_10-combined-μ-2}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.495\textwidth}
    \hfill%
    \includegraphics{plot/μ-3-M_10-combined.pdf}%
    \caption{$ℓ = \Pmu$, $\SU(2)$ triplet}%
    \label{fig:results-M_10-combined-μ-3}
  \end{subfigure}
  \caption{
    Constraints on leptoquark couplings at $M = \SI{10}{\TeV}$.
    Shown are the $2σ$ regions.
  }
  \label{fig:results-M_10-combined}
  \vspace*{-\baselineskip}
\end{figure}

\begin{figure}
  \begin{subfigure}{0.495\textwidth}
    \includegraphics{plot/e-2-M_10-R_K.pdf}%
    \hfill%
    \caption{$ℓ = \Pe$, $\SU(2)$ doublet}%
    \label{fig:results-M_10-R_K-e-2}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.495\textwidth}
    \hfill%
    \includegraphics{plot/e-3-M_10-R_K.pdf}%
    \caption{$ℓ = \Pe$, $\SU(2)$ triplet}%
    \label{fig:results-M_10-R_K-e-3}
  \end{subfigure}
  \\[\baselineskip]
  \begin{subfigure}{0.495\textwidth}
    \includegraphics{plot/μ-2-M_10-R_K.pdf}%
    \hfill%
    \caption{$ℓ = \Pmu$, $\SU(2)$ doublet}%
    \label{fig:results-M_10-R_K-μ-2}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.495\textwidth}
    \hfill%
    \includegraphics{plot/μ-3-M_10-R_K.pdf}%
    \caption{$ℓ = \Pmu$, $\SU(2)$ triplet}%
    \label{fig:results-M_10-R_K-μ-3}
  \end{subfigure}
  \caption{
    Value of $\RK$ as measured (dashed line) and as predicted by the models at $M = \SI{10}{\TeV}$ ($1σ$, $2σ$ and $3σ$ regions).
  }
  \label{fig:results-M_10-R_K}
\end{figure}

\begin{figure}
  \begin{subfigure}{0.495\textwidth}
    \includegraphics{plot/e-2-M_10-R_Kstar.pdf}%
    \hfill%
    \caption{$ℓ = \Pe$, $\SU(2)$ doublet}%
    \label{fig:results-M_10-R_Kstar-e-2}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.495\textwidth}
    \hfill%
    \includegraphics{plot/e-3-M_10-R_Kstar.pdf}%
    \caption{$ℓ = \Pe$, $\SU(2)$ triplet}%
    \label{fig:results-M_10-R_Kstar-e-3}
  \end{subfigure}
  \\[\baselineskip]
  \begin{subfigure}{0.495\textwidth}
    \includegraphics{plot/μ-2-M_10-R_Kstar.pdf}%
    \hfill%
    \caption{$ℓ = \Pmu$, $\SU(2)$ doublet}%
    \label{fig:results-M_10-R_Kstar-μ-2}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.495\textwidth}
    \hfill%
    \includegraphics{plot/μ-3-M_10-R_Kstar.pdf}%
    \caption{$ℓ = \Pmu$, $\SU(2)$ triplet}%
    \label{fig:results-M_10-R_Kstar-μ-3}
  \end{subfigure}
  \caption{
    Value of $\RKs$ as predicted by the models at $M = \SI{10}{\TeV}$ ($1σ$, $2σ$ and $3σ$ regions).
  }
  \label{fig:results-M_10-R_Kstar}
\end{figure}

\section{\texorpdfstring{$M = \SI{50}{\TeV}$}{M = 50 TeV}}
\label{sec:results-M_50}

Figure~\ref{fig:results-M_50-combined} shows the constraints on the leptoquark couplings at $M = \SI{50}{\TeV}$.
At such a high mass, \PBs mixing constraints are by far the strongest and do not allow an explanation of $\RK$, as can be seen in figure~\ref{fig:results-M_50-R_K}.
The predictions for $\RKs$ in figure~\ref{fig:results-M_50-R_Kstar} are therefore also unsurprisingly close to unity.

\begin{figure}
  \begin{subfigure}{0.495\textwidth}
    \includegraphics{plot/e-2-M_50-combined.pdf}%
    \hfill%
    \caption{$ℓ = \Pe$, $\SU(2)$ doublet}%
    \label{fig:results-M_50-combined-e-2}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.495\textwidth}
    \hfill%
    \includegraphics{plot/e-3-M_50-combined.pdf}%
    \caption{$ℓ = \Pe$, $\SU(2)$ triplet}%
    \label{fig:results-M_50-combined-e-3}
  \end{subfigure}
  \\[0.7\baselineskip]
  \includegraphics{plot/legend-combined.pdf}
  \\[0.7\baselineskip]
  \begin{subfigure}{0.495\textwidth}
    \includegraphics{plot/μ-2-M_50-combined.pdf}%
    \hfill%
    \caption{$ℓ = \Pmu$, $\SU(2)$ doublet}%
    \label{fig:results-M_50-combined-μ-2}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.495\textwidth}
    \hfill%
    \includegraphics{plot/μ-3-M_50-combined.pdf}%
    \caption{$ℓ = \Pmu$, $\SU(2)$ triplet}%
    \label{fig:results-M_50-combined-μ-3}
  \end{subfigure}
  \caption{
    Constraints on leptoquark couplings at $M = \SI{50}{\TeV}$.
    Shown are the $2σ$ regions.
  }
  \label{fig:results-M_50-combined}
\end{figure}

\begin{figure}
  \begin{subfigure}{0.495\textwidth}
    \includegraphics{plot/e-2-M_50-R_K.pdf}%
    \hfill%
    \caption{$ℓ = \Pe$, $\SU(2)$ doublet}%
    \label{fig:results-M_50-R_K-e-2}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.495\textwidth}
    \hfill%
    \includegraphics{plot/e-3-M_50-R_K.pdf}%
    \caption{$ℓ = \Pe$, $\SU(2)$ triplet}%
    \label{fig:results-M_50-R_K-e-3}
  \end{subfigure}
  \\[\baselineskip]
  \begin{subfigure}{0.495\textwidth}
    \includegraphics{plot/μ-2-M_50-R_K.pdf}%
    \hfill%
    \caption{$ℓ = \Pmu$, $\SU(2)$ doublet}%
    \label{fig:results-M_50-R_K-μ-2}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.495\textwidth}
    \hfill%
    \includegraphics{plot/μ-3-M_50-R_K.pdf}%
    \caption{$ℓ = \Pmu$, $\SU(2)$ triplet}%
    \label{fig:results-M_50-R_K-μ-3}
  \end{subfigure}
  \caption{
    Value of $\RK$ as measured (dashed line) and as predicted by the models at $M = \SI{50}{\TeV}$ ($1σ$, $2σ$ and $3σ$ regions).
  }
  \label{fig:results-M_50-R_K}
\end{figure}

\begin{figure}
  \begin{subfigure}{0.495\textwidth}
    \includegraphics{plot/e-2-M_50-R_Kstar.pdf}%
    \hfill%
    \caption{$ℓ = \Pe$, $\SU(2)$ doublet}%
    \label{fig:results-M_50-R_Kstar-e-2}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.495\textwidth}
    \hfill%
    \includegraphics{plot/e-3-M_50-R_Kstar.pdf}%
    \caption{$ℓ = \Pe$, $\SU(2)$ triplet}%
    \label{fig:results-M_50-R_Kstar-e-3}
  \end{subfigure}
  \\[\baselineskip]
  \begin{subfigure}{0.495\textwidth}
    \includegraphics{plot/μ-2-M_50-R_Kstar.pdf}%
    \hfill%
    \caption{$ℓ = \Pmu$, $\SU(2)$ doublet}%
    \label{fig:results-M_50-R_Kstar-μ-2}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.495\textwidth}
    \hfill%
    \includegraphics{plot/μ-3-M_50-R_Kstar.pdf}%
    \caption{$ℓ = \Pmu$, $\SU(2)$ triplet}%
    \label{fig:results-M_50-R_Kstar-μ-3}
  \end{subfigure}
  \caption{
    Value of $\RKs$ as predicted by the models at $M = \SI{50}{\TeV}$ ($1σ$, $2σ$ and $3σ$ regions).
  }
  \label{fig:results-M_50-R_Kstar}
\end{figure}
