#!/bin/bash

. eos-scripts/eos.sh

. run/common.sh

. run/combined.sh

. run/e-combined.sh

. run/su2.sh 2

. run/M.sh 10

PARAMETERS_SCAN=(
	"Re{ll_LQ} -1e0 1e0 --prior flat"
	"Im{ll_LQ} -1e0 1e0 --prior flat"
)

MCMC_CHUNKS=2500

main "$@"
