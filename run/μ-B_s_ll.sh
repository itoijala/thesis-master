#!/bin/bash

. eos-scripts/eos.sh

. run/common.sh

. run/l.sh mu

CONSTRAINTS=(
	"${CONSTRAINT_B_s_mumu[@]}"
)

PARAMETERS_SCAN=(
	"Re{ll_LQ} -1e-1 1e-1 --prior flat"
	"Im{ll_LQ} -1e-1 1e-1 --prior flat"
)

PREDICTIONS=(
	"${PREDICTION_B_s_mumu[@]}"
)

MCMC_PRERUN_UPDATE=1000
MCMC_PRERUN_MIN=10000
MCMC_CHUNKS=25
MCMC_CHUNK_SIZE=1000000

main "$@"
