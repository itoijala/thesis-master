GLOBAL_OPTIONS=(
	"model Leptoquark"
	"scan-mode cartesian"
)

PROPAGATE_USE_MCMC=true

MERGE_PROPAGATE_PMC_SPARSE="--sparse"

CONSTRAINT_R_K=(
	"B^+->K^+l^+l^-::R_Kavg[1.00,6.00]@LHCb-2014-Amoroso"
)

CONSTRAINT_B_s_mumu=(
	"B^0_s->mu^+mu^-::BR@CMS-LHCb-2013"
)

CONSTRAINT_Deltam=(
	"B-mixing::Delta_m_s@HFAG-2014"
)

CONSTRAINT_phi_12=(
	"B-mixing::phi_12_s@HFAG-2014"
)

NUISANCE_CKM=(
	"CKM::lambda 6 --prior gaussian +0.2249 +0.2255 +0.2261"
	"CKM::A      6 --prior gaussian +0.803  +0.818  +0.833 "
	"CKM::rhobar 6 --prior gaussian +0.100  +0.124  +0.148 "
	"CKM::etabar 6 --prior gaussian +0.339  +0.354  +0.369 "
)

NUISANCE_BAG_PARAMETER=(
	"bag-parameter::B_s 6 --prior gaussian +1.27 +1.33 +1.39"
)

NUISANCE_DECAY_CONSTANT=(
	"decay-constant::B_s 6 --prior gaussian +0.2232 +0.2277 +0.2322"
)

PREDICTION_R_K=(
	--kinematics s_min 1.00 --kinematics s_max 6.00 --observable "B->Kll::R_Kavg@LargeRecoil,q=u"
)

PREDICTION_B_s_mumu=(
	--observable "B_q->ll::BR@Untagged,q=s,l=mu"
)

PREDICTION_B_s_ee=(
	--observable "B_q->ll::BR@Untagged,q=s,l=e"
)

PREDICTION_Deltam=(
	--observable "B-mixing::Delta_m,q=s"
)

PREDICTION_phi_12=(
	--observable "B-mixing::phi_12,q=s"
)

PREDICTION_R_Kstar=(
	--kinematics s_min 1.00 --kinematics s_max 6.00 --observable "B->K^*ll::R_K^*avg@LargeRecoil"
)
