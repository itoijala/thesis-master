#!/bin/bash

. eos-scripts/eos.sh

. run/common.sh

. run/combined.sh

. run/e-combined.sh

. run/su2.sh 2

. run/M.sh 1

PARAMETERS_SCAN=(
	"Re{ll_LQ} -5e-2 5e-2 --prior flat"
	"Im{ll_LQ} -5e-2 5e-2 --prior flat"
)

MCMC_CHUNKS=2500

main "$@"
