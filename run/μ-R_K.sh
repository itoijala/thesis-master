#!/bin/bash

. eos-scripts/eos.sh

. run/common.sh

. run/R_K.sh

. run/l.sh mu

PARAMETERS_SCAN=(
	"Re{ll_LQ} -1e-1 1e-1 --prior flat"
	"Im{ll_LQ} -1e-1 1e-1 --prior flat"
)

MCMC_CHAINS=8
MCMC_CHUNK_SIZE=5000
MCMC_CHUNKS=1000

main "$@"
