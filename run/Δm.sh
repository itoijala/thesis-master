#!/bin/bash

. eos-scripts/eos.sh

. run/common.sh

CONSTRAINTS=(
	"${CONSTRAINT_Deltam[@]}"
)

PARAMETERS_SCAN=(
	"Re{ll_LQ} -1e0 1e0 --prior flat"
	"Im{ll_LQ} -1e0 1e0 --prior flat"
)

PARAMETERS_NUISANCE=(
	"${NUISANCE_CKM[@]}"
	"${NUISANCE_BAG_PARAMETER[@]}"
	"${NUISANCE_DECAY_CONSTANT[@]}"
)

PREDICTIONS=(
	"${PREDICTION_Deltam[@]}"
)

MCMC_PRERUN_UPDATE=10000
MCMC_PRERUN_MAX=1000000

MCMC_CHUNK_SIZE=1000000
MCMC_CHUNKS=50

main "$@"
