#!/bin/bash

. eos-scripts/eos.sh

. run/common.sh

CONSTRAINTS=(
	"${CONSTRAINT_Deltam[@]}"
	"${CONSTRAINT_phi_12[@]}"
)

PARAMETERS_SCAN=(
	"Re{ll_LQ} -1e0 1e0 --prior flat"
	"Im{ll_LQ} -1e0 1e0 --prior flat"
)

PARAMETERS_NUISANCE=(
	"${NUISANCE_CKM[@]}"
	"${NUISANCE_BAG_PARAMETER[@]}"
	"${NUISANCE_DECAY_CONSTANT[@]}"
)

PREDICTIONS=(
	"${PREDICTION_Deltam[@]}"
	"${PREDICTION_phi_12[@]}"
)

MCMC_PRERUN_UPDATE=100000
MCMC_PRERUN_MIN=1000000
MCMC_PRERUN_MAX=2000000

MCMC_CHUNK_SIZE=1000000
MCMC_CHUNKS=25

main "$@"
