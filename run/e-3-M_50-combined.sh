#!/bin/bash

. eos-scripts/eos.sh

. run/common.sh

. run/combined.sh

. run/e-combined.sh

. run/su2.sh 3

. run/M.sh 50

PARAMETERS_SCAN=(
	"Re{ll_LQ} -2e0 2e0 --prior flat"
	"Im{ll_LQ} -2e0 2e0 --prior flat"
)

MCMC_CHUNKS=2500

main "$@"
