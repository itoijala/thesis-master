#!/bin/bash

. eos-scripts/eos.sh

. run/common.sh

CONSTRAINTS=(
	"${CONSTRAINT_phi_12[@]}"
)

PARAMETERS_SCAN=(
	"Re{ll_LQ} -2e-1 2e-1 --prior flat"
	"Im{ll_LQ} -1e0  1e0  --prior flat"
)

PREDICTIONS=(
	"${PREDICTION_phi_12[@]}"
)

MCMC_PRERUN_UPDATE=10000
MCMC_PRERUN_MIN=100000
MCMC_PRERUN_MAX=10000000

MCMC_CHUNK_SIZE=1000000
MCMC_CHUNKS=10

main "$@"
