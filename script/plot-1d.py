#!/usr/bin/env python3

import pickle
import re
import sys

import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt
import numpy as np
import scipy.ndimage

import libeos

x_label = sys.argv[2]
x_min = float(sys.argv[3])
x_max = float(sys.argv[4])

image_filename = sys.argv[5]
image = scipy.ndimage.imread(image_filename)

details_filename = sys.argv[6]
with open(details_filename, 'rb') as f:
    hist, bins, ylim = pickle.load(f)

fig, ax = plt.subplots(1, 1)

ax.imshow(image, interpolation='none', origin='upper', extent=(x_min, x_max, ylim[0], ylim[1]), aspect='auto')

def plot_prior(ax, hist, bins, prior_string, x_min, x_max):
    prior_type = prior_string.split(':', 1)[0]
    prior_params = prior_string.split(': ', 1)[1]

    x = np.linspace(x_min, x_max, 1000)
    prior = None

    # Gaussian: 17.757 +- 0.0211896201
    if prior_type == 'Gaussian':
        r = re.compile(r'^([^ ]+) \+ ([^ ]+) - ([^ ]+)$')
        m = r.match(prior_params)
        if m:
            mu = float(m.group(1))
            sigma_high = float(m.group(2))
            sigma_low = float(m.group(3))

            prior = libeos.distribution_asymmetric_gaussian_pdf(x, None, None, mu, sigma_low, sigma_high)

        else:
            r = re.compile(r'^([^ ]+) \+- ([^ ]+)$')
            m = r.match(prior_params)
            if m:
                mu = float(m.group(1))
                sigma = float(m.group(2))

                prior = libeos.distribution_gaussian_pdf(x, None, None, mu, sigma)

    # Amoroso limit: a = 0, theta = 0.50054, alpha = 5.0001, beta = 3.7997
    elif prior_type == 'Amoroso limit':
        r = re.compile(r'^a = ([^ ]+), theta = ([^ ]+), alpha = ([^ ]+), beta = ([^ ]+)$')
        m = r.match(prior_params)
        if m:
            a = float(m.group(1))
            theta = float(m.group(2))
            alpha = float(m.group(3))
            beta = float(m.group(4))

            prior = libeos.distribution_amoroso_pdf(x, None, None, a, theta, alpha, beta)

    if prior is None:
        raise Exception('Unknown prior type!')

    ax.plot(x, np.sum(hist * np.diff(bins)) * prior, 'r--')

if len(sys.argv) > 7:
    prior_string = sys.argv[7]
    plot_prior(ax, hist, bins, prior_string, x_min, x_max)

ax.set_xlim(x_min, x_max)
ax.set_xlabel(x_label)

ax.set_ylim(*ylim)
ax.set_yticks([])

ax.grid()
ax.ticklabel_format(scilimits=(-3, 4))

fig.tight_layout(pad=0)
fig.savefig(sys.argv[1])
