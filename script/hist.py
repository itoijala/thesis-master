#!/usr/bin/env python3

import itertools
import pickle
import sys

import h5py

from libeos import (
    parse_parameters,
    parse_observables,
    histogram_chunked,
)

filename = sys.argv[1]
f = h5py.File(filename, 'r')

if len(sys.argv) > 2:
    bins = int(sys.argv[2])
else:
    bins = 100

parameters = parse_parameters(f)
observables = parse_observables(f)

variables = {}
if 'data/observables' in f:
    if 'data/parameters' in f:
        variables.update(parameters)
    variables.update(observables)
else:
    variables.update(parameters)

if 'main run' in f:
    paths = sorted([('/main run/{}/samples'.format(c), None) for c in f['main run']])
elif 'prerun' in f:
    paths = sorted([('/prerun/{}/samples'.format(c), None) for c in f['prerun']])
elif 'data/parameters' in f or 'data/observables' in f:
    paths = [(
        'data/parameters' if 'data/parameters' in f else None,
        'data/observables' if 'data/observables' in f else None,
    )]
else:
    paths = [('/data/final/samples', None)]

f.close() # important!

plot_params = list(variables.keys())
plot_params = [(p,) for p in plot_params] + list(itertools.combinations(sorted(plot_params), 2))

params = [[variables[p] for p in params] for params in plot_params]

hist_output = histogram_chunked(filename, paths, params, bins=bins)

hist_dict = {p: hist_output[i] for i, p in enumerate(tuple(p) for p in plot_params)}

data = parameters, observables, hist_dict

with open('{}-hist-{}.pkl'.format(filename, bins), 'wb') as f:
    pickle.dump(data, f)
