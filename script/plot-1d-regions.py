#!/usr/bin/env python3

import pickle
import sys

import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt
import numpy as np

import libeos

x_param = sys.argv[3]
x_min = float(sys.argv[4])
x_max = float(sys.argv[5])

filename = sys.argv[6]

with open(filename, 'rb') as f:
    hist_dict = pickle.load(f)[2]

hist = hist_dict[(x_param,)]

levels = [1, 2, 3]
blues = mpl.cm.get_cmap('Blues')
colors = ['w'] + [blues(int(256 * i)) for i in [1/3, 2/3, 3/3]]

fig = plt.figure()
ax = fig.add_axes([0, 0, 1, 1])

hist, (bins_x,), _ = hist
cmap = mpl.colors.ListedColormap(colors)
norm = libeos.region_norm(hist, levels)

ax.plot(bins_x, list(hist) + [hist[-1]], drawstyle='steps-post', color='k', solid_capstyle='butt')
ax.bar(bins_x[:-1], hist, width=np.ediff1d(bins_x), color=mpl.cm.ScalarMappable(norm, cmap).to_rgba(hist), linewidth=0)

ax.set_xlim(x_min, x_max)

ax.set_axis_off()
ax.get_xaxis().set_visible(False)
ax.get_yaxis().set_visible(False)

fig.savefig(sys.argv[1], bbox_inches='tight', pad_inches=0)

with open(sys.argv[2], 'wb') as f:
    pickle.dump((hist, bins_x, ax.get_ylim()), f)
