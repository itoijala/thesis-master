#!/usr/bin/env python3

import pickle
import sys

import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt

import libeos

def color_alpha(color, alpha):
    return mpl.colors.colorConverter.to_rgba(color, alpha)

aspect = sys.argv[2]

x_param = sys.argv[3]
x_min = float(sys.argv[4])
x_max = float(sys.argv[5])

y_param = sys.argv[6]
y_min = float(sys.argv[7])
y_max = float(sys.argv[8])

params = [x_param, y_param]

filenames = sys.argv[9::3]
factors = map(float, sys.argv[10::3])
colors = sys.argv[11::3]

for i in range(len(colors)):
    if ',' in colors[i]:
        *rgb, alpha = [float(c) for c in colors[i].split(',')]
    else:
        rgb, alpha = colors[i], 0.5
    colors[i] = color_alpha(rgb, alpha)

hist_dicts = []
for filename in filenames:
    with open(filename, 'rb') as f:
        hist_dicts.append(pickle.load(f)[2])

hists = []
for d in hist_dicts:
    hist = d.get((x_param, y_param), None)
    if hist is None:
        h, (bx, by), (rx, ry) = d[(y_param, x_param)]
        hist = h.T, (by, bx), (ry, rx)
    hists.append(hist)

levels = [2]
colors = [[color_alpha('white', 0), c] for c in colors]

fig = plt.figure()
ax = fig.add_axes([0, 0, 1, 1])

for (hist, (bins_x, bins_y), _), color, factor in zip(hists, colors, factors):
    cmap = mpl.colors.ListedColormap(color)
    norm = libeos.region_norm(hist, levels)
    ax.pcolor(factor * bins_x, factor * bins_y, hist, cmap=cmap, norm=norm)

ax.set_xlim(x_min, x_max)
ax.set_ylim(y_min, y_max)
ax.set_aspect(aspect, adjustable='box')

ax.set_axis_off()
ax.get_xaxis().set_visible(False)
ax.get_yaxis().set_visible(False)

fig.savefig(sys.argv[1], bbox_inches='tight', pad_inches=0)
