#!/usr/bin/env python3

import sys

import matplotlib as mpl
mpl.use('pgf')
import matplotlib.pyplot as plt

from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf)

def color_alpha(color, alpha):
    return mpl.colors.colorConverter.to_rgba(color, alpha)

colors = sys.argv[3::2]
labels = sys.argv[4::2]

fig = plt.figure(figsize=(5.511, 5))

if sys.argv[2] == 'horizontal':
    kwargs = {
        'mode': 'expand',
        'loc': 'lower left',
        'bbox_to_anchor': mpl.transforms.Bbox.from_bounds(0, 0, 1, 1),
        'ncol': len(colors),
    }
elif sys.argv[2] == 'vertical':
    kwargs = {
    }
else:
    raise Exception('Unknown legend type "{}"'.format(sys.argv[2]))

legend = fig.legend(
    handles=[mpl.patches.Patch(fc=color_alpha(c, 0.5), ec='none') for c in colors],
    labels=labels,
    borderaxespad=0.,
    frameon=False,
    **kwargs
)

fig.savefig(sys.argv[1], pad_inches=0, bbox_inches='tight')
