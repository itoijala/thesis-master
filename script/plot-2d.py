#!/usr/bin/env python3

import sys

import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt
import scipy.ndimage

aspect = sys.argv[2]

x_label = sys.argv[3]
x_min = float(sys.argv[4])
x_max = float(sys.argv[5])

y_label = sys.argv[6]
y_min = float(sys.argv[7])
y_max = float(sys.argv[8])

image_filename = sys.argv[9]
image = scipy.ndimage.imread(image_filename)

fig, ax = plt.subplots(1, 1)

ax.imshow(image, interpolation='none', origin='upper', extent=(x_min, x_max, y_min, y_max), aspect='auto')

ax.set_xlim(x_min, x_max)
ax.set_xlabel(x_label)

ax.set_ylim(y_min, y_max)
ax.set_ylabel(y_label)

ax.grid()
ax.set_aspect(aspect, adjustable='box')
ax.ticklabel_format(scilimits=(-3, 4))

fig.savefig(sys.argv[1])
